//! My custom string.

use core::fmt;

#[cfg(feature = "alloc")]
pub use self::owned::MyString;

/// My custom string slice type.
#[derive(PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(transparent)]
// Comparisons implemented for the type are consistent (at least it is intended to be so).
// See <https://github.com/rust-lang/rust-clippy/issues/2025>.
#[allow(clippy::derive_hash_xor_eq, clippy::derive_ord_xor_partial_ord)]
#[cfg_attr(feature = "serde", derive(serde::Serialize))]
#[cfg_attr(feature = "serde", serde(transparent))]
pub struct MyStr(str);

/// Methods to create a `MyStr` value.
impl MyStr {
    /// Creates a new `MyStr` slice.
    ///
    /// # Examples
    ///
    /// ```
    /// # use custom_slice_book_sample::my_str::MyStr;
    /// let s: &MyStr = MyStr::new("hello");
    /// ```
    #[inline]
    #[must_use]
    pub fn new(s: &str) -> &Self {
        unsafe { &*(s as *const str as *const Self) }
    }

    /// Creates a new mutable `MyStr` slice.
    ///
    /// # Examples
    ///
    /// ```
    /// # use custom_slice_book_sample::my_str::MyStr;
    /// let mut s: String = "hello".to_owned();
    /// let t: &mut MyStr = MyStr::new_mut(&mut s);
    /// ```
    #[inline]
    #[must_use]
    pub fn new_mut(s: &mut str) -> &mut Self {
        unsafe { &mut *(s as *mut str as *mut Self) }
    }
}

/// Methods to get the inner value.
impl MyStr {
    /// Returns a reference to the inner string.
    ///
    /// # Examples
    ///
    /// ```
    /// # use custom_slice_book_sample::my_str::MyStr;
    /// let s: &MyStr = MyStr::new("hello");
    ///
    /// let inner: &str = s.as_str();
    /// assert_eq!(inner, "hello");
    /// ```
    #[inline]
    #[must_use]
    pub fn as_str(&self) -> &str {
        &self.0
    }

    /// Returns the mutable reference to the inner string.
    ///
    /// # Examples
    ///
    /// ```
    /// # use custom_slice_book_sample::my_str::MyStr;
    /// let mut s: String = "hello".to_owned();
    /// let t: &mut MyStr = MyStr::new_mut(&mut s);
    ///
    /// let inner: &mut str = t.as_mut_str();
    /// assert_eq!(inner, "hello");
    ///
    /// inner.make_ascii_uppercase();
    /// assert_eq!(inner, "HELLO");
    /// ```
    #[inline]
    #[must_use]
    pub fn as_mut_str(&mut self) -> &mut str {
        &mut self.0
    }
}

impl<'a> From<&'a str> for &'a MyStr {
    #[inline]
    fn from(s: &'a str) -> Self {
        MyStr::new(s)
    }
}

impl<'a> From<&'a mut str> for &'a mut MyStr {
    #[inline]
    fn from(s: &'a mut str) -> Self {
        MyStr::new_mut(s)
    }
}

impl<'a> From<&'a MyStr> for &'a str {
    #[inline]
    fn from(s: &'a MyStr) -> Self {
        s.as_str()
    }
}

impl<'a> From<&'a mut MyStr> for &'a mut str {
    #[inline]
    fn from(s: &'a mut MyStr) -> Self {
        s.as_mut_str()
    }
}

impl AsRef<MyStr> for MyStr {
    #[inline]
    fn as_ref(&self) -> &MyStr {
        self
    }
}

impl AsMut<MyStr> for MyStr {
    #[inline]
    fn as_mut(&mut self) -> &mut MyStr {
        self
    }
}

impl AsRef<str> for MyStr {
    #[inline]
    fn as_ref(&self) -> &str {
        self.as_str()
    }
}

impl AsMut<str> for MyStr {
    #[inline]
    fn as_mut(&mut self) -> &mut str {
        self.as_mut_str()
    }
}

impl fmt::Debug for MyStr {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl fmt::Display for MyStr {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl Default for &MyStr {
    fn default() -> Self {
        MyStr::new(<&str>::default())
    }
}

impl Default for &mut MyStr {
    fn default() -> Self {
        MyStr::new_mut(<&mut str>::default())
    }
}

#[cfg(feature = "serde")]
impl<'de> serde::Deserialize<'de> for &'de MyStr {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        use serde::de::Visitor;

        /// Visitor for `&MyStr`.
        struct StrVisitor;

        impl<'de> Visitor<'de> for StrVisitor {
            type Value = &'de MyStr;

            #[inline]
            fn expecting(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                f.write_str("a string")
            }

            #[inline]
            fn visit_borrowed_str<E>(self, v: &'de str) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                Ok(Self::Value::from(v))
            }
        }

        deserializer.deserialize_str(StrVisitor)
    }
}

/// Implement `PartialEq` and `Eq` for the given types.
macro_rules! impl_cmp {
    ($ty_lhs:ty, $ty_rhs:ty) => {
        impl PartialEq<$ty_rhs> for $ty_lhs {
            #[inline]
            fn eq(&self, o: &$ty_rhs) -> bool {
                <str as PartialEq<str>>::eq(AsRef::as_ref(self), AsRef::as_ref(o))
            }
        }
        impl PartialOrd<$ty_rhs> for $ty_lhs {
            #[inline]
            fn partial_cmp(&self, o: &$ty_rhs) -> Option<core::cmp::Ordering> {
                <str as PartialOrd<str>>::partial_cmp(AsRef::as_ref(self), AsRef::as_ref(o))
            }
        }
    };
}

/// Implement `PartialEq` and `Eq` symmetrically for the given types.
macro_rules! impl_cmp_symmetric {
    ($ty_lhs:ty, $ty_rhs:ty) => {
        impl_cmp!($ty_lhs, $ty_rhs);
        impl_cmp!($ty_rhs, $ty_lhs);
    };
}

impl_cmp_symmetric!(MyStr, str);
impl_cmp_symmetric!(MyStr, &str);
impl_cmp_symmetric!(&MyStr, str);

/// Owned types and related trait impls.
#[cfg(feature = "alloc")]
mod owned {
    use super::*;

    use alloc::borrow::{Cow, ToOwned};
    use alloc::boxed::Box;
    use alloc::rc::Rc;
    use alloc::string::String;
    use alloc::sync::Arc;

    /// My custom owned string type.
    #[derive(Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
    // Comparisons implemented for the type are consistent (at least it is intended to be so).
    // See <https://github.com/rust-lang/rust-clippy/issues/2025>.
    #[allow(clippy::derive_hash_xor_eq, clippy::derive_ord_xor_partial_ord)]
    #[cfg_attr(feature = "serde", derive(serde::Serialize))]
    #[cfg_attr(feature = "serde", serde(transparent))]
    pub struct MyString(String);

    /// Method to create a `MyString`.
    impl MyString {
        #[inline]
        #[must_use]
        pub fn new(s: String) -> Self {
            Self(s)
        }
    }

    /// Methods to get borrowed `MyStr`.
    impl MyString {
        #[inline]
        #[must_use]
        pub fn as_my_str(&self) -> &MyStr {
            MyStr::new(self.0.as_str())
        }

        #[inline]
        #[must_use]
        pub fn as_my_str_mut(&mut self) -> &mut MyStr {
            MyStr::new_mut(self.0.as_mut_str())
        }
    }

    impl core::borrow::Borrow<MyStr> for MyString {
        #[inline]
        fn borrow(&self) -> &MyStr {
            self.as_my_str()
        }
    }

    impl core::borrow::BorrowMut<MyStr> for MyString {
        #[inline]
        fn borrow_mut(&mut self) -> &mut MyStr {
            self.as_my_str_mut()
        }
    }

    impl alloc::borrow::ToOwned for MyStr {
        type Owned = MyString;

        fn to_owned(&self) -> Self::Owned {
            MyString::new(self.as_str().to_owned())
        }
    }

    impl From<String> for MyString {
        #[inline]
        fn from(s: String) -> Self {
            Self::new(s)
        }
    }

    impl From<&str> for MyString {
        #[inline]
        fn from(s: &str) -> Self {
            Self::new(s.to_owned())
        }
    }

    impl From<MyString> for String {
        #[inline]
        fn from(s: MyString) -> Self {
            s.0
        }
    }

    impl From<&MyStr> for MyString {
        #[inline]
        fn from(s: &MyStr) -> Self {
            s.to_owned()
        }
    }

    impl From<&mut MyStr> for MyString {
        #[inline]
        fn from(s: &mut MyStr) -> Self {
            s.to_owned()
        }
    }

    impl From<&MyString> for MyString {
        #[inline]
        fn from(s: &MyString) -> Self {
            s.clone()
        }
    }

    impl From<&MyStr> for Box<MyStr> {
        fn from(s: &MyStr) -> Self {
            // Create the boxed inner slice.
            let inner_box: Box<str> = Box::from(s.as_str());
            // Take the allocated memory out of the box, without releasing.
            let inner_boxed_ptr: *mut str = Box::into_raw(inner_box);

            unsafe {
                // SAFETY: The string is valid as `MyStr`.
                // SAFETY: Memory layouts for `str` and `MyStr` are compatible,
                // and `inner_boxed_ptr` is allocated by `Box::<str>::from()`.
                Box::from_raw(inner_boxed_ptr as *mut MyStr)
            }
        }
    }

    impl From<&MyStr> for Rc<MyStr> {
        fn from(s: &MyStr) -> Self {
            // Create the shared inner slice.
            let inner_box: Rc<str> = Rc::from(s.as_str());
            // Take the allocated memory out of the Rc, without releasing.
            let inner_boxed_ptr: *const str = Rc::into_raw(inner_box);

            unsafe {
                // SAFETY: The string is valid as `MyStr`.
                // SAFETY: Memory layouts for `str` and `MyStr` are compatible,
                // and `inner_boxed_ptr` is allocated by `Rc::<str>::from()`.
                Rc::from_raw(inner_boxed_ptr as *const MyStr)
            }
        }
    }

    impl From<&MyStr> for Arc<MyStr> {
        fn from(s: &MyStr) -> Self {
            // Create the shared inner slice.
            let inner_box: Arc<str> = Arc::from(s.as_str());
            // Take the allocated memory out of the Arc, without releasing.
            let inner_boxed_ptr: *const str = Arc::into_raw(inner_box);

            unsafe {
                // SAFETY: The string is valid as `MyStr`.
                // SAFETY: Memory layouts for `str` and `MyStr` are compatible,
                // and `inner_boxed_ptr` is allocated by `Arc::<str>::from()`.
                Arc::from_raw(inner_boxed_ptr as *const MyStr)
            }
        }
    }

    impl From<MyString> for Box<MyStr> {
        fn from(s: MyString) -> Self {
            // Create the boxed inner slice.
            let inner_box: Box<str> = Box::from(s.0);
            // Take the allocated memory out of the box, without releasing.
            let inner_boxed_ptr: *mut str = Box::into_raw(inner_box);

            unsafe {
                // SAFETY: The string is valid as `MyStr`.
                // SAFETY: Memory layouts for `str` and `MyStr` are compatible,
                // and `inner_boxed_ptr` is allocated by `Box::<str>::from()`.
                Box::from_raw(inner_boxed_ptr as *mut MyStr)
            }
        }
    }

    impl From<MyString> for Rc<MyStr> {
        fn from(s: MyString) -> Self {
            // Create the shared inner slice.
            let inner_box: Rc<str> = Rc::from(s.0);
            // Take the allocated memory out of the Rc, without releasing.
            let inner_boxed_ptr: *const str = Rc::into_raw(inner_box);

            unsafe {
                // SAFETY: The string is valid as `MyStr`.
                // SAFETY: Memory layouts for `str` and `MyStr` are compatible,
                // and `inner_boxed_ptr` is allocated by `Rc::<str>::from()`.
                Rc::from_raw(inner_boxed_ptr as *const MyStr)
            }
        }
    }

    impl From<MyString> for Arc<MyStr> {
        fn from(s: MyString) -> Self {
            // Create the shared inner slice.
            let inner_box: Arc<str> = Arc::from(s.0);
            // Take the allocated memory out of the Arc, without releasing.
            let inner_boxed_ptr: *const str = Arc::into_raw(inner_box);

            unsafe {
                // SAFETY: The string is valid as `MyStr`.
                // SAFETY: Memory layouts for `str` and `MyStr` are compatible,
                // and `inner_boxed_ptr` is allocated by `Arc::<str>::from()`.
                Arc::from_raw(inner_boxed_ptr as *const MyStr)
            }
        }
    }

    impl From<Box<str>> for Box<MyStr> {
        fn from(s: Box<str>) -> Self {
            let boxed_ptr: *mut str = Box::into_raw(s);
            unsafe {
                // SAFETY: The `str` string is also valid as `MyStr`.
                // SAFETY: Memory layouts for `str` and `MyStr` are compatible,
                // and `boxed_ptr` is a pointer from valid `Box<str>`.
                Box::from_raw(boxed_ptr as *mut MyStr)
            }
        }
    }

    impl From<String> for Box<MyStr> {
        fn from(s: String) -> Self {
            s.into_boxed_str().into()
        }
    }

    impl From<Box<MyStr>> for MyString {
        fn from(s: Box<MyStr>) -> Self {
            let boxed_str: Box<str> = s.into();
            Self::new(String::from(boxed_str))
        }
    }

    impl From<Box<MyStr>> for Box<str> {
        fn from(s: Box<MyStr>) -> Self {
            let boxed_ptr = Box::into_raw(s);
            unsafe {
                // SAFETY: The `MyStr` string is also valid as `str`.
                // SAFETY: Memory layouts for `str` and `MyStr` are compatible,
                // and `boxed_ptr` is a pointer from valid `Box<MyStr>`.
                Box::from_raw(boxed_ptr as *mut str)
            }
        }
    }

    impl From<Box<MyStr>> for String {
        fn from(s: Box<MyStr>) -> Self {
            let boxed_str: Box<str> = s.into();
            boxed_str.into()
        }
    }

    impl<'a> From<&'a MyStr> for Cow<'a, MyStr> {
        #[inline]
        fn from(s: &'a MyStr) -> Self {
            Cow::Borrowed(s)
        }
    }

    impl From<MyString> for Cow<'_, MyStr> {
        #[inline]
        fn from(s: MyString) -> Self {
            Cow::Owned(s)
        }
    }

    impl<'a> From<&'a MyString> for Cow<'a, MyStr> {
        #[inline]
        fn from(s: &'a MyString) -> Self {
            Cow::Borrowed(s.as_my_str())
        }
    }

    impl From<Cow<'_, MyStr>> for Box<MyStr> {
        fn from(s: Cow<'_, MyStr>) -> Self {
            match s {
                Cow::Borrowed(s) => s.into(),
                Cow::Owned(s) => s.into(),
            }
        }
    }

    impl From<Cow<'_, MyStr>> for MyString {
        fn from(s: Cow<'_, MyStr>) -> Self {
            match s {
                Cow::Borrowed(s) => s.to_owned(),
                Cow::Owned(s) => s,
            }
        }
    }

    impl core::str::FromStr for MyString {
        type Err = core::convert::Infallible;

        #[inline]
        fn from_str(s: &str) -> Result<Self, Self::Err> {
            Ok(s.into())
        }
    }

    impl core::str::FromStr for Box<MyStr> {
        type Err = core::convert::Infallible;

        #[inline]
        fn from_str(s: &str) -> Result<Self, Self::Err> {
            Ok(Box::<str>::from(s).into())
        }
    }

    impl core::ops::Deref for MyString {
        type Target = MyStr;

        #[inline]
        fn deref(&self) -> &Self::Target {
            self.as_my_str()
        }
    }

    impl core::ops::DerefMut for MyString {
        #[inline]
        fn deref_mut(&mut self) -> &mut Self::Target {
            self.as_my_str_mut()
        }
    }

    impl AsRef<MyStr> for MyString {
        #[inline]
        fn as_ref(&self) -> &MyStr {
            self.as_my_str()
        }
    }

    impl AsMut<MyStr> for MyString {
        #[inline]
        fn as_mut(&mut self) -> &mut MyStr {
            self.as_my_str_mut()
        }
    }

    impl AsRef<str> for Box<MyStr> {
        #[inline]
        fn as_ref(&self) -> &str {
            self.as_str()
        }
    }

    impl AsMut<str> for Box<MyStr> {
        #[inline]
        fn as_mut(&mut self) -> &mut str {
            self.as_mut_str()
        }
    }

    impl AsRef<str> for MyString {
        #[inline]
        fn as_ref(&self) -> &str {
            self.as_str()
        }
    }

    impl AsMut<str> for MyString {
        #[inline]
        fn as_mut(&mut self) -> &mut str {
            self.as_mut_str()
        }
    }

    impl fmt::Debug for MyString {
        #[inline]
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            self.0.fmt(f)
        }
    }

    impl fmt::Display for MyString {
        #[inline]
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            self.0.fmt(f)
        }
    }

    impl Default for Box<MyStr> {
        #[inline]
        fn default() -> Self {
            <&MyStr>::default().into()
        }
    }

    impl_cmp_symmetric!(MyStr, String);
    impl_cmp_symmetric!(MyStr, &String);
    impl_cmp_symmetric!(&MyStr, String);
    impl_cmp_symmetric!(MyStr, Box<str>);
    impl_cmp_symmetric!(&MyStr, Box<str>);
    impl_cmp_symmetric!(Box<MyStr>, str);
    impl_cmp_symmetric!(Box<MyStr>, &str);
    impl_cmp_symmetric!(MyStr, Cow<'_, str>);
    impl_cmp_symmetric!(&MyStr, Cow<'_, str>);

    impl_cmp_symmetric!(MyString, &MyString);
    impl_cmp_symmetric!(MyString, MyStr);
    impl_cmp_symmetric!(MyString, &MyStr);
    impl_cmp_symmetric!(&MyString, MyStr);
    impl_cmp_symmetric!(MyString, str);
    impl_cmp_symmetric!(MyString, &str);
    impl_cmp_symmetric!(&MyString, str);
    impl_cmp_symmetric!(MyString, String);
    impl_cmp_symmetric!(MyString, &String);
    impl_cmp_symmetric!(&MyString, String);

    #[cfg(feature = "serde")]
    impl<'de> serde::Deserialize<'de> for MyString {
        fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
        where
            D: serde::Deserializer<'de>,
        {
            /// Visitor for `MyString`.
            struct StringVisitor;

            impl<'de> serde::de::Visitor<'de> for StringVisitor {
                type Value = MyString;

                #[inline]
                fn expecting(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                    f.write_str("a string")
                }

                #[inline]
                fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
                where
                    E: serde::de::Error,
                {
                    Ok(Self::Value::from(v))
                }

                #[inline]
                fn visit_string<E>(self, v: String) -> Result<Self::Value, E>
                where
                    E: serde::de::Error,
                {
                    Ok(Self::Value::from(v))
                }
            }

            deserializer.deserialize_string(StringVisitor)
        }
    }
}

#[cfg(feature = "serde")]
#[cfg(test)]
mod serde_tests {
    use super::*;

    use serde::de::{
        value::{BorrowedStrDeserializer, Error},
        Deserialize, IntoDeserializer,
    };

    #[test]
    fn deserialize_borrowed_str() {
        let source_data = "hello";
        let source_input = BorrowedStrDeserializer::<'_, Error>::new(source_data);
        let mystr: &MyStr = <&MyStr>::deserialize(source_input).unwrap();
        assert_eq!(mystr.as_str(), source_data);
    }

    #[test]
    fn deserialize_owned_str() {
        let source_input = "hello".to_owned().into_deserializer();
        let result: Result<&MyStr, Error> = <&MyStr>::deserialize(source_input);
        assert!(
            result.is_err(),
            "Deserialize is impossible when the source data does not have enough lifetime"
        );
    }
}
