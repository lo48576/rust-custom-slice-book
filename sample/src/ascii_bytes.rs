//! ASCII bytes.

use core::convert::TryFrom;
use core::fmt;

#[cfg(feature = "alloc")]
pub use self::owned::{AsciiByteBuf, CreationError};

/// Error for conversion from bytes to ASCII string.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct AsciiError {
    /// Byte position up to where the string is valid.
    valid_up_to: usize,
    /// Value of the first invalid byte.
    invalid_byte: u8,
}

impl AsciiError {
    /// Returns the byte position up to where the string is valid.
    #[inline]
    #[must_use]
    pub fn valid_up_to(&self) -> usize {
        self.valid_up_to
    }

    /// Returns the value of the first invalid byte.
    #[inline]
    #[must_use]
    pub fn invalid_byte(&self) -> u8 {
        self.invalid_byte
    }
}

impl fmt::Display for AsciiError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "invalid ASCII character {:#02x?} found at index {}",
            self.invalid_byte, self.valid_up_to
        )
    }
}

#[cfg(feature = "std")]
impl std::error::Error for AsciiError {}

/// ASCII bytes slice type.
#[derive(PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(transparent)]
// Comparisons implemented for the type are consistent (at least it is intended to be so).
// See <https://github.com/rust-lang/rust-clippy/issues/2025>.
#[allow(clippy::derive_hash_xor_eq, clippy::derive_ord_xor_partial_ord)]
#[cfg_attr(feature = "serde", derive(serde::Serialize))]
#[cfg_attr(feature = "serde", serde(transparent))]
pub struct AsciiBytes(str);

/// Validation method.
impl AsciiBytes {
    /// Checks whether the given string is an ASCII string.
    fn validate_bytes(s: &[u8]) -> Result<(), AsciiError> {
        match s
            .iter()
            .copied()
            .enumerate()
            .find(|(_pos, byte)| !byte.is_ascii())
        {
            Some((pos, byte)) => Err(AsciiError {
                valid_up_to: pos,
                invalid_byte: byte,
            }),
            None => Ok(()),
        }
    }
}

/// Methods to create an `AsciiBytes` string without validation.
impl AsciiBytes {
    /// Creates a new ASCII string slice from the given bytes.
    ///
    /// # Safety
    ///
    /// The given bytes should consists of only ASCII characters.
    /// If this constraint is violated, undefined behavior results.
    ///
    /// # Examples
    ///
    /// ```
    /// # use custom_slice_book_sample::ascii_bytes::AsciiBytes;
    /// let from_bytes: &AsciiBytes = unsafe {
    ///     AsciiBytes::new_unchecked(b"bytes")
    /// };
    ///
    /// let from_str: &AsciiBytes = unsafe {
    ///     AsciiBytes::new_unchecked("str".as_bytes())
    /// };
    /// ```
    #[inline]
    #[must_use]
    pub unsafe fn new_unchecked(bytes: &[u8]) -> &Self {
        // SAFETY: This is safe because ASCII string is a valid UTF-8 sequence.
        let s = core::str::from_utf8_unchecked(bytes);

        &*(s as *const str as *const Self)
    }

    /// Creates a new mutable ASCII string slice from the given mutable bytes.
    ///
    /// # Safety
    ///
    /// The given bytes should consists of only ASCII characters.
    /// If this constraint is violated, undefined behavior results.
    ///
    /// # Examples
    ///
    /// ```
    /// # use custom_slice_book_sample::ascii_bytes::AsciiBytes;
    /// let mut vec: Vec<u8> = b"bytes"[..].to_owned();
    /// let from_bytes_mut: &mut AsciiBytes = unsafe {
    ///     AsciiBytes::new_unchecked_mut(&mut vec)
    /// };
    /// ```
    #[inline]
    #[must_use]
    pub unsafe fn new_unchecked_mut(bytes: &mut [u8]) -> &mut Self {
        // SAFETY: This is safe because ASCII string is a valid UTF-8 sequence.
        let s = core::str::from_utf8_unchecked_mut(bytes);

        Self::from_mut_str_unchecked(s)
    }

    /// Creates a new mutable ASCII string slice from the given mutable bytes.
    ///
    /// # Safety
    ///
    /// The given bytes should consists of only ASCII characters.
    /// If this constraint is violated, undefined behavior results.
    ///
    /// # Examples
    ///
    /// ```
    /// # use custom_slice_book_sample::ascii_bytes::AsciiBytes;
    /// let mut s: String = "string".to_owned();
    /// let from_mut_str: &mut AsciiBytes = unsafe {
    ///     AsciiBytes::from_mut_str_unchecked(&mut s)
    /// };
    /// ```
    #[inline]
    #[must_use]
    pub unsafe fn from_mut_str_unchecked(s: &mut str) -> &mut Self {
        &mut *(s as *mut str as *mut Self)
    }
}

/// Methods to create an `AsciiBytes` string safely.
impl AsciiBytes {
    /// Creates a new ASCII string slice from the given bytes.
    fn from_bytes(s: &[u8]) -> Result<&Self, AsciiError> {
        match Self::validate_bytes(s) {
            Ok(_) => Ok(unsafe {
                // SAFETY: This is safe because the string is successfully validated.
                Self::new_unchecked(s)
            }),
            Err(e) => Err(e),
        }
    }

    /// Creates a new ASCII string slice from the given bytes.
    ///
    /// # Examples
    ///
    /// ```
    /// # use custom_slice_book_sample::ascii_bytes::AsciiBytes;
    /// assert!(AsciiBytes::new(b"bytes").is_ok());
    /// assert!(AsciiBytes::new("str").is_ok());
    ///
    /// assert!(AsciiBytes::new(b"\xFF").is_err());
    /// assert!(AsciiBytes::new("\u{FFFD}").is_err());
    /// ```
    #[inline]
    pub fn new<T: ?Sized + AsRef<[u8]>>(s: &T) -> Result<&Self, AsciiError> {
        Self::from_bytes(s.as_ref())
    }

    /// Creates a new ASCII string slice from the given mutable bytes.
    ///
    /// # Examples
    ///
    /// ```
    /// # use custom_slice_book_sample::ascii_bytes::AsciiBytes;
    /// let mut vec_ascii: Vec<u8> = b"bytes"[..].to_owned();
    /// assert!(AsciiBytes::from_bytes_mut(&mut vec_ascii).is_ok());
    ///
    /// let mut vec_non_ascii: Vec<u8> = b"\xFF"[..].to_owned();
    /// assert!(AsciiBytes::from_bytes_mut(&mut vec_non_ascii).is_err());
    /// ```
    pub fn from_bytes_mut(s: &mut [u8]) -> Result<&mut Self, AsciiError> {
        match Self::validate_bytes(s) {
            Ok(_) => Ok(unsafe {
                // SAFETY: This is safe because the string is successfully validated.
                Self::new_unchecked_mut(s)
            }),
            Err(e) => Err(e),
        }
    }

    /// Creates a new ASCII string slice from the given mutable bytes-like values.
    ///
    /// # Examples
    ///
    /// ```
    /// # use custom_slice_book_sample::ascii_bytes::AsciiBytes;
    /// let mut vec_ascii: Vec<u8> = b"ascii"[..].to_owned();
    /// assert!(AsciiBytes::new_mut(&mut vec_ascii).is_ok());
    /// ```
    #[inline]
    pub fn new_mut<T: ?Sized + AsMut<[u8]>>(s: &mut T) -> Result<&mut Self, AsciiError> {
        Self::from_bytes_mut(s.as_mut())
    }

    /// Creates a new ASCII string slice from the given mutable string.
    ///
    /// # Examples
    ///
    /// ```
    /// # use custom_slice_book_sample::ascii_bytes::AsciiBytes;
    /// let mut string_ascii: String = "ascii".to_owned();
    /// assert!(AsciiBytes::from_mut_str(&mut string_ascii).is_ok());
    ///
    /// let mut string_non_ascii: String = "\u{FFFD}".to_owned();
    /// assert!(AsciiBytes::from_mut_str(&mut string_non_ascii).is_err());
    /// ```
    #[inline]
    pub fn from_mut_str(s: &mut str) -> Result<&mut Self, AsciiError> {
        // SAFETY: `AsciiBytes` guarantees that the string is an ASCII string,
        // and ASCII string is valid UTF-8 sequence.
        let bytes = unsafe { s.as_bytes_mut() };

        Self::from_bytes_mut(bytes)
    }
}

/// Methods to get the inner value.
impl AsciiBytes {
    /// Returns a reference to the inner string.
    ///
    /// # Examples
    ///
    /// ```
    /// # use custom_slice_book_sample::ascii_bytes::{AsciiBytes, AsciiError};
    /// let s = AsciiBytes::new("hello")?;
    ///
    /// let inner: &str = s.as_str();
    /// assert_eq!(inner, "hello");
    /// # Ok::<_, AsciiError>(())
    /// ```
    #[inline]
    #[must_use]
    pub fn as_str(&self) -> &str {
        &self.0
    }

    /// Returns the mutable reference to the inner string.
    ///
    /// # Safety
    ///
    /// The caller must ensure that the string is an ASCII string when the borrow ends.
    ///
    /// Use of an `AsciiStr` which contains non-ASCII characters is undefined behavior.
    ///
    /// # Examples
    ///
    /// ```
    /// # use custom_slice_book_sample::ascii_bytes::{AsciiBytes, AsciiError};
    /// let mut ascii = "hello".to_owned();
    /// let s = AsciiBytes::from_mut_str(&mut ascii)?;
    /// assert_eq!(s.as_str(), "hello");
    ///
    /// unsafe {
    ///     let inner = s.as_mut_str();
    ///     // SAFETY: `<str>::make_ascii_upper()` does not increase non-ASCII characters.
    ///     inner.make_ascii_uppercase();
    /// }
    /// assert_eq!(s.as_str(), "HELLO");
    /// assert_eq!(ascii, "HELLO");
    /// # Ok::<_, AsciiError>(())
    /// ```
    #[inline]
    #[must_use]
    pub unsafe fn as_mut_str(&mut self) -> &mut str {
        &mut self.0
    }
}

/// Methods to get the inner value as bytes.
impl AsciiBytes {
    /// Returns a reference to the inner string.
    ///
    /// # Examples
    ///
    /// ```
    /// # use custom_slice_book_sample::ascii_bytes::{AsciiBytes, AsciiError};
    /// let s = AsciiBytes::new("hello")?;
    ///
    /// let bytes: &[u8] = s.as_bytes();
    /// assert_eq!(bytes, b"hello");
    /// # Ok::<_, AsciiError>(())
    /// ```
    #[inline]
    #[must_use]
    pub fn as_bytes(&self) -> &[u8] {
        self.0.as_bytes()
    }

    /// Returns the mutable reference to the inner bytes.
    ///
    /// # Safety
    ///
    /// The caller must ensure that the bytes is an ASCII string when the borrow ends.
    ///
    /// Use of an `AsciiBytes` which contains non-ASCII characters is undefined behavior.
    ///
    /// # Examples
    ///
    /// ```
    /// # use custom_slice_book_sample::ascii_bytes::{AsciiBytes, AsciiError};
    /// let mut ascii = "hello".to_owned();
    /// let s = AsciiBytes::from_mut_str(&mut ascii)?;
    /// assert_eq!(s.as_str(), "hello");
    ///
    /// unsafe {
    ///     let inner = s.as_bytes_mut();
    ///     // SAFETY: `b'H'` is an ASCII character.
    ///     inner[0] = b'H';
    /// }
    /// assert_eq!(s.as_str(), "Hello");
    /// assert_eq!(ascii, "Hello");
    /// # Ok::<_, AsciiError>(())
    /// ```
    #[inline]
    #[must_use]
    pub unsafe fn as_bytes_mut(&mut self) -> &mut [u8] {
        self.0.as_bytes_mut()
    }
}

impl<'a> TryFrom<&'a str> for &'a AsciiBytes {
    type Error = AsciiError;

    #[inline]
    fn try_from(s: &'a str) -> Result<Self, Self::Error> {
        AsciiBytes::new(s)
    }
}

impl<'a> TryFrom<&'a [u8]> for &'a AsciiBytes {
    type Error = AsciiError;

    #[inline]
    fn try_from(s: &'a [u8]) -> Result<Self, Self::Error> {
        AsciiBytes::new(s)
    }
}

impl<'a> TryFrom<&'a mut str> for &'a mut AsciiBytes {
    type Error = AsciiError;

    #[inline]
    fn try_from(s: &'a mut str) -> Result<Self, Self::Error> {
        AsciiBytes::from_mut_str(s)
    }
}

impl<'a> TryFrom<&'a mut [u8]> for &'a mut AsciiBytes {
    type Error = AsciiError;

    #[inline]
    fn try_from(s: &'a mut [u8]) -> Result<Self, Self::Error> {
        AsciiBytes::new_mut(s)
    }
}

impl<'a> From<&'a AsciiBytes> for &'a str {
    #[inline]
    fn from(s: &'a AsciiBytes) -> Self {
        s.as_str()
    }
}

impl<'a> From<&'a AsciiBytes> for &'a [u8] {
    #[inline]
    fn from(s: &'a AsciiBytes) -> Self {
        s.as_bytes()
    }
}

impl AsRef<AsciiBytes> for AsciiBytes {
    #[inline]
    fn as_ref(&self) -> &AsciiBytes {
        self
    }
}

impl AsMut<AsciiBytes> for AsciiBytes {
    #[inline]
    fn as_mut(&mut self) -> &mut AsciiBytes {
        self
    }
}

impl AsRef<str> for AsciiBytes {
    #[inline]
    fn as_ref(&self) -> &str {
        self.as_str()
    }
}

impl AsRef<[u8]> for AsciiBytes {
    #[inline]
    fn as_ref(&self) -> &[u8] {
        self.as_bytes()
    }
}

impl fmt::Debug for AsciiBytes {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl fmt::Display for AsciiBytes {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl Default for &AsciiBytes {
    fn default() -> Self {
        unsafe {
            // SAFETY: An empty bytes is valid ASCII string.
            AsciiBytes::new_unchecked(<&[u8]>::default())
        }
    }
}

impl Default for &mut AsciiBytes {
    fn default() -> Self {
        unsafe {
            // SAFETY: An empty bytes is valid ASCII string.
            AsciiBytes::new_unchecked_mut(<&mut [u8]>::default())
        }
    }
}

/// Implement `PartialEq` and `Eq` for the given types.
macro_rules! impl_cmp {
    ($ty_common:ty, $ty_lhs:ty, $ty_rhs:ty) => {
        impl PartialEq<$ty_rhs> for $ty_lhs {
            #[inline]
            fn eq(&self, o: &$ty_rhs) -> bool {
                <$ty_common as PartialEq<$ty_common>>::eq(AsRef::as_ref(self), AsRef::as_ref(o))
            }
        }
        impl PartialOrd<$ty_rhs> for $ty_lhs {
            #[inline]
            fn partial_cmp(&self, o: &$ty_rhs) -> Option<core::cmp::Ordering> {
                <$ty_common as PartialOrd<$ty_common>>::partial_cmp(
                    AsRef::as_ref(self),
                    AsRef::as_ref(o),
                )
            }
        }
    };
}

/// Implement `PartialEq` and `Eq` symmetrically for the given types.
macro_rules! impl_cmp_symmetric {
    ($ty_common:ty, $ty_lhs:ty, $ty_rhs:ty) => {
        impl_cmp!($ty_common, $ty_lhs, $ty_rhs);
        impl_cmp!($ty_common, $ty_rhs, $ty_lhs);
    };
}

impl_cmp_symmetric!(str, AsciiBytes, str);
impl_cmp_symmetric!(str, AsciiBytes, &str);
impl_cmp_symmetric!(str, &AsciiBytes, str);
impl_cmp_symmetric!([u8], AsciiBytes, [u8]);
impl_cmp_symmetric!([u8], AsciiBytes, &[u8]);
impl_cmp_symmetric!([u8], &AsciiBytes, [u8]);

#[cfg(feature = "serde")]
impl<'de> serde::Deserialize<'de> for &'de AsciiBytes {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        /// Visitor for `&AsciiBytes`.
        struct BytesVisitor;

        impl<'de> serde::de::Visitor<'de> for BytesVisitor {
            type Value = &'de AsciiBytes;

            #[inline]
            fn expecting(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                f.write_str("ASCII bytes")
            }

            #[inline]
            fn visit_borrowed_str<E>(self, v: &'de str) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                Self::Value::try_from(v).map_err(E::custom)
            }

            #[inline]
            fn visit_borrowed_bytes<E>(self, v: &'de [u8]) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                Self::Value::try_from(v).map_err(E::custom)
            }
        }

        deserializer.deserialize_any(BytesVisitor)
    }
}

/// Owned types and related trait impls.
#[cfg(feature = "alloc")]
mod owned {
    use super::*;

    use alloc::borrow::{Cow, ToOwned};
    use alloc::boxed::Box;
    use alloc::rc::Rc;
    use alloc::string::String;
    use alloc::sync::Arc;
    use alloc::vec::Vec;

    /// Error for conversion into ASCII bytes.
    #[derive(Debug, Clone)]
    pub struct CreationError<T> {
        /// Source string which cannot be converted.
        source: T,
        /// Conversion error.
        error: AsciiError,
    }

    impl<T> CreationError<T> {
        /// Returns a reference to the source.
        #[inline]
        #[must_use]
        pub fn source(&self) -> &T {
            &self.source
        }

        /// Retruns the source.
        #[inline]
        #[must_use]
        pub fn into_source(self) -> T {
            self.source
        }

        /// Returns the inner conversion error.
        #[inline]
        #[must_use]
        pub fn ascii_error(&self) -> AsciiError {
            self.error
        }
    }

    impl<T> fmt::Display for CreationError<T> {
        #[inline]
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            self.error.fmt(f)
        }
    }

    #[cfg(feature = "std")]
    impl<T: fmt::Debug> std::error::Error for CreationError<T> {}

    /// My custom owned ASCII bytes type.
    #[derive(Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
    // Comparisons implemented for the type are consistent (at least it is intended to be so).
    // See <https://github.com/rust-lang/rust-clippy/issues/2025>.
    #[allow(clippy::derive_hash_xor_eq, clippy::derive_ord_xor_partial_ord)]
    #[cfg_attr(feature = "serde", derive(serde::Serialize))]
    #[cfg_attr(feature = "serde", serde(transparent))]
    pub struct AsciiByteBuf(String);

    /// Methods to create an `AsciiByteBuf`.
    impl AsciiByteBuf {
        /// Creates a new `AsciiByteBuf` from the given string.
        ///
        /// # Safety
        ///
        /// The given string must be an ASCII string.
        /// If this constraint is violated, undefined behavior results.
        pub unsafe fn from_string_unchecked(s: String) -> Self {
            Self(s)
        }

        /// Creates a new `AsciiByteBuf` from the given bytes.
        ///
        /// # Safety
        ///
        /// The given bytes must be an ASCII string.
        /// If this constraint is violated, undefined behavior results.
        pub unsafe fn from_bytes_unchecked(bytes: Vec<u8>) -> Self {
            // SAFETY: `bytes` must be an ASCII string, and an ASCII string is
            // also a valid UTF-8 string.
            let s = String::from_utf8_unchecked(bytes);

            Self(s)
        }

        pub fn new<T: Into<Vec<u8>> + AsRef<[u8]>>(s: T) -> Result<Self, CreationError<T>> {
            match AsciiBytes::validate_bytes(s.as_ref()) {
                Ok(_) => Ok(unsafe {
                    // SAFETY: This is safe because the string is successfully validated.
                    Self::from_bytes_unchecked(s.into())
                }),
                Err(e) => Err(CreationError {
                    source: s,
                    error: e,
                }),
            }
        }
    }

    /// Methods to get borrowed `AsciiBytes`.
    impl AsciiByteBuf {
        #[inline]
        #[must_use]
        pub fn as_ascii_bytes(&self) -> &AsciiBytes {
            unsafe {
                // SAFETY: `self` is an ASCII string.
                AsciiBytes::new_unchecked(self.0.as_bytes())
            }
        }

        #[inline]
        #[must_use]
        pub fn as_ascii_bytes_mut(&mut self) -> &mut AsciiBytes {
            unsafe {
                // SAFETY: `self` is an ASCII string.
                AsciiBytes::from_mut_str_unchecked(self.0.as_mut_str())
            }
        }
    }

    impl core::borrow::Borrow<AsciiBytes> for AsciiByteBuf {
        #[inline]
        fn borrow(&self) -> &AsciiBytes {
            self.as_ascii_bytes()
        }
    }

    impl core::borrow::BorrowMut<AsciiBytes> for AsciiByteBuf {
        #[inline]
        fn borrow_mut(&mut self) -> &mut AsciiBytes {
            self.as_ascii_bytes_mut()
        }
    }

    impl alloc::borrow::ToOwned for AsciiBytes {
        type Owned = AsciiByteBuf;

        fn to_owned(&self) -> Self::Owned {
            let s = self.as_bytes();
            unsafe {
                // SAFETY: Valid `AsciiBytes` is also valid as `AsciiByteBuf`.
                AsciiByteBuf::from_bytes_unchecked(s.to_owned())
            }
        }
    }

    impl TryFrom<String> for AsciiByteBuf {
        type Error = CreationError<String>;

        #[inline]
        fn try_from(s: String) -> Result<Self, Self::Error> {
            Self::new(s)
        }
    }

    impl TryFrom<Vec<u8>> for AsciiByteBuf {
        type Error = CreationError<Vec<u8>>;

        #[inline]
        fn try_from(s: Vec<u8>) -> Result<Self, Self::Error> {
            Self::new(s)
        }
    }

    impl<'a> TryFrom<&'a str> for AsciiByteBuf {
        type Error = AsciiError;

        fn try_from(s: &'a str) -> Result<Self, Self::Error> {
            AsciiBytes::new(s).map(ToOwned::to_owned)
        }
    }

    impl<'a> TryFrom<&'a [u8]> for AsciiByteBuf {
        type Error = AsciiError;

        fn try_from(s: &'a [u8]) -> Result<Self, Self::Error> {
            AsciiBytes::new(s).map(ToOwned::to_owned)
        }
    }

    impl From<AsciiByteBuf> for String {
        #[inline]
        fn from(s: AsciiByteBuf) -> Self {
            s.0
        }
    }

    impl From<AsciiByteBuf> for Vec<u8> {
        #[inline]
        fn from(s: AsciiByteBuf) -> Self {
            s.0.into()
        }
    }

    impl From<&AsciiBytes> for AsciiByteBuf {
        #[inline]
        fn from(s: &AsciiBytes) -> Self {
            s.to_owned()
        }
    }

    impl From<&mut AsciiBytes> for AsciiByteBuf {
        #[inline]
        fn from(s: &mut AsciiBytes) -> Self {
            s.to_owned()
        }
    }

    impl From<&AsciiByteBuf> for AsciiByteBuf {
        #[inline]
        fn from(s: &AsciiByteBuf) -> Self {
            s.clone()
        }
    }

    impl From<&AsciiBytes> for Box<AsciiBytes> {
        fn from(s: &AsciiBytes) -> Self {
            // Create the boxed inner slice.
            let inner_box: Box<str> = Box::from(s.as_str());
            // Take the allocated memory out of the box, without releasing.
            let inner_boxed_ptr: *mut str = Box::into_raw(inner_box);

            unsafe {
                // SAFETY: The string is valid as `AsciiBytes`.
                // SAFETY: Memory layouts for `str` and `AsciiBytes` are compatible,
                // and `inner_boxed_ptr` is allocated by `Box::<str>::from()`.
                Box::from_raw(inner_boxed_ptr as *mut AsciiBytes)
            }
        }
    }

    impl From<&AsciiBytes> for Rc<AsciiBytes> {
        fn from(s: &AsciiBytes) -> Self {
            // Create the shared inner slice.
            let inner_box: Rc<str> = Rc::from(s.as_str());
            // Take the allocated memory out of the Rc, without releasing.
            let inner_boxed_ptr: *const str = Rc::into_raw(inner_box);

            unsafe {
                // SAFETY: The string is valid as `AsciiBytes`.
                // SAFETY: Memory layouts for `str` and `AsciiBytes` are compatible,
                // and `inner_boxed_ptr` is allocated by `Rc::<str>::from()`.
                Rc::from_raw(inner_boxed_ptr as *const AsciiBytes)
            }
        }
    }

    impl From<&AsciiBytes> for Arc<AsciiBytes> {
        fn from(s: &AsciiBytes) -> Self {
            // Create the shared inner slice.
            let inner_box: Arc<str> = Arc::from(s.as_str());
            // Take the allocated memory out of the Arc, without releasing.
            let inner_boxed_ptr: *const str = Arc::into_raw(inner_box);

            unsafe {
                // SAFETY: The string is valid as `AsciiBytes`.
                // SAFETY: Memory layouts for `str` and `AsciiBytes` are compatible,
                // and `inner_boxed_ptr` is allocated by `Arc::<str>::from()`.
                Arc::from_raw(inner_boxed_ptr as *const AsciiBytes)
            }
        }
    }

    impl From<AsciiByteBuf> for Box<AsciiBytes> {
        fn from(s: AsciiByteBuf) -> Self {
            // Create the boxed inner slice.
            let inner_box: Box<str> = Box::from(s.0);
            // Take the allocated memory out of the box, without releasing.
            let inner_boxed_ptr: *mut str = Box::into_raw(inner_box);

            unsafe {
                // SAFETY: The string is valid as `AsciiBytes`.
                // SAFETY: Memory layouts for `str` and `AsciiBytes` are compatible,
                // and `inner_boxed_ptr` is allocated by `Box::<str>::from()`.
                Box::from_raw(inner_boxed_ptr as *mut AsciiBytes)
            }
        }
    }

    impl From<AsciiByteBuf> for Rc<AsciiBytes> {
        fn from(s: AsciiByteBuf) -> Self {
            // Create the shared inner slice.
            let inner_box: Rc<str> = Rc::from(s.0);
            // Take the allocated memory out of the Rc, without releasing.
            let inner_boxed_ptr: *const str = Rc::into_raw(inner_box);

            unsafe {
                // SAFETY: The string is valid as `AsciiBytes`.
                // SAFETY: Memory layouts for `str` and `AsciiBytes` are compatible,
                // and `inner_boxed_ptr` is allocated by `Rc::<str>::from()`.
                Rc::from_raw(inner_boxed_ptr as *const AsciiBytes)
            }
        }
    }

    impl From<AsciiByteBuf> for Arc<AsciiBytes> {
        fn from(s: AsciiByteBuf) -> Self {
            // Create the shared inner slice.
            let inner_box: Arc<str> = Arc::from(s.0);
            // Take the allocated memory out of the Arc, without releasing.
            let inner_boxed_ptr: *const str = Arc::into_raw(inner_box);

            unsafe {
                // SAFETY: The string is valid as `AsciiBytes`.
                // SAFETY: Memory layouts for `str` and `AsciiBytes` are compatible,
                // and `inner_boxed_ptr` is allocated by `Arc::<str>::from()`.
                Arc::from_raw(inner_boxed_ptr as *const AsciiBytes)
            }
        }
    }

    impl TryFrom<&str> for Box<AsciiBytes> {
        type Error = AsciiError;

        fn try_from(s: &str) -> Result<Self, Self::Error> {
            AsciiBytes::new(s).map(Into::into)
        }
    }

    impl TryFrom<&[u8]> for Box<AsciiBytes> {
        type Error = AsciiError;

        fn try_from(s: &[u8]) -> Result<Self, Self::Error> {
            AsciiBytes::new(s).map(Into::into)
        }
    }

    impl TryFrom<Box<str>> for Box<AsciiBytes> {
        type Error = CreationError<Box<str>>;

        fn try_from(s: Box<str>) -> Result<Self, Self::Error> {
            if let Err(e) = AsciiBytes::validate_bytes(s.as_bytes()) {
                return Err(CreationError {
                    source: s,
                    error: e,
                });
            }

            let boxed_ptr: *mut str = Box::into_raw(s);
            Ok(unsafe {
                // SAFETY: The string is validated as `AsciiBytes`.
                // SAFETY: Memory layouts for `str` and `AsciiBytes` are compatible,
                // and `boxed_ptr` is a pointer from valid `Box<str>`.
                Box::from_raw(boxed_ptr as *mut AsciiBytes)
            })
        }
    }

    impl TryFrom<Box<[u8]>> for Box<AsciiBytes> {
        type Error = CreationError<Box<[u8]>>;

        fn try_from(s: Box<[u8]>) -> Result<Self, Self::Error> {
            if let Err(e) = AsciiBytes::validate_bytes(&s) {
                return Err(CreationError {
                    source: s,
                    error: e,
                });
            }

            // There is no direct `Box<[u8]>` to `Box<str>` method or trait impl.
            // Use `Box<[u8]>` -> `Vec<[u8]>` -> `String` -> `Box<MyStr>` route.
            // These conversions won't cause additional memory allocations.

            let string = unsafe {
                // SAFETY: The bytes is validated as `AsciiBytes`, and
                // valid `AsciiBytes` data is also valid UTF-8 bytes.
                String::from_utf8_unchecked(s.into_vec())
            };
            let boxed_ptr: *mut str = Box::into_raw(string.into_boxed_str());
            Ok(unsafe {
                // SAFETY: The string is validated as `AsciiBytes`.
                // SAFETY: Memory layouts for `str` and `AsciiBytes` are compatible,
                // and `boxed_ptr` is a pointer from valid `Box<str>`.
                Box::from_raw(boxed_ptr as *mut AsciiBytes)
            })
        }
    }

    impl TryFrom<String> for Box<AsciiBytes> {
        type Error = CreationError<String>;

        fn try_from(s: String) -> Result<Self, Self::Error> {
            if let Err(e) = AsciiBytes::validate_bytes(s.as_bytes()) {
                return Err(CreationError {
                    source: s,
                    error: e,
                });
            }

            let boxed_ptr: *mut str = Box::into_raw(s.into_boxed_str());
            Ok(unsafe {
                // SAFETY: The string is validated as `AsciiBytes`.
                // SAFETY: Memory layouts for `str` and `AsciiBytes` are compatible,
                // and `boxed_ptr` is a pointer from valid `Box<str>`.
                Box::from_raw(boxed_ptr as *mut AsciiBytes)
            })
        }
    }

    impl TryFrom<Vec<u8>> for Box<AsciiBytes> {
        type Error = CreationError<Vec<u8>>;

        fn try_from(s: Vec<u8>) -> Result<Self, Self::Error> {
            if let Err(e) = AsciiBytes::validate_bytes(&s) {
                return Err(CreationError {
                    source: s,
                    error: e,
                });
            }

            let string = unsafe {
                // SAFETY: The bytes is validated as `AsciiBytes`, and
                // valid `AsciiBytes` data is also valid UTF-8 bytes.
                String::from_utf8_unchecked(s)
            };
            let boxed_ptr: *mut str = Box::into_raw(string.into_boxed_str());
            Ok(unsafe {
                // SAFETY: The string is validated as `AsciiBytes`.
                // SAFETY: Memory layouts for `str` and `AsciiBytes` are compatible,
                // and `boxed_ptr` is a pointer from valid `Box<str>`.
                Box::from_raw(boxed_ptr as *mut AsciiBytes)
            })
        }
    }

    impl From<Box<AsciiBytes>> for AsciiByteBuf {
        fn from(s: Box<AsciiBytes>) -> Self {
            let boxed_str: Box<str> = s.into();
            let string: String = boxed_str.into();
            unsafe {
                // SAFETY: The string is valid as `AsciiBytes`,
                // and of course also as `AsciiByteBuf`.
                Self::from_string_unchecked(string)
            }
        }
    }

    impl From<Box<AsciiBytes>> for Box<str> {
        fn from(s: Box<AsciiBytes>) -> Self {
            let boxed_ptr: *mut AsciiBytes = Box::into_raw(s);
            unsafe {
                // SAFETY: The `AsciiBytes` bytes is also valid as `str`.
                // SAFETY: Memory layouts for `str` and `AsciiBytes` are compatible,
                // and `boxed_ptr` is a pointer from valid `Box<AsciiBytes>`.
                Box::from_raw(boxed_ptr as *mut str)
            }
        }
    }

    impl From<Box<AsciiBytes>> for Box<[u8]> {
        fn from(s: Box<AsciiBytes>) -> Self {
            let boxed_str: Box<str> = s.into();
            boxed_str.into()
        }
    }

    impl<'a> From<&'a AsciiBytes> for Cow<'a, AsciiBytes> {
        #[inline]
        fn from(s: &'a AsciiBytes) -> Self {
            Cow::Borrowed(s)
        }
    }

    impl From<AsciiByteBuf> for Cow<'_, AsciiBytes> {
        #[inline]
        fn from(s: AsciiByteBuf) -> Self {
            Cow::Owned(s)
        }
    }

    impl<'a> From<&'a AsciiByteBuf> for Cow<'a, AsciiBytes> {
        #[inline]
        fn from(s: &'a AsciiByteBuf) -> Self {
            Cow::Borrowed(s.as_ascii_bytes())
        }
    }

    impl From<Cow<'_, AsciiBytes>> for Box<AsciiBytes> {
        fn from(s: Cow<'_, AsciiBytes>) -> Self {
            match s {
                Cow::Borrowed(s) => s.into(),
                Cow::Owned(s) => s.into(),
            }
        }
    }

    impl From<Cow<'_, AsciiBytes>> for AsciiByteBuf {
        fn from(s: Cow<'_, AsciiBytes>) -> Self {
            match s {
                Cow::Borrowed(s) => s.to_owned(),
                Cow::Owned(s) => s,
            }
        }
    }

    impl core::str::FromStr for AsciiByteBuf {
        type Err = AsciiError;

        #[inline]
        fn from_str(s: &str) -> Result<Self, Self::Err> {
            TryFrom::try_from(s)
        }
    }

    impl core::str::FromStr for Box<AsciiBytes> {
        type Err = AsciiError;

        #[inline]
        fn from_str(s: &str) -> Result<Self, Self::Err> {
            TryFrom::try_from(s)
        }
    }

    impl core::ops::Deref for AsciiByteBuf {
        type Target = AsciiBytes;

        #[inline]
        fn deref(&self) -> &Self::Target {
            self.as_ascii_bytes()
        }
    }

    impl core::ops::DerefMut for AsciiByteBuf {
        #[inline]
        fn deref_mut(&mut self) -> &mut Self::Target {
            self.as_ascii_bytes_mut()
        }
    }

    impl AsRef<AsciiBytes> for AsciiByteBuf {
        #[inline]
        fn as_ref(&self) -> &AsciiBytes {
            self.as_ascii_bytes()
        }
    }

    impl AsMut<AsciiBytes> for AsciiByteBuf {
        #[inline]
        fn as_mut(&mut self) -> &mut AsciiBytes {
            self.as_ascii_bytes_mut()
        }
    }

    impl AsRef<str> for Box<AsciiBytes> {
        #[inline]
        fn as_ref(&self) -> &str {
            self.as_str()
        }
    }

    impl AsRef<str> for AsciiByteBuf {
        #[inline]
        fn as_ref(&self) -> &str {
            self.as_str()
        }
    }

    impl AsRef<[u8]> for Box<AsciiBytes> {
        #[inline]
        fn as_ref(&self) -> &[u8] {
            self.as_bytes()
        }
    }

    impl AsRef<[u8]> for AsciiByteBuf {
        #[inline]
        fn as_ref(&self) -> &[u8] {
            self.as_bytes()
        }
    }

    impl fmt::Debug for AsciiByteBuf {
        #[inline]
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            self.0.fmt(f)
        }
    }

    impl fmt::Display for AsciiByteBuf {
        #[inline]
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            self.0.fmt(f)
        }
    }

    impl Default for Box<AsciiBytes> {
        #[inline]
        fn default() -> Self {
            <&AsciiBytes>::default().into()
        }
    }

    impl_cmp_symmetric!(str, AsciiBytes, String);
    impl_cmp_symmetric!(str, AsciiBytes, &String);
    impl_cmp_symmetric!(str, AsciiBytes, Box<str>);
    impl_cmp_symmetric!(str, &AsciiBytes, Box<str>);
    impl_cmp_symmetric!(str, Box<AsciiBytes>, str);
    impl_cmp_symmetric!(str, Box<AsciiBytes>, &str);
    impl_cmp_symmetric!(str, AsciiBytes, Cow<'_, str>);
    impl_cmp_symmetric!(str, &AsciiBytes, Cow<'_, str>);
    impl_cmp_symmetric!([u8], AsciiBytes, Vec<u8>);
    impl_cmp_symmetric!([u8], AsciiBytes, &Vec<u8>);
    impl_cmp_symmetric!([u8], AsciiBytes, Box<[u8]>);
    impl_cmp_symmetric!([u8], &AsciiBytes, Box<[u8]>);
    impl_cmp_symmetric!([u8], Box<AsciiBytes>, [u8]);
    impl_cmp_symmetric!([u8], Box<AsciiBytes>, &[u8]);
    impl_cmp_symmetric!([u8], AsciiBytes, Cow<'_, [u8]>);
    impl_cmp_symmetric!([u8], &AsciiBytes, Cow<'_, [u8]>);

    impl_cmp_symmetric!(str, AsciiByteBuf, &AsciiByteBuf);
    impl_cmp_symmetric!(str, AsciiByteBuf, AsciiBytes);
    impl_cmp_symmetric!(str, AsciiByteBuf, &AsciiBytes);
    impl_cmp_symmetric!(str, &AsciiByteBuf, AsciiBytes);
    impl_cmp_symmetric!(str, AsciiByteBuf, str);
    impl_cmp_symmetric!(str, AsciiByteBuf, &str);
    impl_cmp_symmetric!(str, &AsciiByteBuf, str);
    impl_cmp_symmetric!([u8], AsciiByteBuf, [u8]);
    impl_cmp_symmetric!([u8], AsciiByteBuf, &[u8]);
    impl_cmp_symmetric!([u8], &AsciiByteBuf, [u8]);
    impl_cmp_symmetric!(str, AsciiByteBuf, String);
    impl_cmp_symmetric!(str, AsciiByteBuf, &String);
    impl_cmp_symmetric!(str, &AsciiByteBuf, String);
    impl_cmp_symmetric!([u8], AsciiByteBuf, Vec<u8>);
    impl_cmp_symmetric!([u8], AsciiByteBuf, &Vec<u8>);
    impl_cmp_symmetric!([u8], &AsciiByteBuf, Vec<u8>);

    #[cfg(feature = "serde")]
    impl<'de> serde::Deserialize<'de> for AsciiByteBuf {
        fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
        where
            D: serde::Deserializer<'de>,
        {
            /// Visitor for `AsciiByteBuf`.
            struct ByteBufVisitor;

            impl<'de> serde::de::Visitor<'de> for ByteBufVisitor {
                type Value = AsciiByteBuf;

                #[inline]
                fn expecting(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                    f.write_str("ASCII bytes")
                }

                #[inline]
                fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
                where
                    E: serde::de::Error,
                {
                    Self::Value::try_from(v).map_err(E::custom)
                }

                #[inline]
                fn visit_string<E>(self, v: String) -> Result<Self::Value, E>
                where
                    E: serde::de::Error,
                {
                    Self::Value::try_from(v).map_err(E::custom)
                }

                #[inline]
                fn visit_bytes<E>(self, v: &[u8]) -> Result<Self::Value, E>
                where
                    E: serde::de::Error,
                {
                    Self::Value::try_from(v).map_err(E::custom)
                }

                #[inline]
                fn visit_byte_buf<E>(self, v: Vec<u8>) -> Result<Self::Value, E>
                where
                    E: serde::de::Error,
                {
                    Self::Value::try_from(v).map_err(E::custom)
                }
            }

            deserializer.deserialize_any(ByteBufVisitor)
        }
    }
}
