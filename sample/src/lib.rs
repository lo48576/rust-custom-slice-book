//! Examples for rust-custom-slice-book.
#![cfg_attr(not(feature = "std"), no_std)]

#[cfg(all(
    feature = "serde",
    feature = "alloc",
    not(feature = "std"),
    not(any(feature = "serde-alloc", feature = "serde-std"))
))]
compile_error!(
    "When both `serde` and `alloc` features are enabled, \
     `serde-alloc` or `serde-std` should also be enabled."
);

#[cfg(all(feature = "serde", feature = "std", not(feature = "serde-std")))]
compile_error!(
    "When both `serde` and `std` features are enabled, `serde-std` should also be enabled."
);

#[cfg(feature = "alloc")]
extern crate alloc;

pub mod ascii_bytes;
pub mod ascii_str;
pub mod my_str;
