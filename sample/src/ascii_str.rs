//! ASCII string.

use core::convert::TryFrom;
use core::fmt;

#[cfg(feature = "alloc")]
pub use self::owned::{AsciiString, FromStringError};

/// Error for conversion from bytes to ASCII string.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct AsciiError {
    /// Byte position up to where the string is valid.
    valid_up_to: usize,
    /// Value of the first invalid byte.
    invalid_byte: u8,
}

impl AsciiError {
    /// Returns the byte position up to where the string is valid.
    #[inline]
    #[must_use]
    pub fn valid_up_to(&self) -> usize {
        self.valid_up_to
    }

    /// Returns the value of the first invalid byte.
    #[inline]
    #[must_use]
    pub fn invalid_byte(&self) -> u8 {
        self.invalid_byte
    }
}

impl fmt::Display for AsciiError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "invalid ASCII character {:#02x?} found at index {}",
            self.invalid_byte, self.valid_up_to
        )
    }
}

#[cfg(feature = "std")]
impl std::error::Error for AsciiError {}

/// ASCII string slice type.
#[derive(PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(transparent)]
// Comparisons implemented for the type are consistent (at least it is intended to be so).
// See <https://github.com/rust-lang/rust-clippy/issues/2025>.
#[allow(clippy::derive_hash_xor_eq, clippy::derive_ord_xor_partial_ord)]
#[cfg_attr(feature = "serde", derive(serde::Serialize))]
#[cfg_attr(feature = "serde", serde(transparent))]
pub struct AsciiStr(str);

/// Validation method.
impl AsciiStr {
    /// Checks whether the given string is an ASCII string.
    fn validate(s: &str) -> Result<(), AsciiError> {
        match s.bytes().enumerate().find(|(_pos, byte)| !byte.is_ascii()) {
            Some((pos, byte)) => Err(AsciiError {
                valid_up_to: pos,
                invalid_byte: byte,
            }),
            None => Ok(()),
        }
    }
}

/// Methods to create an `AsciiStr` string without validation.
impl AsciiStr {
    /// Creates a new ASCII string slice from the given UTF-8 string slice.
    ///
    /// # Safety
    ///
    /// The given bytes should consists of only ASCII characters.
    /// If this constraint is violated, undefined behavior results.
    ///
    /// # Examples
    ///
    /// ```
    /// # use custom_slice_book_sample::ascii_str::AsciiStr;
    /// let s: &AsciiStr = unsafe {
    ///     AsciiStr::new_unchecked("hello")
    /// };
    /// ```
    #[inline]
    #[must_use]
    pub unsafe fn new_unchecked(s: &str) -> &Self {
        &*(s as *const str as *const Self)
    }

    /// Creates a new mutable ASCII string slice from the given mutable UTF-8 string slice.
    ///
    /// # Safety
    ///
    /// The given bytes should consists of only ASCII characters.
    /// If this constraint is violated, undefined behavior results.
    ///
    /// # Examples
    ///
    /// ```
    /// # use custom_slice_book_sample::ascii_str::AsciiStr;
    /// let mut s: String = "hello".to_owned();
    /// let t: &mut AsciiStr = unsafe {
    ///     AsciiStr::new_unchecked_mut(&mut s)
    /// };
    /// ```
    #[inline]
    #[must_use]
    pub unsafe fn new_unchecked_mut(s: &mut str) -> &mut Self {
        &mut *(s as *mut str as *mut Self)
    }
}

/// Methods to create an `AsciiStr` string safely.
impl AsciiStr {
    /// Creates a new ASCII string slice from the given UTF-8 string slice.
    ///
    /// # Examples
    ///
    /// ```
    /// # use custom_slice_book_sample::ascii_str::AsciiStr;
    /// assert!(AsciiStr::new("hello").is_ok());
    ///
    /// assert!(AsciiStr::new("\u{FFFD}").is_err());
    /// ```
    pub fn new(s: &str) -> Result<&Self, AsciiError> {
        match Self::validate(s) {
            Ok(_) => Ok(unsafe {
                // SAFETY: This is safe because the string is successfully validated.
                Self::new_unchecked(s)
            }),
            Err(e) => Err(e),
        }
    }

    /// Creates a new mutable ASCII string slice from the given mutable UTF-8 string slice.
    ///
    /// # Examples
    ///
    /// ```
    /// # use custom_slice_book_sample::ascii_str::AsciiStr;
    /// let mut ascii = "hello".to_owned();
    /// assert!(AsciiStr::new_mut(&mut ascii).is_ok());
    ///
    /// let mut non_ascii = "\u{FFFD}".to_owned();
    /// assert!(AsciiStr::new_mut(&mut non_ascii).is_err());
    /// ```
    pub fn new_mut(s: &mut str) -> Result<&mut Self, AsciiError> {
        match Self::validate(s) {
            Ok(_) => Ok(unsafe {
                // SAFETY: This is safe because the string is successfully validated.
                Self::new_unchecked_mut(s)
            }),
            Err(e) => Err(e),
        }
    }
}

/// Methods to get the inner value.
impl AsciiStr {
    /// Returns a reference to the inner string.
    ///
    /// # Examples
    ///
    /// ```
    /// # use custom_slice_book_sample::ascii_str::{AsciiError, AsciiStr};
    /// let s = AsciiStr::new("hello")?;
    ///
    /// let inner: &str = s.as_str();
    /// assert_eq!(inner, "hello");
    /// # Ok::<_, AsciiError>(())
    /// ```
    #[inline]
    #[must_use]
    pub fn as_str(&self) -> &str {
        &self.0
    }

    /// Returns the mutable reference to the inner string.
    ///
    /// # Safety
    ///
    /// The caller must ensure that the string is an ASCII string when the borrow ends.
    ///
    /// Use of an `AsciiStr` which contains non-ASCII characters is undefined behavior.
    ///
    /// # Examples
    ///
    /// ```
    /// # use custom_slice_book_sample::ascii_str::{AsciiError, AsciiStr};
    /// let mut ascii = "hello".to_owned();
    /// let s = AsciiStr::new_mut(&mut ascii)?;
    /// assert_eq!(s.as_str(), "hello");
    ///
    /// unsafe {
    ///     let inner = s.as_mut_str();
    ///     // SAFETY: `<str>::make_ascii_upper()` does not increase non-ASCII characters.
    ///     inner.make_ascii_uppercase();
    /// }
    /// assert_eq!(s.as_str(), "HELLO");
    /// assert_eq!(ascii, "HELLO");
    /// # Ok::<_, AsciiError>(())
    /// ```
    #[inline]
    #[must_use]
    pub unsafe fn as_mut_str(&mut self) -> &mut str {
        &mut self.0
    }
}

impl<'a> TryFrom<&'a str> for &'a AsciiStr {
    type Error = AsciiError;

    #[inline]
    fn try_from(s: &'a str) -> Result<Self, Self::Error> {
        AsciiStr::new(s)
    }
}

impl<'a> TryFrom<&'a mut str> for &'a mut AsciiStr {
    type Error = AsciiError;

    #[inline]
    fn try_from(s: &'a mut str) -> Result<Self, Self::Error> {
        AsciiStr::new_mut(s)
    }
}

impl<'a> From<&'a AsciiStr> for &'a str {
    #[inline]
    fn from(s: &'a AsciiStr) -> Self {
        s.as_str()
    }
}

impl AsRef<AsciiStr> for AsciiStr {
    #[inline]
    fn as_ref(&self) -> &AsciiStr {
        self
    }
}

impl AsMut<AsciiStr> for AsciiStr {
    #[inline]
    fn as_mut(&mut self) -> &mut AsciiStr {
        self
    }
}

impl AsRef<str> for AsciiStr {
    #[inline]
    fn as_ref(&self) -> &str {
        self.as_str()
    }
}

impl fmt::Debug for AsciiStr {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl fmt::Display for AsciiStr {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl Default for &AsciiStr {
    fn default() -> Self {
        unsafe {
            // SAFETY: An empty string is valid ASCII string.
            AsciiStr::new_unchecked(<&str>::default())
        }
    }
}

impl Default for &mut AsciiStr {
    fn default() -> Self {
        unsafe {
            // SAFETY: An empty string is valid ASCII string.
            AsciiStr::new_unchecked_mut(<&mut str>::default())
        }
    }
}

/// Implement `PartialEq` and `Eq` for the given types.
macro_rules! impl_cmp {
    ($ty_lhs:ty, $ty_rhs:ty) => {
        impl PartialEq<$ty_rhs> for $ty_lhs {
            #[inline]
            fn eq(&self, o: &$ty_rhs) -> bool {
                <str as PartialEq<str>>::eq(AsRef::as_ref(self), AsRef::as_ref(o))
            }
        }
        impl PartialOrd<$ty_rhs> for $ty_lhs {
            #[inline]
            fn partial_cmp(&self, o: &$ty_rhs) -> Option<core::cmp::Ordering> {
                <str as PartialOrd<str>>::partial_cmp(AsRef::as_ref(self), AsRef::as_ref(o))
            }
        }
    };
}

/// Implement `PartialEq` and `Eq` symmetrically for the given types.
macro_rules! impl_cmp_symmetric {
    ($ty_lhs:ty, $ty_rhs:ty) => {
        impl_cmp!($ty_lhs, $ty_rhs);
        impl_cmp!($ty_rhs, $ty_lhs);
    };
}

impl_cmp_symmetric!(AsciiStr, str);
impl_cmp_symmetric!(AsciiStr, &str);
impl_cmp_symmetric!(&AsciiStr, str);

#[cfg(feature = "serde")]
impl<'de> serde::Deserialize<'de> for &'de AsciiStr {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        /// Visitor for `&AsciiStr`.
        struct StrVisitor;

        impl<'de> serde::de::Visitor<'de> for StrVisitor {
            type Value = &'de AsciiStr;

            #[inline]
            fn expecting(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                f.write_str("an ASCII string")
            }

            #[inline]
            fn visit_borrowed_str<E>(self, v: &'de str) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                Self::Value::try_from(v).map_err(E::custom)
            }
        }

        deserializer.deserialize_str(StrVisitor)
    }
}

/// Owned types and related trait impls.
#[cfg(feature = "alloc")]
mod owned {
    use super::*;

    use alloc::borrow::{Cow, ToOwned};
    use alloc::boxed::Box;
    use alloc::rc::Rc;
    use alloc::string::String;
    use alloc::sync::Arc;

    /// Error for conversion from std string to ASCII string.
    #[derive(Debug, Clone)]
    pub struct FromStringError {
        /// Source string which cannot be converted.
        source: String,
        /// Conversion error.
        error: AsciiError,
    }

    impl FromStringError {
        /// Returns a reference to the source string.
        #[inline]
        #[must_use]
        pub fn as_str(&self) -> &str {
            &self.source
        }

        /// Retruns the source string.
        #[inline]
        #[must_use]
        pub fn into_string(self) -> String {
            self.source
        }

        /// Returns the inner conversion error.
        #[inline]
        #[must_use]
        pub fn ascii_error(&self) -> AsciiError {
            self.error
        }
    }

    impl fmt::Display for FromStringError {
        #[inline]
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            self.error.fmt(f)
        }
    }

    #[cfg(feature = "std")]
    impl std::error::Error for FromStringError {}

    /// My custom owned ASCII string type.
    #[derive(Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
    // Comparisons implemented for the type are consistent (at least it is intended to be so).
    // See <https://github.com/rust-lang/rust-clippy/issues/2025>.
    #[allow(clippy::derive_hash_xor_eq, clippy::derive_ord_xor_partial_ord)]
    #[cfg_attr(feature = "serde", derive(serde::Serialize))]
    #[cfg_attr(feature = "serde", serde(transparent))]
    pub struct AsciiString(String);

    /// Methods to create an `AsciiString` string.
    impl AsciiString {
        /// Creates a new `AsciiString` from the given string.
        ///
        /// # Safety
        ///
        /// The given string must be an ASCII string.
        /// If this constraint is violated, undefined behavior results.
        #[inline]
        #[must_use]
        pub unsafe fn new_unchecked(s: String) -> Self {
            Self(s)
        }

        pub fn new(s: String) -> Result<Self, FromStringError> {
            match AsciiStr::validate(&s) {
                Ok(_) => Ok(unsafe {
                    // SAFETY: This is safe because the string is successfully validated.
                    Self::new_unchecked(s)
                }),
                Err(e) => Err(FromStringError {
                    source: s,
                    error: e,
                }),
            }
        }
    }

    /// Methods to get borrowed `AsciiStr`.
    impl AsciiString {
        #[inline]
        #[must_use]
        pub fn as_ascii_str(&self) -> &AsciiStr {
            unsafe {
                // SAFETY: `self` is an ASCII string.
                AsciiStr::new_unchecked(self.0.as_str())
            }
        }

        #[inline]
        #[must_use]
        pub fn as_ascii_str_mut(&mut self) -> &mut AsciiStr {
            unsafe {
                // SAFETY: `self` is an ASCII string.
                AsciiStr::new_unchecked_mut(self.0.as_mut_str())
            }
        }
    }

    impl core::borrow::Borrow<AsciiStr> for AsciiString {
        #[inline]
        fn borrow(&self) -> &AsciiStr {
            self.as_ascii_str()
        }
    }

    impl core::borrow::BorrowMut<AsciiStr> for AsciiString {
        #[inline]
        fn borrow_mut(&mut self) -> &mut AsciiStr {
            self.as_ascii_str_mut()
        }
    }

    impl alloc::borrow::ToOwned for AsciiStr {
        type Owned = AsciiString;

        fn to_owned(&self) -> Self::Owned {
            let s = self.as_str();
            unsafe {
                // SAFETY: Valid `AsciiStr` string is also valid as `AsciiString`.
                AsciiString::new_unchecked(s.to_owned())
            }
        }
    }

    impl TryFrom<String> for AsciiString {
        type Error = FromStringError;

        #[inline]
        fn try_from(s: String) -> Result<Self, Self::Error> {
            Self::new(s)
        }
    }

    impl TryFrom<&str> for AsciiString {
        type Error = AsciiError;

        fn try_from(s: &str) -> Result<Self, Self::Error> {
            AsciiStr::new(s).map(ToOwned::to_owned)
        }
    }

    impl From<AsciiString> for String {
        #[inline]
        fn from(s: AsciiString) -> Self {
            s.0
        }
    }

    impl From<&AsciiStr> for AsciiString {
        #[inline]
        fn from(s: &AsciiStr) -> Self {
            s.to_owned()
        }
    }

    impl From<&mut AsciiStr> for AsciiString {
        #[inline]
        fn from(s: &mut AsciiStr) -> Self {
            s.to_owned()
        }
    }

    impl From<&AsciiString> for AsciiString {
        #[inline]
        fn from(s: &AsciiString) -> Self {
            s.clone()
        }
    }

    impl From<&AsciiStr> for Box<AsciiStr> {
        fn from(s: &AsciiStr) -> Self {
            // Create the boxed inner slice.
            let inner_box: Box<str> = Box::from(s.as_str());
            // Take the allocated memory out of the box, without releasing.
            let inner_boxed_ptr: *mut str = Box::into_raw(inner_box);

            unsafe {
                // SAFETY: The string is valid as `AsciiStr`.
                // SAFETY: Memory layouts for `str` and `AsciiStr` are compatible,
                // and `inner_boxed_ptr` is allocated by `Box::<str>::from()`.
                Box::from_raw(inner_boxed_ptr as *mut AsciiStr)
            }
        }
    }

    impl From<&AsciiStr> for Rc<AsciiStr> {
        fn from(s: &AsciiStr) -> Self {
            // Create the shared inner slice.
            let inner_box: Rc<str> = Rc::from(s.as_str());
            // Take the allocated memory out of the Rc, without releasing.
            let inner_boxed_ptr: *const str = Rc::into_raw(inner_box);

            unsafe {
                // SAFETY: The string is valid as `AsciiStr`.
                // SAFETY: Memory layouts for `str` and `AsciiStr` are compatible,
                // and `inner_boxed_ptr` is allocated by `Rc::<str>::from()`.
                Rc::from_raw(inner_boxed_ptr as *const AsciiStr)
            }
        }
    }

    impl From<&AsciiStr> for Arc<AsciiStr> {
        fn from(s: &AsciiStr) -> Self {
            // Create the shared inner slice.
            let inner_box: Arc<str> = Arc::from(s.as_str());
            // Take the allocated memory out of the Arc, without releasing.
            let inner_boxed_ptr: *const str = Arc::into_raw(inner_box);

            unsafe {
                // SAFETY: The string is valid as `AsciiStr`.
                // SAFETY: Memory layouts for `str` and `AsciiStr` are compatible,
                // and `inner_boxed_ptr` is allocated by `Arc::<str>::from()`.
                Arc::from_raw(inner_boxed_ptr as *const AsciiStr)
            }
        }
    }

    impl From<AsciiString> for Box<AsciiStr> {
        fn from(s: AsciiString) -> Self {
            // Create the boxed inner slice.
            let inner_box: Box<str> = Box::from(s.0);
            // Take the allocated memory out of the box, without releasing.
            let inner_boxed_ptr: *mut str = Box::into_raw(inner_box);

            unsafe {
                // SAFETY: The string is valid as `AsciiStr`.
                // SAFETY: Memory layouts for `str` and `AsciiStr` are compatible,
                // and `inner_boxed_ptr` is allocated by `Box::<str>::from()`.
                Box::from_raw(inner_boxed_ptr as *mut AsciiStr)
            }
        }
    }

    impl From<AsciiString> for Rc<AsciiStr> {
        fn from(s: AsciiString) -> Self {
            // Create the shared inner slice.
            let inner_box: Rc<str> = Rc::from(s.0);
            // Take the allocated memory out of the Rc, without releasing.
            let inner_boxed_ptr: *const str = Rc::into_raw(inner_box);

            unsafe {
                // SAFETY: The string is valid as `AsciiStr`.
                // SAFETY: Memory layouts for `str` and `AsciiStr` are compatible,
                // and `inner_boxed_ptr` is allocated by `Rc::<str>::from()`.
                Rc::from_raw(inner_boxed_ptr as *const AsciiStr)
            }
        }
    }

    impl From<AsciiString> for Arc<AsciiStr> {
        fn from(s: AsciiString) -> Self {
            // Create the shared inner slice.
            let inner_box: Arc<str> = Arc::from(s.0);
            // Take the allocated memory out of the Arc, without releasing.
            let inner_boxed_ptr: *const str = Arc::into_raw(inner_box);

            unsafe {
                // SAFETY: The string is valid as `AsciiStr`.
                // SAFETY: Memory layouts for `str` and `AsciiStr` are compatible,
                // and `inner_boxed_ptr` is allocated by `Arc::<str>::from()`.
                Arc::from_raw(inner_boxed_ptr as *const AsciiStr)
            }
        }
    }

    impl TryFrom<&str> for Box<AsciiStr> {
        type Error = AsciiError;

        fn try_from(s: &str) -> Result<Self, Self::Error> {
            AsciiStr::new(s).map(Into::into)
        }
    }

    impl From<Box<AsciiStr>> for AsciiString {
        fn from(s: Box<AsciiStr>) -> Self {
            let boxed_str: Box<str> = s.into();
            let string: String = boxed_str.into();
            unsafe {
                // SAFETY: The string is valid as `AsciiStr`,
                // and of course also as `AsciiString`.
                Self::new_unchecked(string)
            }
        }
    }

    impl From<Box<AsciiStr>> for Box<str> {
        fn from(s: Box<AsciiStr>) -> Self {
            let boxed_ptr: *mut AsciiStr = Box::into_raw(s);
            unsafe {
                // SAFETY: The `AsciiStr` string is also valid as `str`.
                // SAFETY: Memory layouts for `str` and `AsciiStr` are compatible,
                // and `boxed_ptr` is a pointer from valid `Box<AsciiStr>`.
                Box::from_raw(boxed_ptr as *mut str)
            }
        }
    }

    impl<'a> From<&'a AsciiStr> for Cow<'a, AsciiStr> {
        #[inline]
        fn from(s: &'a AsciiStr) -> Self {
            Cow::Borrowed(s)
        }
    }

    impl From<AsciiString> for Cow<'_, AsciiStr> {
        #[inline]
        fn from(s: AsciiString) -> Self {
            Cow::Owned(s)
        }
    }

    impl<'a> From<&'a AsciiString> for Cow<'a, AsciiStr> {
        #[inline]
        fn from(s: &'a AsciiString) -> Self {
            Cow::Borrowed(s.as_ascii_str())
        }
    }

    impl From<Cow<'_, AsciiStr>> for Box<AsciiStr> {
        fn from(s: Cow<'_, AsciiStr>) -> Self {
            match s {
                Cow::Borrowed(s) => s.into(),
                Cow::Owned(s) => s.into(),
            }
        }
    }

    impl From<Cow<'_, AsciiStr>> for AsciiString {
        fn from(s: Cow<'_, AsciiStr>) -> Self {
            match s {
                Cow::Borrowed(s) => s.to_owned(),
                Cow::Owned(s) => s,
            }
        }
    }

    impl core::str::FromStr for AsciiString {
        type Err = AsciiError;

        #[inline]
        fn from_str(s: &str) -> Result<Self, Self::Err> {
            TryFrom::try_from(s)
        }
    }

    impl core::str::FromStr for Box<AsciiStr> {
        type Err = AsciiError;

        #[inline]
        fn from_str(s: &str) -> Result<Self, Self::Err> {
            TryFrom::try_from(s)
        }
    }

    impl core::ops::Deref for AsciiString {
        type Target = AsciiStr;

        #[inline]
        fn deref(&self) -> &Self::Target {
            self.as_ascii_str()
        }
    }

    impl core::ops::DerefMut for AsciiString {
        #[inline]
        fn deref_mut(&mut self) -> &mut Self::Target {
            self.as_ascii_str_mut()
        }
    }

    impl AsRef<AsciiStr> for AsciiString {
        #[inline]
        fn as_ref(&self) -> &AsciiStr {
            self.as_ascii_str()
        }
    }

    impl AsMut<AsciiStr> for AsciiString {
        #[inline]
        fn as_mut(&mut self) -> &mut AsciiStr {
            self.as_ascii_str_mut()
        }
    }

    impl AsRef<str> for Box<AsciiStr> {
        #[inline]
        fn as_ref(&self) -> &str {
            self.as_str()
        }
    }

    impl AsRef<str> for AsciiString {
        #[inline]
        fn as_ref(&self) -> &str {
            self.as_str()
        }
    }

    impl fmt::Debug for AsciiString {
        #[inline]
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            self.0.fmt(f)
        }
    }

    impl fmt::Display for AsciiString {
        #[inline]
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            self.0.fmt(f)
        }
    }

    impl Default for Box<AsciiStr> {
        #[inline]
        fn default() -> Self {
            <&AsciiStr>::default().into()
        }
    }

    impl_cmp_symmetric!(AsciiStr, String);
    impl_cmp_symmetric!(AsciiStr, &String);
    impl_cmp_symmetric!(AsciiStr, Box<str>);
    impl_cmp_symmetric!(&AsciiStr, Box<str>);
    impl_cmp_symmetric!(Box<AsciiStr>, str);
    impl_cmp_symmetric!(Box<AsciiStr>, &str);
    impl_cmp_symmetric!(AsciiStr, Cow<'_, str>);
    impl_cmp_symmetric!(&AsciiStr, Cow<'_, str>);

    impl_cmp_symmetric!(AsciiString, &AsciiString);
    impl_cmp_symmetric!(AsciiString, AsciiStr);
    impl_cmp_symmetric!(AsciiString, &AsciiStr);
    impl_cmp_symmetric!(&AsciiString, AsciiStr);
    impl_cmp_symmetric!(AsciiString, str);
    impl_cmp_symmetric!(AsciiString, &str);
    impl_cmp_symmetric!(&AsciiString, str);
    impl_cmp_symmetric!(AsciiString, String);
    impl_cmp_symmetric!(AsciiString, &String);
    impl_cmp_symmetric!(&AsciiString, String);

    #[cfg(feature = "serde")]
    impl<'de> serde::Deserialize<'de> for AsciiString {
        fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
        where
            D: serde::Deserializer<'de>,
        {
            /// Visitor for `AsciiString`.
            struct StringVisitor;

            impl<'de> serde::de::Visitor<'de> for StringVisitor {
                type Value = AsciiString;

                #[inline]
                fn expecting(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                    f.write_str("an ASCII string")
                }

                #[inline]
                fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
                where
                    E: serde::de::Error,
                {
                    Self::Value::try_from(v).map_err(E::custom)
                }

                #[inline]
                fn visit_string<E>(self, v: String) -> Result<Self::Value, E>
                where
                    E: serde::de::Error,
                {
                    Self::Value::try_from(v).map_err(E::custom)
                }
            }

            deserializer.deserialize_string(StringVisitor)
        }
    }
}
