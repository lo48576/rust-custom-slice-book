# Rust で独自のスライス型を定義する本

[![pipeline status](https://gitlab.com/lo48576/rust-custom-slice-book/badges/master/pipeline.svg)](https://gitlab.com/lo48576/rust-custom-slice-book/-/commits/master)
![Minimum rustc version: 1.49](https://img.shields.io/badge/rustc-1.49+-lightgray.svg)

Rust で独自のスライス型 (文字列型を含む) を定義するための解説とコード例を含む本。

GitLab サーバで生成されたページが <https://lo48576.gitlab.io/rust-custom-slice-book/> で閲覧できる。

## ライセンス

### 文書

![Licensed under CC-By 4.0.](cc-by-4.0.svg)

### サンプルプログラムのソースコード

![Licensed under CC0 1.0.](cc0-1.0.svg)

### 貢献について

貢献者本人による明示のない限り、本リポジトリに取り入れるために提出された成果物は、追加の条件なしに上記のとおり [CC-BY 4.0][CC-BY-4.0] および [CC0 1.0][CC0-1.0] でライセンスされるものとする。

[CC-BY-4.0]: https://creativecommons.org/licenses/by/4.0/deed.ja
[CC0-1.0]: https://creativecommons.org/publicdomain/zero/1.0/deed.ja
[mdBook]: https://github.com/rust-lang/mdBook
