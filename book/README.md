# Rust で独自のスライス型を定義する本

Rust で独自のスライス型 (文字列型を含む) を定義するための解説とコード例を含む本。

## リモートでの閲覧方法

GitLab サーバで生成されたページが <https://lo48576.gitlab.io/rust-custom-slice-book/> で閲覧できる。

## ローカルでの利用方法

この本は [mdBook] を利用して書かれている。

1. [mdBook] をインストールする。
2. このディレクトリ (`book/`) で `mdbook serve` を実行する。
3. `http://localhost:3000/` にアクセスする。

[mdBook]: https://github.com/rust-lang/mdBook
