# スライス型

スライス型と DST についても説明しておこう。

Rust においては [`[T]`][slice] や [`str`] 、 [`std::path::Path`][`Path`] のように、値そのもののサイズ (長さ) が不定な型が存在する。
これらを DST (Dynamically Sized Types) と呼ぶ。
こういった型は `&[T]` や `&str` のように参照型を通して扱うことになるが、これらの参照型をスライス型と呼ぶ[^footnote-slice-type-in-a-narrow-sense]。
スライス型の値 (すなわち参照) は低レベルで表現されるとき単なるポインタではなく、ポインタと長さの組である[^footnote-fat-pointer]。

> A dynamically-sized view into a contiguous sequence, `[T]`.
>
> ——slice プリミティブ型について、[標準ライブラリのリファレンス](https://doc.rust-lang.org/1.48.0/std/primitive.slice.html)より

DST はサイズ不定であるゆえ、通常の型とは扱いが異なる。
典型的には、(少なくとも現状 (Rust 1.48.0) では[^footnote-unsized-local-values-in-future])参照でない生の値として扱えないなどの制限がある。

```rust,ignore
// コンパイル時にサイズが定まらない DST は生で変数に入れられない。

fn main() {
    let s: str = *"foo";
}
```

```text
   Compiling playground v0.0.1 (/playground)
error[E0277]: the size for values of type `str` cannot be known at compilation time
 --> src/main.rs:2:9
  |
2 |     let s: str = *"foo";
  |         ^ doesn't have a size known at compile-time
  |
  = help: the trait `Sized` is not implemented for `str`
  = note: all local variables must have a statically known size
  = help: unsized locals are gated as an unstable feature

error: aborting due to previous error

For more information about this error, try `rustc --explain E0277`.
error: could not compile `playground`

To learn more, run the command again with --verbose.
```

このように特殊な性質を持つ DST は、定義やメソッド定義、トレイト実装等に注意や工夫が必要であるため、本書ではそれらを紹介する。


* * *

[^footnote-slice-type-in-a-narrow-sense]: もしかすると狭義のスライス型とは `&[T]` のみを指すかもしれないが、注釈なしだと一般には `&str` 等も含む、もう少し広義の型を含めるものと理解している

[^footnote-fat-pointer]: 通常のサイズ固定の型 (`i32` 等) の参照は機械語レベルでは単なるポインタ (メモリアドレス) であるが、ポインタと長さの組となると、ポインタ単体に比べてサイズが2倍となる。そのため、後者を fat pointer と呼ぶ。

[^footnote-unsized-local-values-in-future]: 将来的に制限が緩和される可能性はある。詳しくは [RFC 1909] 等を参照。

[RFC 1909]: https://rust-lang.github.io/rfcs/1909-unsized-rvalues.html
[`Path`]: https://doc.rust-lang.org/stable/std/path/struct.Path.html
[`str`]: https://doc.rust-lang.org/stable/std/primitive.str.html
[slice]: https://doc.rust-lang.org/stable/std/primitive.slice.html
