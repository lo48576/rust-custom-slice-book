# 2種類の独自スライス型

本質的に大した差ではないが、お気持ちのうえで、また実装や設計のうえで独自スライス型は2種類に分類できる。
ひとつは、情報の欠落なしに元となる型と相互に変換できる、追加の制約なしの型。
もうひとつは、相互の変換で情報の欠落があったり、変換の失敗がありえるような、追加の制約付きの型である。

たとえば `str` は「`[u8]` で表現可能なバイト列のうち、 UTF-8 バイト列として妥当なもの」という追加の制約付きの型である。
`&str` の値は無条件に `&[u8]` に変換可能であるが、逆は失敗する可能性がある。

対照的に、 [`std::path::Path`][`Path`] と [`std::ffi::OsStr`][`OsStr`] が表現可能な情報の範囲は低レベルにおいては相互に等価であり、 [`AsRef::as_ref()`][`AsRef::as_ref`] を通して失敗と欠落なしに相互に変換できる。 `std::ffi::OsStr` 自体がとりうる値には制約があるが、これを `std::path::Path` へと変換する際に追加の制約を与えられることはない。

本記事では、これらの2種類の型の例として、 `str` を追加の制約なしに strong typedef した `MyStr` と、「ASCII 文字しか持っていない」という追加の制約付きの `AsciiStr` および `AsciiBytes` 型を用いる。
また、これらに対応する所有権付きの型 (たとえば `str` に対する `String`) として、 `MyString` 型と `AsciiString` および `AsciiByteBuf` 型も定義していく。


[`AsRef::as_ref`]: https://doc.rust-lang.org/stable/std/convert/trait.AsRef.html#tymethod.as_ref
[`OsStr`]: https://doc.rust-lang.org/stable/std/ffi/struct.OsStr.html
[`Path`]: https://doc.rust-lang.org/stable/std/path/struct.Path.html
