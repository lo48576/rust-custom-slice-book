# 元の型へのキャスト

元の型への (mutable 参照でない) キャストは基本的に失敗しないため、 safe かつ単純に書ける。

```rust
// 内部のスライス型への変換は、単純にフィールドを参照するだけである。

# #[repr(transparent)]
# pub struct MyStr(str);
#
impl MyStr {
    #[inline]
    #[must_use]
    pub fn as_str(&self) -> &str {
        &self.0
    }

    #[inline]
    #[must_use]
    pub fn as_mut_str(&mut self) -> &mut str {
        &mut self.0
    }
}
```

ここで注意すべきなのは、**元の型に追加の制約を加えた型の場合、元の型への可変参照を safe に返してはいけない**ということである。

```rust
// `as_mut_str` が unsafe であることに注目。

# #[repr(transparent)]
# pub struct AsciiStr(str);
#
impl AsciiStr {
    #[inline]
    #[must_use]
    pub fn as_str(&self) -> &str {
        &self.0
    }

    /// Returns the mutable reference to the inner string.
    ///
    /// # Safety
    ///
    /// The caller must ensure that the string is an ASCII string when the borrow ends.
    ///
    /// Use of an `AsciiStr` which contains non-ASCII characters is undefined behavior.
    #[inline]
    #[must_use]
    pub unsafe fn as_mut_str(&mut self) -> &mut str {
        &mut self.0
    }
}
```

`AsciiStr` は値の作成や編集の際に適切な検査を行うことで `str` への追加の制約を遵守する必要がある。
しかし、内部の `&mut str` を直接取り出してユーザが編集を行うと、 `AsciiStr` 型がその内容を検査することはできなくなる[^footnote-ensuring-validity-after-arbitrary-modification]。
そのため、編集が完了して値へのコントロールが `AsciiStr` へと戻ってきた時点で内容が妥当なものであることを条件として、 unsafe な関数として内部データへのアクセスを提供するのが一般的である。
たとえば `str` は unsafe な [`str::as_bytes_mut()`][`str::as_bytes_mut`] によって内部の `&mut [u8]` へのアクセスを提供している。

これらの関数は、後に [`AsRef`] トレイト、 [`Deref`] トレイト、 [`From`] トレイト等を実装する際に利用できる。

`AsciiBytes` 型でも、 `str` 関係のメソッドの実装例は `AsciiStr` と同様になる。
しかし `AsciiBytes` はバイト列の側面を強く持つ型であり、 `str` だけでなくさらにその内側の `[u8]` との相互運用も簡単であってほしいかもしれない。
そのような場合に備えて、 `as_bytes()` のようなメソッドを実装することもできる。

```rust
// `str` のみならず、バイト列への変換も実装しておくことができる。

# #[repr(transparent)]
# pub struct AsciiBytes(str);
#
impl AsciiBytes {
    #[inline]
    #[must_use]
    pub fn as_bytes(&self) -> &[u8] {
        self.0.as_bytes()
    }

    /// Returns the mutable reference to the inner bytes.
    ///
    /// # Safety
    ///
    /// The caller must ensure that the bytes is an ASCII string when the borrow ends.
    ///
    /// Use of an `AsciiBytes` which contains non-ASCII characters is undefined behavior.
    #[inline]
    #[must_use]
    pub unsafe fn as_mut_bytes(&mut self) -> &mut [u8] {
        self.0.as_bytes_mut()
    }
}
```

このような便利なメソッドを実装しておくと、のちのち `AsRef<[u8]>` のようなトレイトを実装したくなったとき楽をできる。


* * *

[^footnote-ensuring-validity-after-arbitrary-modification]:
  もちろん、 `AsciiStr` があらゆる処理の前に値の妥当性を検査し、不正な値であれば panic 等することで未定義動作を阻止するという実装は可能である。
  しかしこのような実装ではどのような処理の前にも必ず検査をする必要があり、検査コストが軽くない場合にそのようなことをすると大変ありがたくない。
  このような場合、ドキュメントと `unsafe` で明示したうえで「約束守らなかったら未定義動作 (UB) な！」としてしまうのが定石である。

[`AsRef`]: https://doc.rust-lang.org/stable/std/convert/trait.AsRef.html
[`Deref`]: https://doc.rust-lang.org/stable/std/ops/trait.Deref.html
[`From`]: https://doc.rust-lang.org/stable/std/convert/trait.From.html
[`str::as_bytes_mut`]: https://doc.rust-lang.org/stable/std/primitive.str.html#method.as_bytes_mut
