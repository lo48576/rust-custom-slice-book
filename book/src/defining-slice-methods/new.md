# 値の作成

## 追加の制約なしの型の場合

まずは追加の制約なしの型から考えよう。
[先の節](/defining-slice-types/repr-transparent.md#why-necessary)で説明したように、スライス型の変換は解釈の切り替えによって行う。

```rust
// 内部のスライス型から変換して独自スライス型の参照を作る。

#[repr(transparent)]
pub struct MyStr(str);

impl MyStr {
    #[inline]
    #[must_use]
    pub fn new(s: &str) -> &Self {
        unsafe { &*(s as *const str as *const Self) }
    }

    #[inline]
    #[must_use]
    pub fn new_mut(s: &mut str) -> &mut Self {
        unsafe { &mut *(s as *mut str as *mut Self) }
    }
}
```

ここで `unsafe` は本質的に不可避であることに留意せよ。
また [`#[inline]`][attr-inline] と [`#[must_use]`][attr-must-use] は任意だが、私は付けることにしている。
なぜなら関数は値の解釈の変更以外の実務を一切行わず、また副作用がないため利用しない解釈変更を行うことはナンセンスだからである。
(もっとも、このくらいであればわざわざ注釈を付けずとも普通に最適化で消されるが……)

これらの関数は、後に [`From`] トレイトや [`AsRef`] トレイトを実装する際に利用できる。

## 追加の制約付きの型の場合

追加の制約があって値の変換が失敗しうる場合、許可すべきでない値の作成を許してしまうとクラッシュや未定義動作の原因となりうる。
よって通常 safe なコードで値を作ろうとするときは必ず検査をかける必要があるため、多少の下準備が必要になる。

### エラー型

まずは検査を行う関数から返すエラー型を用意する。

```rust
// エラー型。 エラーの出たバイト位置と、その場所の値を持つ。 `std::str::Utf8Error` を参考にした。

use std::fmt;

/// Error for conversion from bytes to ASCII string.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct AsciiError {
    valid_up_to: usize,
    invalid_byte: u8,
}

impl AsciiError {
    pub fn valid_up_to(&self) -> usize {
        self.valid_up_to
    }

    pub fn invalid_byte(&self) -> u8 {
        self.invalid_byte
    }
}

impl fmt::Display for AsciiError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "invalid ASCII character {:#02x?} found at index {}",
            self.invalid_byte, self.valid_up_to
        )
    }
}

impl std::error::Error for AsciiError {}
```

文字列が ASCII 文字のみでできているという性質は構文解析が必要なほど複雑なものではないため、 `str` を作るときのエラー型 [`std::str::Utf8Error`][`Utf8Error`] を参考にした。

`Utf8Error` は、文字列が先頭からどこまでなら問題ないか (すなわち、どこの場所以降を含むとエラーになるか) の情報 [`valid_up_to`][`Utf8Error::valid_up_to`] と、不正だったバイト列のどの位置が UTF-8 として不正なバイトだったかの情報 [`error_len`][`Utf8Error::error_len`] の、2つの情報を持つエラー型である。
一方 ASCII 文字列は UTF-8 と違って複数バイトに渡る情報を持たず、エラーが発生する位置は常に非 ASCII な最初のバイトであると決まっている。
よって `AsciiError` においては、どこまでなら問題なくどこからエラーかの `valid_up_to` と、エラー位置の (すなわち最初の非 ASCII な) バイトの値を持つようなエラー型とした。

より複雑な構造を持つ文字列型の文法エラーを通知したいのであれば、エラー型を複雑にするなり、あきらめて単純化したエラーを返すなり、何らかの設計上の決断を行う必要があるだろう。

### 内部の型のみから作る場合

エラー型ができたら、検査を行う関数を書く。
まずは独自スライス型の内部の型のみを受け入れる場合。

```rust
// 与えられたデータが型の制約を満たすか確認する。 エラー型をどこまで詳細にするか
// にもよるが、ちょっとしたパーサのようなものを書くことになる。
# #[derive(Debug, Clone, Copy, PartialEq, Eq)]
# pub struct AsciiError {
#     valid_up_to: usize,
#     invalid_byte: u8,
# }
#
# #[repr(transparent)]
# pub struct AsciiStr(str);

impl AsciiStr {
    fn validate(s: &str) -> Result<(), AsciiError> {
        match s.bytes().enumerate().find(|(_pos, byte)| !byte.is_ascii()) {
            Some((pos, byte)) => Err(AsciiError {
                valid_up_to: pos,
                invalid_byte: byte,
            }),
            None => Ok(()),
        }
    }
}
```

この検査はちょっとしたパーサのようなものになるかもしれないが、大事なのはエラーかそうでないか、エラーならどのようなエラーか、それだけである。
よって戻り値の型は Result<(), Error> のようなものになり、 `Ok` で返すべき値はない。

<aside class="admon note">

#### 検査関数の戻り値の型

ここで、検査関数が `Result<&str, Error>` を返すと便利なのではと考える人もいるかもしれない。
これでも問題ないといえば問題ないのだが、 `&mut MyStr` のような型を作ろうとしたとき `Result` が入力文字列を借用しっぱなしになっているのは少々鬱陶しい。
そうでなくとも検査関数は入力の所有権を消費 (consume) しないため、実際は参照を `Ok` の値として返しても総合的には大したメリットにならない場合が多いだろう。

</aside>

さて、検査ができるようになったら次は値の変換である。
とりあえず検査なしの単純な (必ずしも安全でない) 型変換から書こう。

これは検査を省略こそするが、検査を通らない文字列を渡されたりなどして不正な値が作られた場合、後の利用で未定義動作を生じるような危険なオブジェクトが発生する可能性があるため、呼び出し側に検査の義務を課す unsafe 関数とする。

```rust
// 追加の制約のない場合と同じように実装するが、今回は関数自体は unsafe であり、
// 関数名も _unchecked を含む

# #[repr(transparent)]
# pub struct AsciiStr(str);
#
impl AsciiStr {
    /// Creates a new ASCII string slice from the given UTF-8 string slice.
    ///
    /// # Safety
    ///
    /// The given bytes should consists of only ASCII characters.
    /// If this constraint is violated, undefined behavior results.
    #[inline]
    #[must_use]
    pub unsafe fn new_unchecked(s: &str) -> &Self {
        &*(s as *const str as *const Self)
    }

    /// Creates a new mutable ASCII string slice from the given mutable UTF-8 string slice.
    ///
    /// # Safety
    ///
    /// The given bytes should consists of only ASCII characters.
    /// If this constraint is violated, undefined behavior results.
    #[inline]
    #[must_use]
    pub unsafe fn new_unchecked_mut(s: &mut str) -> &mut Self {
        &mut *(s as *mut str as *mut Self)
    }
}
```

[Rust API guidelines][api-guidelines:failure-docs] では、unsafe な関数において呼び出し側が守るべき不変条件のすべてを doc comment の Safety セクションで提示することを推奨している。
利用者が安全に関数を呼び出すために不可欠な情報のため、必ず doc comment で説明するべきである。

<aside class="admon note">

#### debug\_assert

ユーザを信用せず `new_unchecked` の不正な利用にも気付きたいということであれば、ここに `debug_assert` マクロを突っ込んでデバッグビルド時にだけ強制的に検査を行うという手がある。

```rust
# #[repr(transparent)]
# pub struct AsciiStr(str);
# impl AsciiStr {
#     fn validate(s: &str) -> Result<(), ()> {
#         unimplemented!()
#     }
# }
#
impl AsciiStr {
    /// Creates a new ASCII string slice from the given UTF-8 string slice.
    ///
    /// # Safety
    ///
    /// The given string should consists of only ASCII characters.
    /// If this constraint is violated, undefined behavior results.
    #[inline]
    #[must_use]
    pub unsafe fn new_unchecked(s: &str) -> &Self {
        debug_assert!(
            Self::validate(s).is_ok(),
            "Input should be valid ASCII string, but was not: input={:?}",
            s
        );
        &*(s as *const str as *const Self)
    }
}
```

ただしこれだと、後述の安全な `new` で二重に検査が行われる。
これが気になるようなら、関数を分離するという手もある。
そこまですべきかは微妙かもしれないが。

```rust
# #[repr(transparent)]
# pub struct AsciiStr(str);
# impl AsciiStr {
#     fn validate(s: &str) -> Result<(), ()> {
#         unimplemented!()
#     }
# }
#
impl AsciiStr {
    /// Creates a new ASCII string slice from the given UTF-8 string slice.
    ///
    /// # Safety
    ///
    /// The given string should consists of only ASCII characters.
    /// If this constraint is violated, undefined behavior results.
    #[inline]
    #[must_use]
    unsafe fn new_really_unchecked(s: &str) -> &Self {
        &*(s as *const str as *const Self)
    }

    /// Creates a new ASCII string slice from the given UTF-8 string slice.
    ///
    /// # Safety
    ///
    /// The given string should consists of only ASCII characters.
    /// If this constraint is violated, undefined behavior results.
    #[must_use]
    pub unsafe fn new_unchecked(s: &str) -> &Self {
        debug_assert!(
            Self::validate(s).is_ok(),
            "Input should be valid ASCII string, but was not: input={:?}",
            s
        );
        Self::new_really_unchecked(s)
    }
}
```

</aside>

検査なしの変換が実装できたら、次は検査ありの安全な変換である。
戻り値の型に注意して実装する。

```rust
// 制約を満たす文字列であれば読み替える。
// 制約を満たさなければ、読み替えずエラーとする。

# #[repr(transparent)]
# pub struct AsciiStr(str);
# impl AsciiStr {
#    unsafe fn new_unchecked(s: &str) -> &Self { unimplemented!() }
#    unsafe fn new_unchecked_mut(s: &mut str) -> &mut Self { unimplemented!() }
#     fn validate(s: &str) -> Result<(), AsciiError> {
#         unimplemented!()
#     }
# }
# pub struct AsciiError;
#
impl AsciiStr {
    pub fn new(s: &str) -> Result<&Self, AsciiError> {
        match Self::validate(s) {
            Ok(_) => Ok(unsafe {
                // SAFETY: This is safe because the string is successfully validated.
                Self::new_unchecked(s)
            }),
            Err(e) => Err(e),
        }
    }

    pub fn new_mut(s: &mut str) -> Result<&mut Self, AsciiError> {
        match Self::validate(s) {
            Ok(_) => Ok(unsafe {
                // SAFETY: This is safe because the string is successfully validated.
                Self::new_unchecked_mut(s)
            }),
            Err(e) => Err(e),
        }
    }
}
```

これで、効率的だが安全性の検査がスキップされるものと、検査が必ず強制される安全なものが実装できた。


<aside class="admon note">

#### `#[must_use]` は不要

戻り値型が `Result` である場合、型の方に `#[must_use]` 指定が付いているため、関数側で改めて指定する必要はない。
むしろ指定すると clippy に [`clippy::double_must_use` lint][clippy:double-must-use] で無意味だと警告される。

</aside>

これらの関数は、後に [`TryFrom`] トレイトを実装する際に利用できる。

### 内部の型と異なる型から作る場合

ASCII 文字列はバイト列として扱いたい場合も多いだろう。
そこで `AsciiBytes` 型では、内部的には UTF-8 の `str` 型で保持しつつ、値は `&[u8]` からも作れるようにしよう。

まず、検査では `&str` でなく `&[u8]` を受け取る。
`&str` はノーコストで `&[u8]` に変換できるから、これは `&str` から値を作りたいときにも使える。

```rust
// 検査の関数。 内側の型のみから作る場合との違いは、
// 引数の型と、イテレータでのバイト列の取り出し方だけである。

# #[derive(Debug, Clone, Copy, PartialEq, Eq)]
# pub struct AsciiError {
#     valid_up_to: usize,
#     invalid_byte: u8,
# }
#
#[repr(transparent)]
pub struct AsciiBytes(str);

impl AsciiBytes {
    fn validate_bytes(s: &[u8]) -> Result<(), AsciiError> {
        match s
            .iter()
            .copied()
            .enumerate()
            .find(|(_pos, byte)| !byte.is_ascii())
        {
            Some((pos, byte)) => Err(AsciiError {
                valid_up_to: pos,
                invalid_byte: byte,
            }),
            None => Ok(()),
        }
    }
}
```

続けて unsafe な値の作成関数。
一度 `&[u8]` を `&str` に変換してから、いつもの変換をかける。
ASCII 文字列は明らかに妥当な UTF-8 バイト列でもあるため、 `std::str::from_utf8()` を呼び出すことができる。

```rust
// バイト列を受け取る。
// `&[u8]` を一度 `&str` に変換して、段階的に `&AsciiBytes` へと変換する。

# #[repr(transparent)]
# pub struct AsciiBytes(str);
#
impl AsciiBytes {
    /// Creates a new ASCII string slice from the given bytes.
    ///
    /// # Safety
    ///
    /// The given bytes should consists of only ASCII characters.
    /// If this constraint is violated, undefined behavior results.
    pub unsafe fn new_unchecked(bytes: &[u8]) -> &Self {
        // SAFETY: This is safe because ASCII string is a valid UTF-8 sequence.
        let s = core::str::from_utf8_unchecked(bytes);

        &*(s as *const str as *const Self)
    }

    /// Creates a new mutable ASCII string slice from the given mutable bytes.
    ///
    /// # Safety
    ///
    /// The given bytes should consists of only ASCII characters.
    /// If this constraint is violated, undefined behavior results.
    pub unsafe fn new_unchecked_mut(bytes: &mut [u8]) -> &mut Self {
        // SAFETY: This is safe because ASCII string is a valid UTF-8 sequence.
        let s = core::str::from_utf8_unchecked_mut(bytes);

        Self::from_mut_str_unchecked(s)
    }

    /// Creates a new mutable ASCII string slice from the given mutable bytes.
    ///
    /// # Safety
    ///
    /// The given bytes should consists of only ASCII characters.
    /// If this constraint is violated, undefined behavior results.
    pub unsafe fn from_mut_str_unchecked(s: &mut str) -> &mut Self {
        &mut *(s as *mut str as *mut Self)
    }
}
```

`AsciiBytes` の内部の型は `str` であるから、横着して一気にキャストせず、 [`std::str::from_utf8_unchecked()`][`str::from_utf8_unchecked`] 等で `&[u8]` を一度 `&str` に変換したのちそれを `&AsciiBytes` に変換する。

`from_mut_str_unchecked()` を別のメソッドとして分離したのは、あとで使うためである。
使う予定がないなら `new_unchecked_mut()` 内で全てを済ませてもよい。

あとは safe な値の作成。
今回は `&[u8]` と `&str` の両方を受け取れるよう、 `AsRef<[u8]>` を使うことにしよう。

```rust
# #[repr(transparent)]
# pub struct AsciiBytes(str);
# impl AsciiBytes {
#     fn new_unchecked(s: &[u8]) -> &Self { unimplemented!() }
#     fn new_unchecked_mut(s: &mut [u8]) -> &mut Self { unimplemented!() }
#     fn validate_bytes(s: &[u8]) -> Result<(), AsciiError> {
#         unimplemented!()
#     }
# }
# pub struct AsciiError;
#
impl AsciiBytes {
    fn from_bytes(s: &[u8]) -> Result<&Self, AsciiError> {
        match Self::validate_bytes(s) {
            Ok(_) => Ok(unsafe {
                // SAFETY: This is safe because the string is successfully validated.
                Self::new_unchecked(s)
            }),
            Err(e) => Err(e),
        }
    }

    #[inline]
    pub fn new<T: ?Sized + AsRef<[u8]>>(s: &T) -> Result<&Self, AsciiError> {
        Self::from_bytes(s.as_ref())
    }

    pub fn from_bytes_mut(s: &mut [u8]) -> Result<&mut Self, AsciiError> {
        match Self::validate_bytes(s) {
            Ok(_) => Ok(unsafe {
                // SAFETY: This is safe because the string is successfully validated.
                Self::new_unchecked_mut(s)
            }),
            Err(e) => Err(e),
        }
    }

    #[inline]
    pub fn new_mut<T: ?Sized + AsMut<[u8]>>(s: &mut T) -> Result<&mut Self, AsciiError> {
        Self::from_bytes_mut(s.as_mut())
    }

    #[inline]
    pub fn from_mut_str(s: &mut str) -> Result<&mut Self, AsciiError> {
        // SAFETY: `AsciiBytes` guarantees that the string is an ASCII string,
        // and ASCII string is valid UTF-8 sequence.
        let bytes = unsafe { s.as_bytes_mut() };

        Self::from_bytes_mut(bytes)
    }
}
```

敢えて `new()` で完結させず `.as_ref()` だけ済ませて `from_bytes()` に処理を移譲しているのは、バイナリサイズやコンパイル時間増大の回避のためである。
もしこれらを `new()` 内で済ませてしまうと、 `Self::validate_bytes()` を呼び出すような `new` 関数の実体が `T` 型の種類分だけ作られ、それぞれについて最適化やバイナリ生成が行われることになるだろう。
このようなコード重複は無駄であるため、本命の処理を使う前に型を合流させてしまうのである。
このような手動での事前の単一化 (monomorphization) は[コンパイル時間の短縮と生成されるバイナリの縮小に](https://raphlinus.github.io/rust/2019/08/21/rust-bloat.html#use-polymorphism-sparingly)[貢献することが知られている](https://llogiq.github.io/2019/05/18/momo.html)。

型パラメータ `T` には `[u8]` 等のサイズ不定な DST が来ることを想定しているため、 `T: ?Sized` でサイズ不定でも構わないと明示する必要がある。

`from_mut_str()` を `new_mut()` と別で用意しているのは、 `str` が `AsMut<str>` を実装していないからである[^footnote-lack-of-reference-conversion-trait-impls]。
受け付ける型と内部の型が一致しない場合、こういう面倒が増えるので多少の覚悟が必要である。


* * *

[^footnote-lack-of-reference-conversion-trait-impls]:
  実はこういったトレイトの実装抜けは、たまに発見されては実装が追加されたりなどしている。
  参照の変換に使われるトレイトは `AsRef`, `AsMut`, `Deref`, `DerefMut`, `From`, `Borrow`, `BorrowMut` などなど実に多くの種類がある。
  そのうえ `&String` や `&mut str` のような微妙な型は基本的に使われない (`&str` や `&mut String` がよく使われる) ため、「誰も困っていないので修正されない」といったことになりがちで、 `AsMut<str> for str` もそういった見落としのひとつである。

[`AsRef`]: https://doc.rust-lang.org/stable/std/convert/trait.AsRef.html
[`From`]: https://doc.rust-lang.org/stable/std/convert/trait.From.html
[`TryFrom`]: https://doc.rust-lang.org/stable/std/convert/trait.TryFrom.html
[`Utf8Error::error_len`]: https://doc.rust-lang.org/stable/std/str/struct.Utf8Error.html#method.error_len
[`Utf8Error::valid_up_to`]: https://doc.rust-lang.org/stable/std/str/struct.Utf8Error.html#method.valid_up_to
[`Utf8Error`]: https://doc.rust-lang.org/stable/std/str/struct.Utf8Error.html
[`str::from_utf8_unchecked`]: https://doc.rust-lang.org/stable/std/str/fn.from_utf8_unchecked.html
[api-guidelines:failure-docs]: https://rust-lang.github.io/api-guidelines/documentation.html#function-docs-include-error-panic-and-safety-considerations-c-failure
[attr-inline]: https://doc.rust-lang.org/reference/attributes/codegen.html#the-inline-attribute
[attr-must-use]: https://doc.rust-lang.org/reference/attributes/diagnostics.html#the-must_use-attribute
[clippy:double-must-use]: https://rust-lang.github.io/rust-clippy/master/index.html#double_must_use
