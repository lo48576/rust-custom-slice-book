# nostd, alloc 対応

組み込みなど制限された環境などでは std が使えなかったり、 alloc が使えなかったりする。
しかし独自スライス型だけであれば alloc が使えない環境でも利用可能なはずである[^footnote-validator-would-require-allocation]。
本章では、 `std` や `alloc` などが利用できない環境に独自スライス型を対応させるための方法を紹介する。

まず以下に、検査で alloc が必要ない場合にどの環境でどこまで実装できるかを簡単に紹介する。

<table>
  <caption>各環境で使える機能</caption>
  <thead>
    <tr>
      <th rowspan="2">機能</th>
      <th colspan="3">環境</th>
      <th rowspan="2">註記</th>
    </tr>
    <tr>
      <th>core</th>
      <th>alloc</th>
      <th>std</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>独自スライス型</th>
      <td>○</td>
      <td>○</td>
      <td>○</td>
    </tr>
    <tr>
      <th>所有権付きスライス型</th>
      <td>△</td>
      <td>○</td>
      <td>○</td>
      <td>
        所有権付きスライス型でアロケーションしなければ core 環境でも問題ない (たとえば内部の型として <code>[u8; 6]</code> を使うなど)。
        ただし <code>Cow</code> と <code>ToOwned</code> は core 環境では使えない。
      </td>
    </tr>
    <tr>
      <th><code>Box</code></th>
      <td>×</td>
      <td>○</td>
      <td>○</td>
      <td>
        <code>alloc::boxed::Box</code>
      </td>
    </tr>
    <tr>
      <th><code>Rc</code></th>
      <td>×</td>
      <td>○</td>
      <td>○</td>
      <td>
        <code>alloc::rc::Rc</code>
      </td>
    </tr>
    <tr>
      <th><code>Arc</code></th>
      <td>×</td>
      <td>○</td>
      <td>○</td>
      <td>
        <code>alloc::sync::Arc</code>
      </td>
    </tr>
    <tr>
      <th><code>Cow</code></th>
      <td>×</td>
      <td>○</td>
      <td>○</td>
      <td>
        <code>alloc::borrow::Cow</code>
      </td>
    </tr>
    <tr>
      <th><code>str</code></th>
      <td>○</td>
      <td>○</td>
      <td>○</td>
      <td>
        組み込み型
      </td>
    </tr>
    <tr>
      <th><code>[T]</code></th>
      <td>○</td>
      <td>○</td>
      <td>○</td>
      <td>
        組み込み型
      </td>
    </tr>
    <tr>
      <th><code>String</code></th>
      <td>×</td>
      <td>○</td>
      <td>○</td>
      <td>
        <code>alloc::string::String</code>
      </td>
    </tr>
    <tr>
      <th><code>Vec</code></th>
      <td>×</td>
      <td>○</td>
      <td>○</td>
      <td>
        <code>alloc::vec::Vec</code>
      </td>
    </tr>
    <tr>
      <th>エラー型への <code>Error</code> トレイト実装</th>
      <td>×</td>
      <td>×</td>
      <td>○</td>
      <td>
        <code>std::error::Error</code> は <code>alloc</code> に存在しない。
        <code>Error</code> トレイトを実装せずともエラー型として使うことは可能なので、そこまで気にする必要はない。
      </td>
    </tr>
    <tr>
      <th><code>From</code>, <code>TryFrom</code></th>
      <td>○</td>
      <td>○</td>
      <td>○</td>
      <td>
        <code>core::convert::{From, TryFrom}</code>
      </td>
    </tr>
    <tr>
      <th><code>FromStr</code></th>
      <td>○</td>
      <td>○</td>
      <td>○</td>
      <td>
        <code>core::str::FromStr</code>
      </td>
    </tr>
    <tr>
      <th><code>AsRef</code>, <code>AsMut</code></th>
      <td>○</td>
      <td>○</td>
      <td>○</td>
      <td>
        <code>core::convert::{AsRef, AsMut}</code>
      </td>
    </tr>
    <tr>
      <th><code>Deref</code>, <code>DerefMut</code></th>
      <td>○</td>
      <td>○</td>
      <td>○</td>
      <td>
        <code>core::ops::{Deref, DerefMut}</code>
      </td>
    </tr>
    <tr>
      <th><code>Debug</code>, <code>Display</code></th>
      <td>○</td>
      <td>○</td>
      <td>○</td>
      <td>
        <code>core::fmt::{Debug, Display}</code>
      </td>
    </tr>
    <tr>
      <th><code>Borrow</code>, <code>BorrowMut</code></th>
      <td>○</td>
      <td>○</td>
      <td>○</td>
      <td>
        <code>core::borrow::{Borrow, BorrowMut}</code>
      </td>
    </tr>
    <tr>
      <th><code>ToOwned</code></th>
      <td>×</td>
      <td>○</td>
      <td>○</td>
      <td>
        <code>alloc::borrow::ToOwned</code>
      </td>
    </tr>
    <tr>
      <th><code>Default</code></th>
      <td>○</td>
      <td>○</td>
      <td>○</td>
      <td>
        <code>core::default::Default</code>
      </td>
    </tr>
    <tr>
      <th>比較 (<code>PartialEq</code>, <code>PartialOrd</code>, <code>Eq</code>, <code>Ord</code>)</th>
      <td>○</td>
      <td>○</td>
      <td>○</td>
      <td>
        <code>core::cmp::{PartialEq, PartialOrd, Eq, Ord}</code>
      </td>
    </tr>
    <tr>
      <th><code>serde</code> 対応</th>
      <td>○</td>
      <td>○</td>
      <td>○</td>
      <td>
        serde は (少なくとも v1.0.118 時点で既に) nostd / alloc 対応している。
        <code>alloc</code> feature と <code>std</code> feature で制御できる。
      </td>
    </tr>
  </tbody>
</table>


* * *

[^footnote-validator-would-require-allocation]: もちろん検査関数 (あるいはパーサ) がメモリアロケーションを必要とする場合は、検査ができないので alloc 無効環境に対応しょうとするのは無意味だが。
