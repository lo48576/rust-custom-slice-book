# 所有権付きの型の定義

特筆すべきことはない。 書くだけである。

```rust
/// My custom owned string type.
#[derive(Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
// Comparisons implemented for the type are consistent (at least it is intended to be so).
// See <https://github.com/rust-lang/rust-clippy/issues/2025>.
#[allow(clippy::derive_hash_xor_eq, clippy::derive_ord_xor_partial_ord)]
pub struct MyString(String);
```

```rust
/// Owned ASCII string type.
#[derive(Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
// Comparisons implemented for the type are consistent (at least it is intended to be so).
// See <https://github.com/rust-lang/rust-clippy/issues/2025>.
#[allow(clippy::derive_hash_xor_eq, clippy::derive_ord_xor_partial_ord)]
pub struct AsciiString(String);
```

```rust
/// Owned ASCII string type.
#[derive(Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
// Comparisons implemented for the type are consistent (at least it is intended to be so).
// See <https://github.com/rust-lang/rust-clippy/issues/2025>.
#[allow(clippy::derive_hash_xor_eq, clippy::derive_ord_xor_partial_ord)]
pub struct AsciiByteBuf(String);
```

敢えて指摘するなら、 `#[repr(transparent)]` が不要であること、 `Default`, `Clone`, `Copy` トレイトが derive 可能であることがスライス型の定義との違いである。

<aside class="admon note">

## `Default` トレイトが本当に必要か考えること

ここで頭を空っぽにして `#[derive(Default)]` してしまいがちなのだが、本当にそれでいいのかよく考えるべきである。
独自スライス型の定義という文脈では、たとえば「空でない配列のスライス型」とか「C言語の識別子であるような文字列のスライス型」みたいなものを定義したくなるかもしれない。
こういった場合、デフォルトとして空の配列や空文字列が生成されては型の存在意義に反する。

</aside>
