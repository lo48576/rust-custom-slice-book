# 追加の制約付き場合

制約がある場合、エラー型の定義については多少の工夫の余地がある。
値の作成に使う型として内部の型 (今回の例では `String`) のみを想定する場合と、内部の型以外の型 (今回の例では `Vec<[u8]>` や `Box<str>` 等) も受け入れる場合の、それぞれについて例とともに解説する。

## 内部の型からしか作成を許さない場合

内部の型からしか作成を許さない単純なインターフェースにする場合、まずエラー型がこのようになる。

```rust
// エラー型。 `std::string::FromUtf8Error` 型が参考になる。

# #[derive(Debug, Clone, Copy)]
# pub struct AsciiError;
# impl core::fmt::Display for AsciiError {
#     fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result { unimplemented!() }
# }
#
#[derive(Debug, Clone)]
pub struct FromStringError {
    source: String,
    error: AsciiError,
}

impl FromStringError {
    #[inline]
    #[must_use]
    pub fn as_str(&self) -> &str {
        &self.source
    }

    #[inline]
    #[must_use]
    pub fn into_string(self) -> String {
        self.source
    }

    #[inline]
    #[must_use]
    pub fn ascii_error(&self) -> AsciiError {
        self.error
    }
}

impl core::fmt::Display for FromStringError {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        self.error.fmt(f)
    }
}

impl std::error::Error for FromStringError {}
```

`AsciiString` の作成に失敗した場合に、元となる値の所有権を消費せずエラーに含めて返す (この例では `into_string()` で取り出せるようにする) というのがポイントである。
これによって、「もし `AsciiString` でなかったらアルファベットへの変換をかけて、改めて作成を試みる」のようなことが追加のアロケーションなしで可能になる。

このエラー型は [`std::string::FromUtf8Error`] のつくりを踏襲した。
`FromUtf8Error` は `Vec<u8>` から `String` を作る際のエラー型だが、内部的には `&[u8]` から `&str` を作るときの [`std::str::Utf8Error`] エラー型と、変換に失敗した入力バイト列の組であり、 `FromUtf8Error` でもこれらの両方をメソッドで取り出すことが可能である。

このエラー型を使って変換を書く。

```rust
// 素直に書くだけ。

# #[derive(Debug, Clone, Copy)]
# pub struct AsciiError;
# impl core::fmt::Display for AsciiError {
#     fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result { unimplemented!() }
# }
#
# #[repr(transparent)]
# pub struct AsciiStr(str);
# impl AsciiStr {
#     fn validate(s: &str) -> Result<(), AsciiError> {
#         unimplemented!()
#     }
# }
#
# #[derive(Debug, Clone)]
# pub struct FromStringError {
#     source: String,
#     error: AsciiError,
# }
#
pub struct AsciiString(String);

impl AsciiString {
    /// Creates a new `AsciiString` from the given string.
    ///
    /// # Safety
    ///
    /// The given string must be an ASCII string.
    /// If this constraint is violated, undefined behavior results.
    #[inline]
    #[must_use]
    pub unsafe fn new_unchecked(s: String) -> Self {
        Self(s)
    }

    pub fn new(s: String) -> Result<Self, FromStringError> {
        match AsciiStr::validate(&s) {
            Ok(_) => Ok(unsafe {
                // SAFETY: This is safe because the string is successfully validated.
                Self::new_unchecked(s)
            }),
            Err(e) => Err(FromStringError {
                source: s,
                error: e,
            }),
        }
    }
}
```

面白さの欠片もないが、 **`new_unchecked(s: &String)` を `unsafe` にする**のは大変重要である。
`Self(s)` 自体は `unsafe` なしで書ける処理であるが、ここで渡された `s` を無条件に受け入れてよいわけではないことに留意しなければならない。
もし `s` が ASCII 文字列でなかった場合、不正な `AsciiString` を作り未定義動作を誘発することになりかねないため、検査なしでそのような危険な処理を行うこの関数は `unsafe` なのである。

他にポイントがあるとすれば、 `AsciiStr::validate(&s)` のように検証をスライス型の方に移譲しているのと、エラー値の作成で新しく作ったエラー型の方を作っているくらいだろうか。

## 内部の型以外からの作成も認める場合

複数の型の値からの作成を認める場合、元となる値をエラーに含めて返すためには多相なエラー型が必要となる。
名前も `FromStringError` では string 以外から作ろうとしたとき微妙な感じなので、 `CreationError` のような一般的な名前にしよう。

```rust
// 退屈な実装。 前述の `FromStringError` とほぼ同じである。

use std::fmt;

# #[derive(Debug, Clone, Copy)]
# pub struct AsciiError;
# impl core::fmt::Display for AsciiError {
#     fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result { unimplemented!() }
# }
#
#[derive(Debug, Clone)]
pub struct CreationError<T> {
    source: T,
    error: AsciiError,
}

impl<T> CreationError<T> {
    #[inline]
    #[must_use]
    pub fn source(&self) -> &T {
        &self.source
    }

    #[inline]
    #[must_use]
    pub fn into_source(self) -> T {
        self.source
    }

    #[inline]
    #[must_use]
    pub fn ascii_error(&self) -> AsciiError {
        self.error
    }
}

impl<T> fmt::Display for CreationError<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.error.fmt(f)
    }
}

impl<T: fmt::Debug> std::error::Error for CreationError<T> {}
```

値の作成では、 `AsciiBytes::new_unchecked()` と同様、 `Vec<u8>` を一度 `String` を経由して `AsciiByteBuf` に変換する。

```rust
# #[derive(Debug, Clone, Copy)]
# pub struct AsciiError;
# impl core::fmt::Display for AsciiError {
#     fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result { unimplemented!() }
# }
#
# #[repr(transparent)]
# pub struct AsciiBytes(str);
# impl AsciiBytes {
#     fn validate_bytes(s: &[u8]) -> Result<(), AsciiError> {
#         unimplemented!()
#     }
# }
#
# #[derive(Debug, Clone)]
# pub struct CreationError<T> {
#     source: T,
#     error: AsciiError,
# }
#
pub struct AsciiByteBuf(String);

impl AsciiByteBuf {
    /// Creates a new `AsciiByteBuf` from the given string.
    ///
    /// # Safety
    ///
    /// The given string must be an ASCII string.
    /// If this constraint is violated, undefined behavior results.
    pub unsafe fn from_string_unchecked(s: String) -> Self {
        Self(s)
    }

    /// Creates a new `AsciiByteBuf` from the given bytes.
    ///
    /// # Safety
    ///
    /// The given bytes must be an ASCII string.
    /// If this constraint is violated, undefined behavior results.
    pub unsafe fn from_bytes_unchecked(bytes: Vec<u8>) -> Self {
        // SAFETY: `bytes` must be an ASCII string, and an ASCII string is
        // also a valid UTF-8 string.
        let s = String::from_utf8_unchecked(bytes);

        Self(s)
    }

    pub fn new<T: Into<Vec<u8>> + AsRef<[u8]>>(s: T) -> Result<Self, CreationError<T>> {
        match AsciiBytes::validate_bytes(s.as_ref()) {
            Ok(_) => Ok(unsafe {
                // SAFETY: This is safe because the string is successfully validated.
                Self::from_bytes_unchecked(s.into())
            }),
            Err(e) => Err(CreationError {
                source: s,
                error: e,
            }),
        }
    }
}
```

`new()` の引数となる型には、最終的に `Vec<u8>` にするための `Into<Vec<u8>>` と、検査で必要な `&[u8]` を取り出すための `AsRef<[u8]>` の trait bound が必要である。

## 便利メソッド

のちのちトレイト実装で使うため、やはり便利メソッドも実装しておく。

```rust
//  所有権付きの型から、所有権なしの独自スライス型を取り出す。

# pub struct AsciiError;
#
# #[repr(transparent)]
# pub struct AsciiStr(str);
# impl AsciiStr {
#    unsafe fn new_unchecked(s: &str) -> &Self { unimplemented!() }
#    unsafe fn new_unchecked_mut(s: &mut str) -> &mut Self { unimplemented!() }
#     fn validate(s: &str) -> Result<(), AsciiError> {
#         unimplemented!()
#     }
# }
#
# pub struct AsciiString(String);
#
impl AsciiString {
    #[inline]
    #[must_use]
    pub fn as_ascii_str(&self) -> &AsciiStr {
        unsafe {
            // SAFETY: `self` is an ASCII string.
            AsciiStr::new_unchecked(self.0.as_str())
        }
    }

    #[inline]
    #[must_use]
    pub fn as_ascii_str_mut(&mut self) -> &mut AsciiStr {
        unsafe {
            // SAFETY: `self` is an ASCII string.
            AsciiStr::new_unchecked_mut(self.0.as_mut_str())
        }
    }
}
```



いきなり `AsciiString` を `AsciiStr` にするのではなく、 `String` と `&str` を経由する。
`self.0` で `String` を取り出し、 `.as_str()` で `&str` にして、最後に `AsciiStr::new_unchecked` で `&AsciiStr` を得る。

ほとんど同じになるが、一応 `AsciiBytes` の実装も載せておこう。

```rust
// `AsciiStr` での例と違うのは、 `.as_str()` の代わりに `.as_bytes()` を使っている点と、
// `AsciiStr::new_unchecked_mut` の代わりに `AsciiBytes::from_mut_str_unchecked()`
// を使っている点くらいである。

# pub struct AsciiError;
#
# #[repr(transparent)]
# pub struct AsciiBytes(str);
# impl AsciiBytes {
#     fn new_unchecked(s: &[u8]) -> &Self { unimplemented!() }
#     fn from_mut_str_unchecked(s: &mut str) -> &mut Self { unimplemented!() }
#     fn validate_bytes(s: &[u8]) -> Result<(), AsciiError> {
#         unimplemented!()
#     }
# }
#
# pub struct AsciiByteBuf(String);
#
impl AsciiByteBuf {
    #[inline]
    #[must_use]
    pub fn as_ascii_bytes(&self) -> &AsciiBytes {
        unsafe {
            // SAFETY: `self` is an ASCII string.
            AsciiBytes::new_unchecked(self.0.as_bytes())
        }
    }

    #[inline]
    #[must_use]
    pub fn as_ascii_bytes_mut(&mut self) -> &mut AsciiBytes {
        unsafe {
            // SAFETY: `self` is an ASCII string.
            AsciiBytes::from_mut_str_unchecked(self.0.as_mut_str())
        }
    }
}
```

このような「独自型と別の型の間で直接変換が難しい場合、内部の型を経由して多段階で変換を行う」という工夫は、以後のトレイト実装でも必要になる場面がある。


[`std::str::Utf8Error`]: https://doc.rust-lang.org/stable/std/str/struct.Utf8Error.html
[`std::string::FromUtf8Error`]: https://doc.rust-lang.org/stable/std/string/struct.FromUtf8Error.html
