# serde 対応

本章では、 nostd 対応した状態で、加えて serde 対応を追加する例を提示する。
nostd 等を考えない場合は、単に `std` feature や `alloc` feature 関係の分岐等をなくして素直に std 環境用に書けば良いだけなので、例を参考に簡単に書けるだろう。
