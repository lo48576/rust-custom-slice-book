# 附録

## 参考リンク

* メモリレイアウト関係
    + [Type layout - The Rust Reference](https://doc.rust-lang.org/reference/type-layout.html)
    + [Other reprs - The Rustonomicon](https://doc.rust-lang.org/nomicon/other-reprs.html#reprtransparent)
    + [1758-repr-transparent - The Rust RFC Book](https://rust-lang.github.io/rfcs/1758-repr-transparent.html)
    + [std::boxed - Rust, "Memory layout" セクション](https://doc.rust-lang.org/1.48.0/std/boxed/index.html#memory-layout)
* 本記事の筆者による実用文字列型の実装例
    + [iri-string](https://crates.io/crates/iri-string) クレート
        - 型パラメータとして文法を定める規格を受け取るような、多相な文字列型を実装している
          (例: [規格表現用 Spec トレイト](https://docs.rs/iri-string/0.3.0/iri_string/spec/trait.Spec.html)、
          [RiAbsoluteStr 文字列型](https://docs.rs/iri-string/0.3.0/iri_string/types/struct.RiAbsoluteStr.html)、
          `RiAbsoluteStr<IriSpec>` の別名である [IriAbsoluteStr 文字列型](https://docs.rs/iri-string/0.3.0/iri_string/types/type.IriAbsoluteStr.html))。
    + [datetime-string](https://crates.io/crates/datetime-string) クレート
    + [xml-string](https://crates.io/crates/xml-string) クレート

## 主要な更新履歴

* 2021-01-12: 本書の公開。元記事から文言の調整、実装の追加、例示コードの修正、サンプルコード追加。
* 2020-12-26: [本書の元になった記事](https://blog.cardina1.red/2020/12/24/defining-custom-slice-types/)の公開。
