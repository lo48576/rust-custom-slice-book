# struct

```rust
// 再掲

pub struct MyStr(str);
```

型自体は struct で定義する。
例では単要素の tuple struct としたが、フィールドをひとつしか持たない通常の構造体で定義してもよい。

```rust
// tuple struct でなく通常の構造体を扱うこともできる。

pub struct MyStr {
    inner: str,
}
```

この辺りは純粋に好みの問題である。

<aside class="admon note">

## 複数フィールド

厳密には、最後のフィールドがベースとする DST でさえあれば、最初の方に0個以上の ZST を置くことは問題がない。
ZST (Zero Sized Types) とは実行時にサイズが0であるような型で、典型的には [`std::marker::PhantomData`][`PhantomData`] などである。

```rust
// 最後が目的の DST (この例では `str`) でさえあれば、
// その前に ZST (この例では `PhantomData` があっても問題ない。

/// Token string slice in some syntax `S`.
#[repr(transparent)]
pub struct Token<S> {
    /// Syntax.
    _syntax: core::marker::PhantomData<fn() -> S>,
    /// Inner data.
    inner: str,
}
```

もっと言うと、実は DST より前のフィールドが ZST でなくても良いのだが、その場合、もはや新しく定義する型はスライス型ではなくなるため、本記事の対象範囲外である。
詳細は [Exotically Sized Types - The Rustonomicon][rustonomicon:exotically-sized-types] あたりを参照のこと。 

</aside>


[`PhantomData`]: https://doc.rust-lang.org/stable/std/marker/struct.PhantomData.html
[rustonomicon:exotically-sized-types]: https://doc.rust-lang.org/nomicon/exotic-sizes.html
