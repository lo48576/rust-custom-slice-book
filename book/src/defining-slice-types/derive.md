# derive

```rust
#[derive(PartialEq, Eq, PartialOrd, Ord, Hash)]
# struct MyStr(str);
```

基本的に derive は何事もなく利用できるが、定義している型が DST であるゆえ、 `Self` が必要になるトレイトは使えないことに注意が必要である。
具体的には、 `Default`, `Clone` トレイトではメソッドの戻り値に `Self` が使われているため実装できず、 `Copy` トレイトも `Clone` を前提としているため同じく実装できない。
よって独自スライス型で derive 可能なトレイトは `Debug`, `PartialEq`, `Eq`, `PartialOrd`, `Ord`, `Hash` である。

無論、これらのトレイトは実装せずともよいし、 derive せず自分で impl しても良い。
たとえば「比較時に大文字と小文字が区別されない文字列型」などを作ろうとしたとき、 `PartialEq` や `PartialOrd` を derive せず手書きで実装することになるだろう。

<aside class="admon tip">

## `Debug` トレイトは手動実装した方が幸せになれるかも

たとえば `"hello"` という文字列から `MyStr` 型を作った場合、 `#[derive(Debug)]` しているとデバッグ出力で `MyStr("hello")` のようにフォーマットされてしまう。
元となった `str` と同じように `"hello"` とフォーマットしたければ、 `Debug` トレイトは[後述のように](../implementing-traits/fmt.md)自前で実装してやる必要がある。

</aside>

```rust
// Comparisons implemented for the type are consistent (at least it is intended to be so).
// See <https://github.com/rust-lang/rust-clippy/issues/2025>.
#[allow(clippy::derive_hash_xor_eq, clippy::derive_ord_xor_partial_ord)]
# struct MyStr(str);
```

これは最悪体験なのだが、 `Hash` と `PartialEq` の組や `Ord` と `PartialOrd` の組で一方を `derive` して一方を手動で実装すると、 clippy に deny で叱られる。
これは **`#[derive(PartialEq, Hash)]` していても別の型への `PartialEq` 実装があると叱られる**という最悪の実装なので、無効化するしかない。

一応 false positive の問題として[上流で認識されてはいる][clippy-derive-xor-false-positive]ので、そのうち改善されることが期待できる。


[clippy-derive-xor-false-positive]: https://github.com/rust-lang/rust-clippy/issues/2025
