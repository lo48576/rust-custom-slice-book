# `AsRef`, `AsMut`

これらのトレイトの必要性は用途にもよるため、場合によっては [`AsRef`] や [`AsMut`] の実装は不要かもしれないが、とりあえず実装例を提示する。

## 独自スライス型自体への変換

これは自分で実装していると意外に忘れがちだが、 `AsRef<T> for T` のようなデフォルト実装は存在しない。
つまり、 `T: AsRef<MySlice>` のような trait bound を利用する可能性のある型には、自分で `impl AsRef<MySlice> for MySlice` のような実装を用意してやる必要がある。

```rust
// 自分自身の参照への変換。

# #[repr(transparent)]
# pub struct MyStr(str);
#
# #[derive(Clone)]
# pub struct MyString(String);
# impl MyString {
#     fn as_my_str(&self) -> &MyStr { unimplemented!() }
#     fn as_my_str_mut(&mut self) -> &mut MyStr { unimplemented!() }
# }
#
impl AsRef<MyStr> for MyStr {
    #[inline]
    fn as_ref(&self) -> &MyStr {
        self
    }
}

impl AsMut<MyStr> for MyStr {
    #[inline]
    fn as_mut(&mut self) -> &mut MyStr {
        self
    }
}

impl AsRef<MyStr> for MyString {
    #[inline]
    fn as_ref(&self) -> &MyStr {
        self.as_my_str()
    }
}

impl AsMut<MyStr> for MyString {
    #[inline]
    fn as_mut(&mut self) -> &mut MyStr {
        self.as_my_str_mut()
    }
}
```

追加の制約の有無に関係なく実装は同様になるため、 `AsciiStr` と `AsciiBytes` の例は省略する。

## 元のスライス型への変換

追加の制約なしの型については何も考えず実装できる。
が、 **`Box` に対する `AsRef` の実装をしておくと便利かもしれない**。

```rust
// 追加の制約のない型では、 `AsMut` を実装してもよい。しなくともよい。

# #[repr(transparent)]
# pub struct MyStr(str);
# impl MyStr {
#     fn as_str(&self) -> &str { unimplemented!() }
#     fn as_mut_str(&self) -> &mut str { unimplemented!() }
# }
#
# #[derive(Clone)]
# pub struct MyString(String);
#
# impl std::ops::Deref for MyString {
#     type Target = MyStr;
#
#     fn deref(&self) -> &Self::Target { unimplemented!() }
# }
#
# impl std::ops::DerefMut for MyString {
#     fn deref_mut(&mut self) -> &mut Self::Target { unimplemented!() }
# }
#
impl AsRef<str> for MyStr {
    #[inline]
    fn as_ref(&self) -> &str {
        self.as_str()
    }
}

impl AsMut<str> for MyStr {
    #[inline]
    fn as_mut(&mut self) -> &mut str {
        self.as_mut_str()
    }
}

impl AsRef<str> for Box<MyStr> {
    #[inline]
    fn as_ref(&self) -> &str {
        self.as_str()
    }
}

impl AsMut<str> for Box<MyStr> {
    #[inline]
    fn as_mut(&mut self) -> &mut str {
        self.as_mut_str()
    }
}

impl AsRef<str> for MyString {
    #[inline]
    fn as_ref(&self) -> &str {
        self.as_str()
    }
}

impl AsMut<str> for MyString {
    #[inline]
    fn as_mut(&mut self) -> &mut str {
        self.as_mut_str()
    }
}
```

メソッド実装の節でも指摘したように、 追加の制約のある型では元の型への mutable な参照を safe に返してはいけない。
つまり、**そのような場合には `AsMut` を実装してはいけない**。

```rust
// 追加の制約付きの型では、 `AsMut` を実装**してはいけない**。

# #[repr(transparent)]
# pub struct AsciiStr(str);
# impl AsciiStr {
#     fn as_str(&self) -> &str { unimplemented!() }
#     fn as_mut_str(&self) -> &mut str { unimplemented!() }
# }
#
# #[derive(Clone)]
# pub struct AsciiString(String);
#
# impl std::ops::Deref for AsciiString {
#     type Target = AsciiStr;
#
#     fn deref(&self) -> &Self::Target { unimplemented!() }
# }
#
# impl std::ops::DerefMut for AsciiString {
#     fn deref_mut(&mut self) -> &mut Self::Target { unimplemented!() }
# }
#
impl AsRef<str> for AsciiStr {
    #[inline]
    fn as_ref(&self) -> &str {
        self.as_str()
    }
}

impl AsRef<str> for Box<AsciiStr> {
    #[inline]
    fn as_ref(&self) -> &str {
        self.as_str()
    }
}

impl AsRef<str> for AsciiString {
    #[inline]
    fn as_ref(&self) -> &str {
        self.as_str()
    }
}
```

`AsRef<str> for Box<MyStr>` は稀に欲しいことがある。
この実装がなくとも `v: Box<MyStr>` から `v.as_str()` によって `&str` を得ることはできるが、 `fn foo<T: AsRef<str>>(v: &T)` のような関数に `f(&v)` と渡すことはできない。

`AsciiBytes` については `AsciiStr` の場合と同様なので省略する。

## その他の型への変換

元の型と自分自身への変換以外のスライス型への変換を実装しても良い (もちろん安全であればの話だが)。
たとえば `str` は `AsRef<[u8]>`, `AsRef<std::ffi::OsStr>`, `AsRef<std::path::Path>` 等のトレイト実装を持っている。

```rust
// `AsRef<[u8]>` を実装した例。

# struct AsciiBytes(str);
# impl AsciiBytes {
#     fn as_bytes(&self) -> &[u8] { unimplemented!() }
#     fn as_bytes_mut(&mut self) -> &mut [u8] { unimplemented!() }
# }
#
# struct AsciiByteBuf(String);
#
# impl std::ops::Deref for AsciiByteBuf {
#     type Target = AsciiBytes;
#
#     fn deref(&self) -> &Self::Target { unimplemented!() }
# }
#
# impl std::ops::DerefMut for AsciiByteBuf {
#     fn deref_mut(&mut self) -> &mut Self::Target { unimplemented!() }
# }
#
impl AsRef<[u8]> for AsciiBytes {
    #[inline]
    fn as_ref(&self) -> &[u8] {
        self.as_bytes()
    }
}

impl AsRef<[u8]> for Box<AsciiBytes> {
    #[inline]
    fn as_ref(&self) -> &[u8] {
        self.as_bytes()
    }
}

impl AsRef<[u8]> for AsciiByteBuf {
    #[inline]
    fn as_ref(&self) -> &[u8] {
        self.as_bytes()
    }
}
```

どのような変換を実装するかは用途次第であるため、例はこれだけとしておく。
`MyStr` と `str` の相互運用性を高めたければ `AsRef<[u8]> for MyStr` などを実装しても良いだろう。

くれぐれも**制約が緩くなるような変換で mutable な参照を返さないこと**。


[`AsMut`]: https://doc.rust-lang.org/stable/std/convert/trait.AsMut.html
[`AsRef`]: https://doc.rust-lang.org/stable/std/convert/trait.AsRef.html
