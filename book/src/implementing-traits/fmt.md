# `Debug`, `Display`

[`Debug`] トレイトは [`println!()`][`println!`] や [`format!()`][`format!`] において `{:?}` を指定したときの表示を、 [`Display`] トレイトは `{}` を指定したときの表示を司る。

これらの実装で一番ありがちな要求は、 strong typedef によって表示が変化しないでほしいなどであろう。

```rust
// 単純に内部の型 (`str` と `String`) に処理を移譲した。

use std::fmt;

# #[repr(transparent)]
# struct MyStr(str);
#
# struct MyString(String);
#
impl fmt::Debug for MyStr {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl fmt::Display for MyStr {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl fmt::Debug for MyString {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl fmt::Display for MyString {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}
```

この例では、内側の型 (`MyStr` の場合は `str`) の [`Debug`] と [`Display`] 実装をそのまま流用している。
何かしらの整形をして表示したくば、そのように自前で実装すべきである。

整形に全くこだわりがないのであれば、 `#[derive(Debug)]` などをしてもよい。

`Box<T>` については、 `Debug` と `Display` はそれぞれ `T: Debug` の場合と `T: Display` の場合に自動的に生えてくるよう `Box<T>` 側で実装されている。
さらに自前で実装しようとすると conflicting implementations と叱られるので、何もしなくてよい。

<aside class="admon tip">

## `.fmt()` の解決

実はトレイト実装をしている間、トレイトメソッド内ではそのトレイトが use されたような状態になっている。
よって、 `self.0.fmt(f)` は `Debug` の実装中には `Debug::fmt()` へと、また `Display` の実装中には `Display::fmt()` へと解決される。

```rust
use std::fmt;

# #[repr(transparent)]
# struct MyStr(str);
#
# struct MyString(String);
#
impl fmt::Debug for MyStr {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        // self.0.fmt(f) is same as:
        <str as fmt::Debug>::fmt(&self.0, f)
    }
}

impl fmt::Display for MyStr {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        // self.0.fmt(f) is same as:
        <str as fmt::Display>::fmt(&self.0, f)
    }
}
```

</aside>

上の例では両方で内側の型のトレイト実装へと丸投げしたが、もし独自スライス型で独自に整形を行うなら、所有権付きの型の実装は内部の型でなくスライス型の方へ丸投げした方が良い。

```rust
// `self.0` ではなく `self.as_my_str()` で取得した独自スライス型の値へ整形を丸投げする手もある。

use std::fmt;

# #[repr(transparent)]
# #[derive(Debug)]
# struct MyStr(str);
#
# impl fmt::Display for MyStr {
#     fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result { unimplemented!() }
# }
#
# struct MyString(String);
# impl MyString {
#     fn as_my_str(&self) -> &MyStr { unimplemented!() }
# }
#
impl fmt::Debug for MyString {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.as_my_str().fmt(f)
    }
}

impl fmt::Display for MyString {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.as_my_str().fmt(f)
    }
}
```

追加の制約の有無は関係ないので、 `AsciiStr` や `AsciiBytes` などの実装例は省略する。

ここでは一般的な `Debug` と `Display` だけを挙げたが、他の `std::fmt` の整形用トレイトで実装したいものがあれば好きに実装することができる。


[`Debug`]: https://doc.rust-lang.org/stable/std/fmt/trait.Debug.html
[`Display`]: https://doc.rust-lang.org/stable/std/fmt/trait.Display.html
[`format!`]: https://doc.rust-lang.org/stable/std/macro.format.html
[`println!`]: https://doc.rust-lang.org/stable/std/macro.println.html
