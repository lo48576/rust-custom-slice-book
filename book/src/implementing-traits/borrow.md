# `Borrow`, `BorrowMut`, `ToOwned`

これらはスライス型を独自に定義するうえで特に使い勝手に影響するトレイトである。
具体的には、 `.to_owned()` が使えるようになる[^footnote-toowned-in-prelude]のと、 `std::borrow::Cow` が使えるようになる。

```rust
//  所有権なしのスライス型と所有権付きの型を相互に紐付ける。

# #[repr(transparent)]
# pub struct MyStr(str);
# impl MyStr {
#     fn as_str(&self) -> &str { unimplemented!() }
# }
#
# pub struct MyString(String);
# impl MyString {
#     fn as_my_str(&self) -> &MyStr { unimplemented!() }
#     fn as_my_str_mut(&mut self) -> &mut MyStr { unimplemented!() }
#     fn new(s: String) -> Self { unimplemented!() }
# }
#
impl core::borrow::Borrow<MyStr> for MyString {
    #[inline]
    fn borrow(&self) -> &MyStr {
        self.as_my_str()
    }
}

impl core::borrow::BorrowMut<MyStr> for MyString {
    #[inline]
    fn borrow_mut(&mut self) -> &mut MyStr {
        self.as_my_str_mut()
    }
}

impl std::borrow::ToOwned for MyStr {
    type Owned = MyString;

    fn to_owned(&self) -> Self::Owned {
        MyString::new(self.as_str().to_owned())
    }
}
```

[`Borrow<T>`][`Borrow`] は `T` 型の参照を取り出すためのトレイトである。
[`BorrowMut<T>`][`BorrowMut`] は mutable 参照を取り出す版。

<!-- TODO: AsRef と Borrow の違いについての補足 -->

[`ToOwned`] は、参照型から所有権付きの型 (と値) を得るためのトレイトである。
`Clone` トレイトが実装されてさえいれば、 `&T` の所有権付きの型 `T` の値を `.clone()` によって得られる。
しかし `[T]`、 `str` や `MyStr` などのスライス型は DST であり `Clone` トレイトを実装できず、参照を外した型の値をそのまま保持できないから、代わりに何かしらのバッファ的な型が必要になる。
これは `[T]` の場合は `Vec<T>` であり、 `str` の場合は `String` であり、同様に `MyStr` に対しては `MyString` を用意してやろうということである。

同じようなコードになるが、一応 unsafe を使うことになるので `AsciiStr` のコード例も確認しておこう。

```rust
// ほとんど同じだが unsafe を使っている部分がある。

# #[repr(transparent)]
# pub struct AsciiStr(str);
# impl AsciiStr {
#     fn as_str(&self) -> &str { unimplemented!() }
# }
#
# pub struct AsciiString(String);
# impl AsciiString {
#     fn as_ascii_str(&self) -> &AsciiStr { unimplemented!() }
#     fn as_ascii_str_mut(&mut self) -> &mut AsciiStr { unimplemented!() }
#     unsafe fn new_unchecked(s: String) -> Self { unimplemented!() }
# }
#
impl core::borrow::Borrow<AsciiStr> for AsciiString {
    #[inline]
    fn borrow(&self) -> &AsciiStr {
        self.as_ascii_str()
    }
}

impl core::borrow::BorrowMut<AsciiStr> for AsciiString {
    #[inline]
    fn borrow_mut(&mut self) -> &mut AsciiStr {
        self.as_ascii_str_mut()
    }
}

impl std::borrow::ToOwned for AsciiStr {
    type Owned = AsciiString;

    fn to_owned(&self) -> Self::Owned {
        let s = self.as_str();
        unsafe {
            // SAFETY: Valid `AsciiStr` string is also valid as `AsciiString`.
            AsciiString::new_unchecked(s.to_owned())
        }
    }
}
```

`&AsciiStr` から直接 `AsciiString` を作れないため `&str` と `String` を経由する。
これ自体は `MyStr` の例と同じだが、今回の例では `String` から `AsciiString` を作るのが `unsafe` な操作である。
不変条件を満たしていること自体は明らかなので、コメントで明示したうえで素直に書けばよい。

<aside class="admon tip">

## unsafe の利用を排するべきか

unsafe の個数や頻度をコードの危険性や品質の判定に使おうというムーヴメントがあり、これ自体はまあ真っ当なものである。
しかし、この判定基準に過剰に迎合するために本来は unsafe であるものをあたかも unsafe でないかのように見せるというのはよろしくない。

たとえば `AsciiStr` の例と同等のコードは次のようにも書ける。

```rust
// unsafe を追放できた、バンザイ！ ……本当に？

# #[repr(transparent)]
# pub struct AsciiStr(str);
# impl AsciiStr {
#     fn as_str(&self) -> &str { unimplemented!() }
# }
#
# pub struct AsciiString(String);
# impl AsciiString {
#     fn as_ascii_str(&self) -> &AsciiStr { unimplemented!() }
#     fn as_ascii_str_mut(&mut self) -> &mut AsciiStr { unimplemented!() }
#     unsafe fn new_unchecked(s: String) -> Self { unimplemented!() }
# }
#
# impl core::borrow::Borrow<AsciiStr> for AsciiString {
#     #[inline]
#     fn borrow(&self) -> &AsciiStr {
#         self.as_ascii_str()
#     }
# }
#
impl std::borrow::ToOwned for AsciiStr {
    type Owned = AsciiString;

    fn to_owned(&self) -> Self::Owned {
        AsciiString(self.as_str().to_owned())
    }
}
```

これでプログラムがより安全になっただろうか？
もちろんそんなことはない。
むしろ「`AsciiString` の内容は ASCII 文字列でなければならない」という不変条件の明示がなくなり、さらには unsafe のマークも消されてしまったことで、本当に安全か確認することさえ大変になってしまった。
事前知識一切なしにプログラムを読んで、いきなり「この `AsciiString(s.to_owned())` では暗黙に不変条件が設定されていて、それは確かに守られているな」と思える人はいないだろう。

独自スライスまわりの操作は本質的に unsafe が不可避だったり、あるいは safe に書けはするが検査するまでもなく安全とわかる箇所がそれなりに多い。
安易に unsafe の数を減らすのではなく、人間による安全性の確認がしやすいような実装を心掛けるべきである。

もしどうしても unsafe を排したいのであれば、無駄な検査が行われるコストと引き換えに以下のように書くことはできる。

```rust
// 本当に safe なコード。本来不要な検査が行われる。
// 万が一実装バグなどで不変条件が破れていても、未定義動作にならずクラッシュする。

# #[repr(transparent)]
# pub struct AsciiStr(str);
# impl AsciiStr {
#     fn as_str(&self) -> &str { unimplemented!() }
# }
#
# pub struct AsciiString(String);
# impl AsciiString {
#     fn as_ascii_str(&self) -> &AsciiStr { unimplemented!() }
#     fn as_ascii_str_mut(&mut self) -> &mut AsciiStr { unimplemented!() }
#     fn new(s: String) -> Result<Self, ()> { unimplemented!() }
# }
#
# impl core::borrow::Borrow<AsciiStr> for AsciiString {
#     #[inline]
#     fn borrow(&self) -> &AsciiStr {
#         self.as_ascii_str()
#     }
# }
#
impl std::borrow::ToOwned for AsciiStr {
    type Owned = AsciiString;

    fn to_owned(&self) -> Self::Owned {
        let s = self.as_str();
        AsciiString::new(s.to_owned())
            .expect("Should never fail: `AsciiStr` must have ASCII string")
    }
}
```

</aside>

<!-- TODO -->

* * *

[^footnote-toowned-in-prelude]:
  実は `"hello".to_owned()` などとした場合、 `<str as ToOwned>::to_owned("hello")` が呼ばれている。
  存在感の薄いトレイトだが、 `ToOwned` は prelude に入っているのである。

[`Borrow`]: https://doc.rust-lang.org/stable/std/borrow/trait.Borrow.html
[`BorrowMut`]: https://doc.rust-lang.org/stable/std/borrow/trait.BorrowMut.html
[`ToOwned`]: https://doc.rust-lang.org/stable/std/borrow/trait.ToOwned.html
