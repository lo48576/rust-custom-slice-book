# `From`, `TryFrom`

説明するまでもないが、値の作成や変換に用いるトレイトである。
本節では、 `From`, `TryFrom`, あるいは `Into` トレイトで用意しておきたい実装について、例とともに列挙していく。

<aside class="admon note">

## `TryFrom` は2020年12月 (Rust 1.48.0) 時点で prelude 入りしていない

以後のコード例ではあたかも `TryFrom` が直接使えるかのように書いているが、 `TryFrom` は (`From` 等と違って) 未だ prelude 入りしていない。
暗黙に `use std::convert::TryFrom;` しているものと考えてほしい。

</aside>

<aside class="admon note">

## 可能なら `Into` よりも常に `From` の実装を優先すべし

`From<T> for U` が実装された型 `T` と `U` について、 `Into<U> for T` は自動的に実装される。

```rust,ignore
// Rust 1.48.0 における、 `From` から自動的に発生する `Into` の実装。
// https://github.com/rust-lang/rust/blob/7eac88abb2e57e752f3302f02be5f3ce3d7adfb4/library/core/src/convert/mod.rs#L538-L547

// From implies Into
#[stable(feature = "rust1", since = "1.0.0")]
impl<T, U> Into<U> for T
where
    U: From<T>,
{
    fn into(self) -> U {
        U::from(self)
    }
}
```

つまり `From<T> for U` さえ実装しておけば、 `U: From<T>` と `T: Into<U>` の両方に対応できるのである。

一方 `Into<U> for T` を実装してしまうと、 `From<T> for U` を実装することができない。
なぜなら、これを実装すると、先に挙げた汎用実装により生成される `Into` の実装と手動の `Into` 実装が競合するからである。
つまり、 `Into` を手動で実装した型の組について、対応する `From` 実装が存在できないということになる。
ゆえに `From` と `Into` の両方が使えるよう、手動で実装するのは可能な場合常に `From` にするべきである。

逆に trait bound として使うときは、 `From` よりも `Into` トレイトを使う方が望ましい。
なぜなら、 `From` が実装されておらず `Into` が実装されている型の組がありうるからである。

`Into` を手動で実装する必要があるのは、 orphan rule 絡みの部分で、主に多相型を使っている場合などに発生する一部の状況のみである。
`From` を実装しようとしてコンパイルエラーでどうしようもなくなれば自然とそのタイミングがわかるので、そうなってから `Into` の実装を試みるのでも遅くはない。

</aside>
