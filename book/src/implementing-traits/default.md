# `Default`

所有権付きの独自型であれば [`Default`] トレイトは derive するなり自前実装するなりが簡単であるから、好きに実装すれば良い。

しかし実はそれ以外にも `Default` を実装できる対象がある。
独自スライス型の参照と可変参照、そして `Box` である。

```rust
// 追加の制約のない型の場合。

# #[repr(transparent)]
# struct MyStr(str);
# impl MyStr {
#     fn new(s: &str) -> &Self { unimplemented!() }
#     fn new_mut(s: &mut str) -> &mut Self { unimplemented!() }
# }
#
# impl From<&MyStr> for Box<MyStr> {
#     fn from(s: &MyStr) -> Self { unimplemented!() }
# }
#
impl Default for &MyStr {
    fn default() -> Self {
        MyStr::new(<&str>::default())
    }
}

impl Default for &mut MyStr {
    fn default() -> Self {
        MyStr::new_mut(<&mut str>::default())
    }
}

impl Default for Box<MyStr> {
    #[inline]
    fn default() -> Self {
        <&MyStr>::default().into()
    }
}
```

地味に便利なことに、 `&'_ [T]` や `&'_ mut [T]` 、 `&'_ str` 、 `&'_ mut str` 等の参照型には実は `Default` トレイトが実装されている。
もちろんこれらは長さ0の配列や文字列であるから、参照が指す値は特に有意義ということもない。
しかし `str` や `[T]` の strong typedef を定義しようというときにはこのトレイト実装自体が強力な道具となる。
なぜなら、 `Default` で作成できるこれらの参照は任意の lifetime で作ることができるため、どのような lifetime が要求されていても問題なく使える万能の参照だからである。

```rust,no_run
// どんな lifetime でも使えるので `'static` さえ可能だし、 mutable な参照も作れる

# #[repr(transparent)]
# struct MyStr(str);
#
# impl Default for &'_ mut MyStr {
#     fn default() -> Self { unimplemented!() }
# }
#
let a: &'static str = Default::default(); // OK!
let b: &'static mut MyStr = Default::default(); // OK!
```

`Default` トレイトはどんな場合にでも実装できるわけではないから、本当に妥当なデフォルト値があってそれが正しく用いられているかは確認すべきである。

```rust
// 追加の制約付きだが、デフォルト値として空文字列が使えるのでそのように `Default` を実装する。
# #[repr(transparent)]
# struct AsciiStr(str);
# impl AsciiStr {
#     unsafe fn new_unchecked(s: &str) -> &Self { unimplemented!() }
#     unsafe fn new_unchecked_mut(s: &mut str) -> &mut Self { unimplemented!() }
# }
#
# impl From<&AsciiStr> for Box<AsciiStr> {
#     fn from(s: &AsciiStr) -> Self { unimplemented!() }
# }
#

impl Default for &AsciiStr {
    fn default() -> Self {
        unsafe {
            // SAFETY: An empty string is valid ASCII string.
            AsciiStr::new_unchecked(<&str>::default())
        }
    }
}

impl Default for &mut AsciiStr {
    fn default() -> Self {
        unsafe {
            // SAFETY: An empty string is valid ASCII string.
            AsciiStr::new_unchecked_mut(<&mut str>::default())
        }
    }
}

impl Default for Box<AsciiStr> {
    #[inline]
    fn default() -> Self {
        <&AsciiStr>::default().into()
    }
}
```

`AsciiBytes` の例は `AsciiStr` と同様になるので省略する。


[`Default`]: https://doc.rust-lang.org/stable/std/default/trait.Default.html
