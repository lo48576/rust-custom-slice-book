# スマートポインタとの変換

これが絶妙に非自明なので詳しく解説する。

## スライスからスマートポインタへの変換

まずは基本となる [`Box<_>`][`Box`] の例で説明する。

```rust
// `Box<MyStr>` の作成。

# #[repr(transparent)]
# struct MyStr(str);
# impl MyStr {
#     fn as_str(&self) -> &str { unimplemented!() }
# }
#
impl From<&MyStr> for Box<MyStr> {
    fn from(s: &MyStr) -> Self {
        // Create the boxed inner slice.
        let inner_box: Box<str> = Box::from(s.as_str());
        // Take the allocated memory out of the box, without releasing.
        let inner_boxed_ptr: *mut str = Box::into_raw(inner_box);

        unsafe {
            // SAFETY: The string is valid as `MyStr`.
            // SAFETY: Memory layouts for `str` and `MyStr` are compatible,
            // and `inner_boxed_ptr` is allocated by `Box::<str>::from()`.
            Box::from_raw(inner_boxed_ptr as *mut MyStr)
        }
    }
}
```

`Box<MyStr>` を直接作るのでなく、 `&MyStr` → `&str` → `Box<str>` → `*mut str` → `*mut MyStr` → `Box<MyStr>` というように、作り方が std より提供されている `Box<str>` を起点として、 mut 生ポインタを経由して `MyStr` へ型変換することで値を作る。

以下がその流れである。

```rust,ignore
// Create the boxed inner slice.
let inner_box: Box<str> = Box::from(s.as_str());
```

`&MyStr` から中身の `&str` を `.as_str()` で取り出し、ここから `Box::from()` (より正確には `Box::<str>::form()`) で boxed string を作成する。
`Box::from()` は借用である `&str` から所有権付きの文字列を作る必要があるため、内部でメモリアロケーションが発生し、 `&str` が新たに確保されたメモリ領域内にコピーされる。

これで `&MyStr` → `Box<str>` への変換ができる。

```rust,ignore
// Take the allocated memory out of the box, without releasing.
let inner_boxed_ptr: *mut str = Box::into_raw(inner_box);
```

`Box::into_raw()` は、確保されたメモリ領域をそのままに `Box` 自体を解体する。
`Box` から別の型の `Box` へと安全に直接変換する方法がないため、一度生ポインタを経由してキャスト (型の読み替え) をしてやる必要があるのである。
もちろん生ポインタは所有権管理が自動ではないため、後で `Box::from_raw()` によって再び `Box` へと再構築してやらないとメモリリークとなる。

これで `Box<str>` → `*mut str` への変換ができる。

```rust,ignore
unsafe {
    // SAFETY: The string is valid as `MyStr`.
    // SAFETY: Memory layouts for `str` and `MyStr` are compatible,
    // and `inner_boxed_ptr` is allocated by `Box::<str>::from()`.
    Box::from_raw(inner_boxed_ptr as *mut MyStr)
}
```

`inner_boxed_ptr as *mut MyStr` で `*mut str` から `*mut MyStr` へ、さらに `Box::from_raw()` で `*mut MyStr` から `Box<MyStr>` への変換ができる。

ここでは2つの不変条件 (invariant) が要求されていることに注意せよ。 ひとつは `inner_boxed_ptr: *mut str` を `*mut MyStr` にキャストするための (より正確には、キャストしたのち安全に参照するための)、「ポインタで指されたデータが `MyStr` として妥当である」という条件。
`MyStr` の例では追加の制約はないが、もし `AsciiStr` のように追加の制約があっても問題ない。
そもそも元になったデータが、引数として渡された `s: &MyStr` や `s: &AsciiStr` などの自分自身そのものの型であり、受け取って以降に値の加工は行っていないから、この不変条件は自明に満たされるのである。

もうひとつの不変条件は `Box::from_raw()` で生ポインタから Box を安全に作るための条件である。

> For non-zero-sized values, a Box will use the Global allocator for its allocation.
> It is valid to convert both ways between a Box and a raw pointer allocated with the Global allocator,
> given that the Layout used with the allocator is correct for the type.
>
> —— [std::boxed のドキュメント](https://doc.rust-lang.org/1.48.0/std/boxed/index.html#memory-layout)

端的に言えば、 `Box<T>` が内部的にメモリアロケーションに用いるのと同じメモリレイアウトで確保された領域であれば、 `Box::<T>::from_raw()` に渡しても安全であるということである。
`MyStr` の例については、 `#[repr(transparent)]` によってメモリレイアウトが `str` と互換になることが保証されているため、 `Box<str>` が確保する領域のメモリレイアウトと `Box<MyStr>` が確保する領域のそれも互換である。
すなわち、 `Box::<str>::from()` が確保するメモリ領域は `Box::<MyStr>::from()` が確保するであろうメモリ領域と互換なレイアウトを持つはずであるため、そのどちらも安全に `Box::<MyStr>::from_raw()` に渡すことができるというわけである。

> ```
> // `fn from` in `impl From<&CStr> for Box<CStr>` current implementation relies
> // on `CStr` being layout-compatible with `[u8]`.
> // When attribute privacy is implemented, `CStr` should be annotated as `#[repr(transparent)]`.
> ```
>
> —— [Rust 1.48.0 の std::ffi::CStr の定義についてのコメント (抜粋)](https://github.com/rust-lang/rust/blob/7eac88abb2e57e752f3302f02be5f3ce3d7adfb4/library/std/src/ffi/c_str.rs#L191-L193)

<aside class="admon important">

### 標準ライブラリの unsafe なコードの真似には注意

注意しなければならないのが、標準ライブラリでは必ずしも `#[repr(transparent)]` を指定していないが、これをユーザが真似してはいけないということである。

たとえば Rust 1.48.0 時点での `std::ffi::CStr` の定義についてのコメントでは、 `#[repr(transparent)]` の指定がない理由が述べられている。
`#[repr(transparent)]` があれば `&CStr` から `Box<CStr>` への変換が明らかに安全になるが、 `#[repr(transparent)]` であるという性質自体はライブラリとして保証したくないため、現時点ではコンパイラ実装の詳細に依存しておくとのことである。
これは標準ライブラリがコンパイラと共にメンテナンスされ頒布されているからこそできる荒技で、コンパイラの内部実装の変化が発生した際に標準ライブラリはそれを知って適切に実装を変えたり、あるいはコンパイラが std の型だけ特別扱いすることができるからこそ許されている。
すなわち `core` や `std` の内部においては、ユーザが通常手に入れられるよりも多くの保証が特別に与えられているのである。

もしユーザが `std` 等のコードをそのまま自分のクレートにコピペすると、最悪の場合安全性の保証が足りず未定義動作や脆弱性を引き起こすことになる。
unsafe なコードを書くときは `std` で使われていた方法だからといって信用するのではなく、安全である根拠をきちんと考える必要がある。

</aside>

これで `*mut str` → `*mut MyStr` → `Box<MyStr>` への変換ができる。
以上のすべてを組み合わせれば、 `&MyStr` → `Box<MyStr>` への変換が完成したことになる。

同じようなスマートポインタであるところの [`std::rc::Rc`][`Rc`] と [`std::sync::Arc`][`Arc`] も、ほとんど同じようなものである。

```rust
// `Rc<MyStr>` と `Arc<MyStr>` の作成。

use std::rc::Rc;
use std::sync::Arc;

# #[repr(transparent)]
# struct MyStr(str);
# impl MyStr {
#     fn as_str(&self) -> &str { unimplemented!() }
# }
#
impl From<&MyStr> for Rc<MyStr> {
    fn from(s: &MyStr) -> Self {
        // Create the shared inner slice.
        let inner_box: Rc<str> = Rc::from(s.as_str());
        // Take the allocated memory out of the Rc, without releasing.
        let inner_boxed_ptr: *const str = Rc::into_raw(inner_box);

        unsafe {
            // SAFETY: The string is valid as `MyStr`.
            // SAFETY: Memory layouts for `str` and `MyStr` are compatible,
            // and `inner_boxed_ptr` is allocated by `Rc::<str>::from()`.
            Rc::from_raw(inner_boxed_ptr as *const MyStr)
        }
    }
}

impl From<&MyStr> for Arc<MyStr> {
    fn from(s: &MyStr) -> Self {
        // Create the shared inner slice.
        let inner_box: Arc<str> = Arc::from(s.as_str());
        // Take the allocated memory out of the Arc, without releasing.
        let inner_boxed_ptr: *const str = Arc::into_raw(inner_box);

        unsafe {
            // SAFETY: The string is valid as `MyStr`.
            // SAFETY: Memory layouts for `str` and `MyStr` are compatible,
            // and `inner_boxed_ptr` is allocated by `Arc::<str>::from()`.
            Arc::from_raw(inner_boxed_ptr as *const MyStr)
        }
    }
}
```

[`Rc`] と [`Arc`] は型名 (コメント内のものも含む) 以外全く同じコードである。
複数の型に対して何度も同じような実装を書きそうであれば、マクロ化してしまうのも手だろう。

`Box` との違いは、 `into_raw()` が返す生ポインタの mutability である。
`Box` では確保されたメモリの唯一の所有者が box 自身なので、 mutable な生ポインタを返せる。
一方、 `Rc` や `Arc` では自分以外にも同じメモリ領域を参照しているものがあるかもしれないため所有権を奪うことはできず、参照者が唯一である保証ができないため const な生ポインタを返すしかない。

追加の制約付きの型 (例では `AsciiStr` と `AsciiBytes`) についても全く同様のコードになるので、実装例は省略する。

## 所有権付きの型からスマートポインタへの変換

所有権付きの型からの変換も、スライスからの変換と同様に既存の `String` や `Vec<T>` 等の既存の型を経由して実装する。

```rust
// `MyString` からの `Box<MyStr>` の作成。

# #[repr(transparent)]
# struct MyStr(str);
#
# struct MyString(String);
#
impl From<MyString> for Box<MyStr> {
    fn from(s: MyString) -> Self {
        // Create the boxed inner slice.
        let inner_box: Box<str> = Box::from(s.0);
        // Take the allocated memory out of the box, without releasing.
        let inner_boxed_ptr: *mut str = Box::into_raw(inner_box);

        unsafe {
            // SAFETY: The string is valid as `MyStr`.
            // SAFETY: Memory layouts for `str` and `MyStr` are compatible,
            // and `inner_boxed_ptr` is allocated by `Box::<str>::from()`.
            Box::from_raw(inner_boxed_ptr as *mut MyStr)
        }
    }
}
```

やっていることは本質的にはスライス型からの変化と同じで、 `Box::from(s.as_str())` で `&str` から `Box` を作っていたところを、 `Box::from(s.0)` で `String` から `Box` を作るよう変更しただけである。

```rust
// `MyString` からの `Box<MyStr>` の作成。

use std::rc::Rc;
use std::sync::Arc;

# #[repr(transparent)]
# struct MyStr(str);
#
# struct MyString(String);
#
impl From<MyString> for Rc<MyStr> {
    fn from(s: MyString) -> Self {
        // Create the shared inner slice.
        let inner_box: Rc<str> = Rc::from(s.0);
        // Take the allocated memory out of the Rc, without releasing.
        let inner_boxed_ptr: *const str = Rc::into_raw(inner_box);

        unsafe {
            // SAFETY: The string is valid as `MyStr`.
            // SAFETY: Memory layouts for `str` and `MyStr` are compatible,
            // and `inner_boxed_ptr` is allocated by `Rc::<str>::from()`.
            Rc::from_raw(inner_boxed_ptr as *const MyStr)
        }
    }
}

impl From<MyString> for Arc<MyStr> {
    fn from(s: MyString) -> Self {
        // Create the shared inner slice.
        let inner_box: Arc<str> = Arc::from(s.0);
        // Take the allocated memory out of the Arc, without releasing.
        let inner_boxed_ptr: *const str = Arc::into_raw(inner_box);

        unsafe {
            // SAFETY: The string is valid as `MyStr`.
            // SAFETY: Memory layouts for `str` and `MyStr` are compatible,
            // and `inner_boxed_ptr` is allocated by `Arc::<str>::from()`.
            Arc::from_raw(inner_boxed_ptr as *const MyStr)
        }
    }
}
```

`Rc` と `Arc` についても同様で、特に語るべきことはない。
また `AsciiString` や `AsciiByteBuf` のような追加の制限付きの型でも同様の実装になるため、これも例は省略する。

## その他の `Box` への変換

`&str` 以外からの `Box<str>` への変換も、あると便利なので実装すると良いかもしれない。

```rust
// 追加の制約なしの型での変換。

# #[repr(transparent)]
# struct MyStr(str);
#
# struct MyString(String);
#
impl From<Box<str>> for Box<MyStr> {
    fn from(s: Box<str>) -> Self {
        let boxed_ptr = Box::into_raw(s);
        unsafe {
            // SAFETY: The `str` string is also valid as `MyStr`.
            // SAFETY: Memory layouts for `str` and `MyStr` are compatible,
            // and `boxed_ptr` is a pointer from valid `Box<str>`.
            Box::from_raw(boxed_ptr as *mut MyStr)
        }
    }
}

impl From<String> for Box<MyStr> {
    fn from(s: String) -> Self {
        s.into_boxed_str().into()
    }
}
```

追加の制約がある場合、制約を強める方向での変換には `From` の代わりに `TryFrom` を使う。
以下の例では追加で `str` のみならず `[u8]` 関係の型との変換も載せる。

```rust
// 追加の制約付きの型での変換。

use std::convert::TryFrom;

# struct AsciiError;
#
# #[repr(transparent)]
# struct AsciiBytes(str);
# impl AsciiBytes {
#     fn new<T: ?Sized + AsRef<[u8]>>(s: &T) -> Result<&Self, AsciiError> { unimplemented!() }
#     fn validate_bytes(s: &[u8]) -> Result<(), AsciiError> { unimplemented!() }
# }
#
# struct CreationError<T> {
#     source: T,
#     error: AsciiError,
# }
#
# impl From<&AsciiBytes> for Box<AsciiBytes> {
#     fn from(s: &AsciiBytes) -> Box<AsciiBytes> { unimplemented!() }
# }
#
impl TryFrom<&str> for Box<AsciiBytes> {
    type Error = AsciiError;

    fn try_from(s: &str) -> Result<Self, Self::Error> {
        AsciiBytes::new(s).map(Into::into)
    }
}

impl TryFrom<&[u8]> for Box<AsciiBytes> {
    type Error = AsciiError;

    fn try_from(s: &[u8]) -> Result<Self, Self::Error> {
        AsciiBytes::new(s).map(Into::into)
    }
}

impl TryFrom<Box<str>> for Box<AsciiBytes> {
    type Error = CreationError<Box<str>>;

    fn try_from(s: Box<str>) -> Result<Self, Self::Error> {
        if let Err(e) = AsciiBytes::validate_bytes(s.as_bytes()) {
            return Err(CreationError {
                source: s,
                error: e,
            });
        }

        let boxed_ptr: *mut str = Box::into_raw(s);
        Ok(unsafe {
            // SAFETY: The string is validated as `AsciiBytes`.
            // SAFETY: Memory layouts for `str` and `AsciiBytes` are compatible,
            // and `boxed_ptr` is a pointer from valid `Box<str>`.
            Box::from_raw(boxed_ptr as *mut AsciiBytes)
        })
    }
}

impl TryFrom<Box<[u8]>> for Box<AsciiBytes> {
    type Error = CreationError<Box<[u8]>>;

    fn try_from(s: Box<[u8]>) -> Result<Self, Self::Error> {
        if let Err(e) = AsciiBytes::validate_bytes(&s) {
            return Err(CreationError {
                source: s,
                error: e,
            });
        }

        // There is no direct `Box<[u8]>` to `Box<str>` method or trait impl.
        // Use `Box<[u8]>` -> `Vec<[u8]>` -> `String` -> `Box<MyStr>` route.
        // These conversions won't cause additional memory allocations.

        let string = unsafe {
            // SAFETY: The bytes is validated as `AsciiBytes`, and
            // valid `AsciiBytes` data is also valid UTF-8 bytes.
            String::from_utf8_unchecked(s.into_vec())
        };
        let boxed_ptr: *mut str = Box::into_raw(string.into_boxed_str());
        Ok(unsafe {
            // SAFETY: The string is validated as `AsciiBytes`.
            // SAFETY: Memory layouts for `str` and `AsciiBytes` are compatible,
            // and `boxed_ptr` is a pointer from valid `Box<str>`.
            Box::from_raw(boxed_ptr as *mut AsciiBytes)
        })
    }
}

impl TryFrom<String> for Box<AsciiBytes> {
    type Error = CreationError<String>;

    fn try_from(s: String) -> Result<Self, Self::Error> {
        if let Err(e) = AsciiBytes::validate_bytes(s.as_bytes()) {
            return Err(CreationError {
                source: s,
                error: e,
            });
        }

        let boxed_ptr: *mut str = Box::into_raw(s.into_boxed_str());
        Ok(unsafe {
            // SAFETY: The string is validated as `AsciiBytes`.
            // SAFETY: Memory layouts for `str` and `AsciiBytes` are compatible,
            // and `boxed_ptr` is a pointer from valid `Box<str>`.
            Box::from_raw(boxed_ptr as *mut AsciiBytes)
        })
    }
}

impl TryFrom<Vec<u8>> for Box<AsciiBytes> {
    type Error = CreationError<Vec<u8>>;

    fn try_from(s: Vec<u8>) -> Result<Self, Self::Error> {
        if let Err(e) = AsciiBytes::validate_bytes(&s) {
            return Err(CreationError {
                source: s,
                error: e,
            });
        }

        let string = unsafe {
            // SAFETY: The bytes is validated as `AsciiBytes`, and
            // valid `AsciiBytes` data is also valid UTF-8 bytes.
            String::from_utf8_unchecked(s)
        };
        let boxed_ptr: *mut str = Box::into_raw(string.into_boxed_str());
        Ok(unsafe {
            // SAFETY: The string is validated as `AsciiBytes`.
            // SAFETY: Memory layouts for `str` and `AsciiBytes` are compatible,
            // and `boxed_ptr` is a pointer from valid `Box<str>`.
            Box::from_raw(boxed_ptr as *mut AsciiBytes)
        })
    }
}
```

似たようなコードが多いのが気になるなら何かしらの関数にまとめても良いかもしれないが、本質的にはエラーの検査と連鎖的な変換の羅列に過ぎず、本質的な複雑性はかなり低い。
また関数にまとめても unsafe 関数になってしまうため、 safety についてのコメントを付けるとあまりコード短縮にはならない気がするので、筆者個人的にはこのままで良い気がする。
その辺りは好みの問題である。

`AsciiStr` でなく `AsciiBytes` を例に使ったのは、エラー型の設計の問題である。
`AsciiStr` 用の `FromStringError` は `String` からのエラーしか表現できず、 `TryFrom<Box<str>> for Box<AsciiStr>` などで返すべきエラー型として不適当であり、結果として実装できるトレイトに制限がかかってしまう。
もちろん無駄のある実装——変換に失敗したら `Box<str>` を `String` に変換して `FromStringError` を返す——も可能だが、無闇に入力を加工して返すのはエラー型として望ましい振る舞いではない。

<aside class="admon note">

## 標準ライブラリの変換用トレイト実装も抜けが多い

なお 2021-01-11 (Rust 1.49.0) 時点で標準ライブラリには `TryFrom<Box<[u8]>> for Box<str>` どころか `TryFrom<&[u8]> for &str` すら実装がない有様なので、実は `From` や `TryFrom` はそこまで網羅的に実装しなくとも `str` 程度のユーザビリティは確保できる。

</aside>

## `Box` からの変換

できるだけ既にある変換の実装に丸投げしようとするのが肝である。

```rust
// 追加の制約なしの型での変換。

# #[repr(transparent)]
# struct MyStr(str);
#
# struct MyString(String);
# impl MyString {
#     fn new(s: String) -> Self { unimplemented!() }
# }
#
impl From<Box<MyStr>> for MyString {
    fn from(s: Box<MyStr>) -> Self {
        let boxed_str: Box<str> = s.into();
        Self::new(String::from(boxed_str))
    }
}

impl From<Box<MyStr>> for Box<str> {
    fn from(s: Box<MyStr>) -> Self {
        let boxed_ptr = Box::into_raw(s);
        unsafe {
            // SAFETY: The `MyStr` string is also valid as `str`.
            // SAFETY: Memory layouts for `str` and `MyStr` are compatible,
            // and `boxed_ptr` is a pointer from valid `Box<MyStr>`.
            Box::from_raw(boxed_ptr as *mut str)
        }
    }
}

impl From<Box<MyStr>> for String {
    fn from(s: Box<MyStr>) -> Self {
        let boxed_str: Box<str> = s.into();
        boxed_str.into()
    }
}
```

制約を弱める方向の実装であれば、追加の制約なしの場合とコードはほとんど変わらない。

```rust
// 追加の制約付きの型での変換。

# struct AsciiError;
#
# #[repr(transparent)]
# struct AsciiBytes(str);
#
# struct CreationError<T> {
#     source: T,
#     error: AsciiError,
# }
#
# struct AsciiByteBuf(String);
# impl AsciiByteBuf {
#     unsafe fn from_string_unchecked(s: String) -> Self { unimplemented!() }
# }
#
impl From<Box<AsciiBytes>> for AsciiByteBuf {
    fn from(s: Box<AsciiBytes>) -> Self {
        let boxed_str: Box<str> = s.into();
        let string: String = boxed_str.into();
        unsafe {
            // SAFETY: The string is valid as `AsciiBytes`,
            // and of course also as `AsciiByteBuf`.
            Self::from_string_unchecked(string)
        }
    }
}

impl From<Box<AsciiBytes>> for Box<str> {
    fn from(s: Box<AsciiBytes>) -> Self {
        let boxed_ptr: *mut AsciiBytes = Box::into_raw(s);
        unsafe {
            // SAFETY: The `AsciiBytes` bytes is also valid as `str`.
            // SAFETY: Memory layouts for `str` and `AsciiBytes` are compatible,
            // and `boxed_ptr` is a pointer from valid `Box<AsciiBytes>`.
            Box::from_raw(boxed_ptr as *mut str)
        }
    }
}

impl From<Box<AsciiBytes>> for Box<[u8]> {
    fn from(s: Box<AsciiBytes>) -> Self {
        let boxed_str: Box<str> = s.into();
        boxed_str.into()
    }
}
```


[`Arc`]: https://doc.rust-lang.org/stable/std/sync/struct.Arc.html
[`Box`]: https://doc.rust-lang.org/stable/std/boxed/struct.Box.html
[`Rc`]: https://doc.rust-lang.org/stable/std/rc/struct.Rc.html
