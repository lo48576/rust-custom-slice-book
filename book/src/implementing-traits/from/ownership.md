# 所有権の有無の変換

所有権なしのスライス型と所有権付きの型の変換は[既に `Borrow`, `ToOwned` で実装した][implementing-traits/borrow]が、これらの変換を `From` トレイトでも提供する。

所有権ありからなしへの変換は `From<&MyString> for &MyStr` のような実装をすることもできるが、基本的に借用を得る方向では [`Borrow`] や [`Deref`] を使うものなので、ここでは実装しない。

```rust
// 所有権なしから所有権付きへの変換。

# #[repr(transparent)]
# pub struct MyStr(str);
#
# #[derive(Clone)]
# pub struct MyString(String);
#
# impl core::borrow::Borrow<MyStr> for MyString {
#     #[inline]
#     fn borrow(&self) -> &MyStr { unimplemented!() }
# }
#
# impl std::borrow::ToOwned for MyStr {
#     type Owned = MyString;
#
#     fn to_owned(&self) -> Self::Owned { unimplemented!() }
# }
#
impl From<&MyStr> for MyString {
    #[inline]
    fn from(s: &MyStr) -> Self {
        s.to_owned()
    }
}

impl From<&mut MyStr> for MyString {
    #[inline]
    fn from(s: &mut MyStr) -> Self {
        s.to_owned()
    }
}

impl From<&MyString> for MyString {
    #[inline]
    fn from(s: &MyString) -> Self {
        s.clone()
    }
}
```


[`Borrow`]: https://doc.rust-lang.org/stable/std/borrow/trait.Borrow.html
[`Deref`]: https://doc.rust-lang.org/stable/std/ops/trait.Deref.html
[implementing-traits/borrow]: ../borrow.md
