# 追加の制約付きでの値の作成

追加の制約付きの型は、制約を満たさない値で作成が試みられたとき失敗する必要がある。
このような場合 `From` や `Into` を使わず、代わりに失敗の可能性のある変換を表現する [`TryFrom`] や [`TryInto`] を使う。

`&str` からの変換については特別に `FromStr` トレイトも存在するが、これは別の節で解説する。
<!-- TODO: `FromStr` の節へリンク -->

```rust
// スライス型からの変換の実装。

# use std::convert::TryFrom;
#
# pub struct AsciiError;
#
# #[repr(transparent)]
# pub struct AsciiStr(str);
# impl AsciiStr {
#     fn new(s: &str) -> Result<&Self, AsciiError> { unimplemented!() }
#     fn new_mut(s: &mut str) -> Result<&mut Self, AsciiError> { unimplemented!() }
# }
#
impl<'a> TryFrom<&'a str> for &'a AsciiStr {
    type Error = AsciiError;

    #[inline]
    fn try_from(s: &'a str) -> Result<Self, Self::Error> {
        AsciiStr::new(s)
    }
}

impl<'a> TryFrom<&'a mut str> for &'a mut AsciiStr {
    type Error = AsciiError;

    #[inline]
    fn try_from(s: &'a mut str) -> Result<Self, Self::Error> {
        AsciiStr::new_mut(s)
    }
}
```

またしても `'a` が多いが、残念ながら明示する必要があるので諦めて書こう。
`AsciiBytes` 用の実装はほぼ同じなので省略。

```rust
// `String` のみを受け付ける版の実装。

# use std::convert::TryFrom;
#
# pub struct AsciiError;
#
# #[repr(transparent)]
# pub struct AsciiStr(str);
# impl AsciiStr {
#     fn new(s: &str) -> Result<&Self, AsciiError> { unimplemented!() }
#     fn new_mut(s: &mut str) -> Result<&mut Self, AsciiError> { unimplemented!() }
# }
#
# pub struct FromStringError;
#
# pub struct AsciiString(String);
# impl AsciiString {
#     fn new(s: String) -> Result<Self, FromStringError> { unimplemented!() }
# }
#
# impl std::borrow::Borrow<AsciiStr> for AsciiString {
#     fn borrow(&self) -> &AsciiStr { unimplemented!() }
# }
#
# impl std::borrow::ToOwned for AsciiStr {
#     type Owned = AsciiString;
#     fn to_owned(&self) -> Self::Owned { unimplemented!() }
# }
#
impl TryFrom<String> for AsciiString {
    type Error = FromStringError;

    #[inline]
    fn try_from(s: String) -> Result<Self, Self::Error> {
        Self::new(s)
    }
}

impl TryFrom<&str> for AsciiString {
    type Error = AsciiError;

    fn try_from(s: &str) -> Result<Self, Self::Error> {
        AsciiStr::new(s).map(ToOwned::to_owned)
    }
}
```

型パラメータがない場合、かなり単純。
特に言うべきことはない。

ところが多相の場合、残念ながら `TryFrom` は思ったほど一般的にできない。

```rust,ignore
// 多相版の実装……と言いたいが、残念ながらこれは**コンパイルが通らない**

# use std::convert::TryFrom;
#
# pub struct AsciiError;
#
# #[repr(transparent)]
# pub struct AsciiBytes(str);
# impl AsciiBytes {
#     fn new(s: &str) -> Result<&Self, AsciiError> { unimplemented!() }
#     fn new_mut(s: &mut str) -> Result<&mut Self, AsciiError> { unimplemented!() }
# }
#
# pub struct CreationError<T>(T);
#
# pub struct AsciiByteBuf(ByteBuf);
# impl AsciiByteBuf {
#     fn new(s: ByteBuf) -> Result<Self, FromByteBufError> { unimplemented!() }
# }
#
impl<T: Into<Vec<u8>> + AsRef<[u8]>> TryFrom<T> for AsciiByteBuf {
    type Error = CreationError<T>;

    #[inline]
    fn try_from(s: T) -> Result<Self, Self::Error> {
        Self::new(s)
    }
}
```

このようなコードでは、以下のようなエラーが出る:

```text
error[E0119]: conflicting implementations of trait `std::convert::TryFrom<_>` for type `ascii_bytes::owned::AsciiByteBuf`:
   --> src/ascii_bytes.rs:241:5
    |
241 |     impl<T: Into<Vec<u8>> + AsRef<[u8]>> TryFrom<T> for AsciiByteBuf {
    |     ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    |
    = note: conflicting implementation in crate `core`:
            - impl<T, U> std::convert::TryFrom<U> for T
              where U: std::convert::Into<T>;

error: aborting due to previous error

For more information about this error, try `rustc --explain E0119`.
```

これは、第三者ユーザがたとえば `Foo` という型を定義して `Into<AsciiString> for Foo`, `Into<Vec<u8>> for Foo`, `AsRef<[u8]> for Foo` の3つのトレイト実装を用意してしまったとき、第三者ユーザが実装した `Into<AsciiString> for Foo` とあなたが実装した `TryFrom<T> for AsciiString` が競合すると言っているのである (`TryFrom` は `Into` が実装されている型の組に対して自動的に実装される。ちょうど `Into` と `From` と同様の関係である)。

根本的には「知らないところで第三者が定義するかもしれない型に対して余計なことをしすぎるな」という話なので、諦めて既知のめぼしい型に対して手作業で実装していこう。

```rust
// 動く実装。面倒だが仕方ない。

# use std::convert::TryFrom;
#
# pub struct AsciiError;
#
# #[repr(transparent)]
# pub struct AsciiBytes(str);
# impl AsciiBytes {
#     fn new<T: ?Sized + AsRef<[u8]>>(s: &T) -> Result<&Self, AsciiError> { unimplemented!() }
#     fn new_mut<T: ?Sized + AsMut<[u8]>>(s: &mut T) -> Result<&mut Self, AsciiError> { unimplemented!() }
# }
#
# pub struct CreationError<T> {
#     source: T,
#     error: AsciiError,
# }
#
# pub struct AsciiByteBuf(String);
# impl AsciiByteBuf {
#     fn new<T: Into<Vec<u8>> + AsRef<[u8]>>(s: T) -> Result<Self, CreationError<T>> { unimplemented!() }
# }
#
# impl std::borrow::Borrow<AsciiBytes> for AsciiByteBuf {
#     fn borrow(&self) -> &AsciiBytes { unimplemented!() }
# }
#
# impl std::borrow::ToOwned for AsciiBytes {
#     type Owned = AsciiByteBuf;
#     fn to_owned(&self) -> Self::Owned { unimplemented!() }
# }
#
impl TryFrom<String> for AsciiByteBuf {
    type Error = CreationError<String>;

    #[inline]
    fn try_from(s: String) -> Result<Self, Self::Error> {
        Self::new(s)
    }
}

impl TryFrom<Vec<u8>> for AsciiByteBuf {
    type Error = CreationError<Vec<u8>>;

    #[inline]
    fn try_from(s: Vec<u8>) -> Result<Self, Self::Error> {
        Self::new(s)
    }
}

impl<'a> TryFrom<&'a str> for AsciiByteBuf {
    type Error = AsciiError;

    fn try_from(s: &'a str) -> Result<Self, Self::Error> {
        AsciiBytes::new(s).map(ToOwned::to_owned)
    }
}

impl<'a> TryFrom<&'a [u8]> for AsciiByteBuf {
    type Error = AsciiError;

    fn try_from(s: &'a [u8]) -> Result<Self, Self::Error> {
        AsciiBytes::new(s).map(ToOwned::to_owned)
    }
}
```

ここで注意すべきなのは、検査の前にアロケーションを発生させるべきでないという点である。
`&str` から `String` を作った後にエラーが発覚した場合、 `String` 作成のためのメモリアロケーションは全くの無駄となってしまう。
これを避けるべく、例では検査と同時に `&AsciiBytes` を先に作り、そこから[先に解説した][implementing-traits/borrow] `ToOwned` トレイトを利用して `AsciiByteBuf` を作成した。
(`ToOwned::to_owned` より `Into::into` の方が短いので、そちらを使っても良い。)

`&AsciiBytes` を経由しない方法もあり、たとえば以下のように実装することもできる。

```rust
// 検証と値の作成を別々に行う。 `&AsciiBytes` を経由しないが unsafe なコードになる。

# use std::convert::TryFrom;
#
# pub struct AsciiError;
#
# #[repr(transparent)]
# pub struct AsciiBytes(str);
# impl AsciiBytes {
#     fn validate_bytes(s: &[u8]) -> Result<(), AsciiError> { unimplemented!() }
# }
#
# pub struct CreationError<T> {
#     source: T,
#     error: AsciiError,
# }
#
# pub struct AsciiByteBuf(String);
# impl AsciiByteBuf {
#     fn new<T: Into<Vec<u8>> + AsRef<[u8]>>(s: T) -> Result<Self, CreationError<T>> { unimplemented!() }
# }
#
# impl AsciiByteBuf {
#     fn from_string_unchecked(s: String) -> Self { unimplemented!() }
#     fn from_bytes_unchecked(s: Vec<u8>) -> Self { unimplemented!() }
# }
#
# impl std::borrow::Borrow<AsciiBytes> for AsciiByteBuf {
#     fn borrow(&self) -> &AsciiBytes { unimplemented!() }
# }
#
# impl std::borrow::ToOwned for AsciiBytes {
#     type Owned = AsciiByteBuf;
#     fn to_owned(&self) -> Self::Owned { unimplemented!() }
# }
#
impl<'a> TryFrom<&'a str> for AsciiByteBuf {
    type Error = CreationError<&'a str>;

    fn try_from(s: &'a str) -> Result<Self, Self::Error> {
        match AsciiBytes::validate_bytes(s.as_bytes()) {
            Ok(_) => Ok(unsafe {
                // SAFETY: `s` is already validated.
                Self::from_string_unchecked(s.to_owned())
            }),
            Err(e) => Err(CreationError {
                source: s,
                error: e,
            }),
        }
    }
}

impl<'a> TryFrom<&'a [u8]> for AsciiByteBuf {
    type Error = CreationError<&'a [u8]>;

    fn try_from(s: &'a [u8]) -> Result<Self, Self::Error> {
        match AsciiBytes::validate_bytes(s) {
            Ok(_) => Ok(unsafe {
                // SAFETY: `s` is already validated.
                Self::from_bytes_unchecked(s.to_owned())
            }),
            Err(e) => Err(CreationError {
                source: s,
                error: e,
            }),
        }
    }
}
```

個人的には独自スライス型 (ここでは `AsciiBytes`) を経由する方がシンプルで良いと思うが、まあ好みの問題だろう。


[`TryFrom`]: https://doc.rust-lang.org/stable/std/convert/trait.TryFrom.html
[`TryInto`]: https://doc.rust-lang.org/stable/std/convert/trait.TryInto.html
[implementing-traits/borrow]: borrow.md
