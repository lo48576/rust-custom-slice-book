# 追加の制約なしでの値の作成

まず手始めに、 `new()` 相当のものから実装していこう。

```rust
// 型を見れば当然のもの。

# #[repr(transparent)]
# pub struct MyStr(str);
# impl MyStr {
#     fn new(s: &str) -> &Self { unimplemented!() }
#     fn new_mut(s: &mut str) -> &mut Self { unimplemented!() }
# }
#
impl<'a> From<&'a str> for &'a MyStr {
    #[inline]
    fn from(s: &'a str) -> Self {
        MyStr::new(s)
    }
}

impl<'a> From<&'a mut str> for &'a mut MyStr {
    #[inline]
    fn from(s: &'a mut str) -> Self {
        MyStr::new_mut(s)
    }
}
```

`'a` が多いが、残念ながら明示する必要があるので諦めて書こう。
それから、 `&str` など独自スライス型の内部の型からの作成もあとで使いたくなることがあるため、実装しておくのがよい。
`&mut str` などからの変換も実装しておくと便利かもしれないが、そこはお好みである。
ちなみに `From<&mut str> for String` は割と最近 (Rust 1.44.0) になって実装が追加された。

```rust
// #それはそう

# pub struct MyString(String);
# impl MyString {
#     fn new(s: String) -> Self { unimplemented!() }
# }
#
impl From<String> for MyString {
    #[inline]
    fn from(s: String) -> Self {
        Self::new(s)
    }
}

impl From<&str> for MyString {
    #[inline]
    fn from(s: &str) -> Self {
        Self::new(s.to_owned())
    }
}
```

所有権付きの型についてはもはや説明は必要ないだろう。
