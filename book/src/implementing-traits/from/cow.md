# `Cow` との変換

<aside class="admon important">

## 先に `Borrow` と `ToOwned` トレイトの実装が必要

`Cow<'_, T>` は `T` が `Sized` な (つまりサイズがコンパイル時に確定している) 型である場合には何もせずうまく使えるが、 `T` が unsized である場合 (DST である場合を含む) はセットアップが必要である。
より具体的には、 `Cow` は「借用された型」と「所有権付きの型」を通常 `&'_ T` と `T` として扱うが、 `T` が unsized である場合これらの型は全く別々の型になるため、それらの型を相互に紐付けする必要がある。
たとえば `str` と `String` 、あるいは `[u8]` と `Vec<u8>` などの型の組がそのような組であるが、いずれも [`std::borrow::Borrow`][`Borrow`] トレイトと [`std::borrowed::ToOwned`][`ToOwned`] トレイトで紐付いている。

`Borrow` と `ToOwned` の実装については[別の節で説明した][implementing-traits/borrow]。

</aside>

スライス型には多くのバリエーションがあるが、それらの間の変換をどこまで実装するかは好み次第でもある。
たとえば以下の例の `From<&'a MyString> for Cow<'a, MyStr>` などはユーザ側で `.as_my_str()` を使って `&MyStr` にしてから `From<&'a MyStr>` の実装を使うこともできるが、標準ライブラリで `From<&'a String> for Cow<'a str>` が実装されていたため実装することにした。
逆に `From<Box<MyStr>> for Cow<'_, MyStr>` を低コストで実装することも可能だが、標準ライブラリにこれに相当する実装がなかったため例には入れなかった。

```rust
// `Cow` への変換。

use std::borrow::Cow;

# #[repr(transparent)]
# pub struct MyStr(str);
# impl MyStr {
#     fn as_str(&self) -> &str { unimplemented!() }
# }
#
# #[derive(Clone)]
# pub struct MyString(String);
# impl MyString {
#     fn as_my_str(&self) -> &MyStr { unimplemented!() }
#     fn as_my_str_mut(&mut self) -> &mut MyStr { unimplemented!() }
#     fn new(s: String) -> Self { unimplemented!() }
# }
#
# impl std::borrow::Borrow<MyStr> for MyString {
#     fn borrow(&self) -> &MyStr { unimplemented!() }
# }
#
# impl std::borrow::BorrowMut<MyStr> for MyString {
#     fn borrow_mut(&mut self) -> &mut MyStr { unimplemented!() }
# }
#
# impl std::borrow::ToOwned for MyStr {
#     type Owned = MyString;
#
#     fn to_owned(&self) -> Self::Owned { unimplemented!() }
# }
#
impl<'a> From<&'a MyStr> for Cow<'a, MyStr> {
    #[inline]
    fn from(s: &'a MyStr) -> Self {
        Cow::Borrowed(s)
    }
}

impl From<MyString> for Cow<'_, MyStr> {
    #[inline]
    fn from(s: MyString) -> Self {
        Cow::Owned(s)
    }
}

impl<'a> From<&'a MyString> for Cow<'a, MyStr> {
    #[inline]
    fn from(s: &'a MyString) -> Self {
        Cow::Borrowed(s.as_my_str())
    }
}
```

```rust
// `Cow` からの変換。

use std::borrow::Cow;

# #[repr(transparent)]
# pub struct MyStr(str);
# impl MyStr {
#     fn as_str(&self) -> &str { unimplemented!() }
# }
#
# #[derive(Clone)]
# pub struct MyString(String);
# impl MyString {
#     fn as_my_str(&self) -> &MyStr { unimplemented!() }
#     fn as_my_str_mut(&mut self) -> &mut MyStr { unimplemented!() }
#     fn new(s: String) -> Self { unimplemented!() }
# }
#
# impl std::borrow::Borrow<MyStr> for MyString {
#     fn borrow(&self) -> &MyStr { unimplemented!() }
# }
#
# impl std::borrow::BorrowMut<MyStr> for MyString {
#     fn borrow_mut(&mut self) -> &mut MyStr { unimplemented!() }
# }
#
# impl std::borrow::ToOwned for MyStr {
#     type Owned = MyString;
#
#     fn to_owned(&self) -> Self::Owned { unimplemented!() }
# }
#
# impl From<&MyStr> for Box<MyStr> {
#     fn from(s: &MyStr) -> Self { unimplemented!() }
# }
#
# impl From<MyString> for Box<MyStr> {
#     fn from(s: MyString) -> Self { unimplemented!() }
# }
#
impl From<Cow<'_, MyStr>> for Box<MyStr> {
    fn from(s: Cow<'_, MyStr>) -> Self {
        match s {
            Cow::Borrowed(s) => s.into(),
            Cow::Owned(s) => s.into(),
        }
    }
}

impl From<Cow<'_, MyStr>> for MyString {
    fn from(s: Cow<'_, MyStr>) -> Self {
        match s {
            Cow::Borrowed(s) => s.to_owned(),
            Cow::Owned(s) => s,
        }
    }
}
```

追加の制約ありの型についても同様の実装になるため、例は省略する。

[`BorrowMut`]: https://doc.rust-lang.org/stable/std/borrow/trait.BorrowMut.html
[`Borrow`]: https://doc.rust-lang.org/stable/std/borrow/trait.Borrow.html
[`ToOwned`]: https://doc.rust-lang.org/stable/std/borrow/trait.ToOwned.html
[implementing-traits/borrow]: ../borrow.md
