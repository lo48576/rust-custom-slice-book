# はじめに

Rust でプログラムを書くうえで、用途や制約に応じた適切な型の定義は大変重要である。
たとえば Rust ではバイト列は `[u8]`、 UTF-8 文字列は [`str`]、OS ネイティブのエンコーディングの文字列は [`std::ffi::OsStr`][`OsStr`]、パス文字列は [`std::path::Path`][`Path`] といったように、横着すればひとつの型で済むような様々なデータに対してそれぞれの特徴に応じた型を標準で用意している。

ときに標準ライブラリで用意された型ばかりでなく、自分で専用の型を用意したいこともある。
たとえば「相対パスのみを保持できる型」のようなものを実装したくなるかもしれない。
もちろん Rust でこれは可能なのだが、 `str` のような (値として利用するときは `&str` のように参照を使う) 可変長のスライスのような型は、定義したり十分な利便性を確保するのに多少のコツが必要となる。

本書では、独自のスライス型や関係する定義の方法や理由について、実際に動く例を挙げて詳細に解説する。
サンプルコードが長く、本書中での引用が断片的あるいは不完全になることがあるため、動く状態のコードが本書のソースコードとともに <https://gitlab.com/lo48576/rust-custom-slice-book> で提供される。

## 免責

正しい情報の提示については最善を尽くすが、本文書の情報の正確性や有用性については保障されず、無保証で提供される。

修正、追加、更新、翻訳などの貢献を歓迎する。

リポジトリは <https://gitlab.com/lo48576/rust-custom-slice-book> である。

## ライセンス

![Licensed under CC-BY 4.0](cc-by-4.0.svg)

本書は [クリエイティブ・コモンズ 表示 4.0 国際 ライセンス][CC-BY-4.0] の下で提供されている。

また、サンプルコードは [CC0 1.0 全世界][CC0-1.0] の下で提供されている。


[CC-BY-4.0]: https://creativecommons.org/licenses/by/4.0/deed.ja
[CC0-1.0]: https://creativecommons.org/publicdomain/zero/1.0/deed.ja
[`OsStr`]: https://doc.rust-lang.org/stable/std/ffi/struct.OsStr.html
[`Path`]: https://doc.rust-lang.org/stable/std/path/struct.Path.html
[`str`]: https://doc.rust-lang.org/stable/std/primitive.str.html
