# スライス型の定義

型定義の時点では、追加の制約の有無は基本的に関係ない[^footnote-additional-restriction-at-definition]。

追加の制約なしのスライス型:
```rust
/// My custom string slice type.
#[derive(PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(transparent)]
// Comparisons implemented for the type are consistent (at least it is intended to be so).
// See <https://github.com/rust-lang/rust-clippy/issues/2025>.
#[allow(clippy::derive_hash_xor_eq, clippy::derive_ord_xor_partial_ord)]
pub struct MyStr(str);
```

追加の制約付きのスライス型:
```rust
/// ASCII string slice type.
#[derive(PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(transparent)]
// Comparisons implemented for the type are consistent (at least it is intended to be so).
// See <https://github.com/rust-lang/rust-clippy/issues/2025>.
#[allow(clippy::derive_hash_xor_eq, clippy::derive_ord_xor_partial_ord)]
pub struct AsciiStr(str);
```

追加の制約付きのスライス型の別の例:
```rust
/// ASCII bytes slice type.
#[derive(PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(transparent)]
// Comparisons implemented for the type are consistent (at least it is intended to be so).
// See <https://github.com/rust-lang/rust-clippy/issues/2025>.
#[allow(clippy::derive_hash_xor_eq, clippy::derive_ord_xor_partial_ord)]
pub struct AsciiBytes(str);
```

AsciiStr と AsciiBytes の違いは、前者が文字列との相互運用のみを前提とする単純な実装で、後者は文字列のみならずバイト列との相互変換などが扱える点である。
利便性で考えれば後者が良いが、実装が若干煩雑になる箇所もある。
よって、本書では単純な実装で誤魔化すことも複雑な実装もできるよう、両方のコード例を提示する。

<aside class="admon note">

## バイト列でも `str` で持つ理由

たとえば「ASCII 文字列」は単なるバイト列ともいえるし、 UTF-8 文字列であるともいえる。
このように `&[u8]` で参照することも `&str` で参照することもできる場合、好みの問題ではあるがより強い制約を表明している型で持つと多少楽かもしれない。

というのも、制約を外すのは簡単だが、制約を加えるには大抵実行時に検査のコストがかかるからである。
たとえば `&str` 型は「データが妥当な UTF-8 バイト列であること」という制約を持っており、これを取り去って `&[u8]` にするのは実質ノーコストで可能である。
しかし `&[u8]` のデータを `&str` に変換するには、妥当な UTF-8 バイト列であるか実行時に検査するか、あるいは確信がある場合でも unsafe な処理を行わなければいけない。

ただし、 `AsciiBytes` が内部的に `str` を持つのに `[u8]` との相互運用性を十分に確保しようとすると、これら3つの型の間で適切な型変換を挟む必要があるため、実装としては若干煩雑になる。

最後には好みの問題となるが、以上のように、制約の強い型を積極的に使うことが望ましい点と、煩雑な実装のサンプルを提示したいという理由から、本記事では `AsciiBytes` 型では `[u8]` でなく `str` 型を内部に持たせることとする。

実際、筆者が実装した [`datetime-string`] クレートでは、内部的なバイト列操作の多さが理由で、 `str` でなく `[u8]` で ASCII 文字列を保持する独自スライス型を定義している (例: [`DateTimeStr` 型](https://gitlab.com/lo48576/datetime-string/-/blob/v0.2.1/src/rfc3339/date_time.rs#L72))。

</aside>


* * *

<!-- TODO: 型パラメータ対応の節を書き終えたら修正 -->
[^footnote-additional-restriction-at-definition]: たとえばその追加の制約 (典型的には文法) を型パラメータとして与えるなどを考えると、定義の段階である程度の設計の調整が必要になるが、それは別の節で説明する (かもしれない)。

[datetime-string]: https://crates.io/crates/datetime-string
