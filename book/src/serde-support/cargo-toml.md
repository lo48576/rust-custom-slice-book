# Cargo.toml

何はともあれ Cargo.toml からである。

```toml
# serde への依存を設定した Cargo.toml の例 (抜粋)。

[features]
default = ["std"]

alloc = []
std = ["alloc"]

serde-alloc = ["serde/alloc"]
serde-std = ["serde/std"]

[dependencies]

[dependencies.serde]
version = "1.0.118"
optional = true
default-features = false
features = ["derive"]
```

serde の alloc, std 対応の有効化を制御するための feature 定義と、依存の指定を追加した。

```toml
serde-alloc = ["serde/alloc"]
serde-std = ["serde/std"]
```

ユーザが `serde-std` feature を有効化すると、依存している serde についても自動的に `std` feature を有効化するという feature である。
`serde-alloc` も同様に serde クレートの `alloc` feature を有効化するための feature である。

```toml
[dependencies.serde]
version = "1.0.118"
```

serde クレートへの依存を指定する。
`serde = { version = "...", ... }` のように1行で指定しても良いのだが、これだと項目が多い場合に行が長くなって可読性が落ちたり git で管理するときの差分が見づらくなることが考えられるため、今回は隔離して複数行で記述することにした。

依存クレートのバージョンを指定するときは、[特別な理由がなければ最新のバージョンを指定すること][dont-leave-cargo-toml-broken]。

```toml
optional = true
```

serde を必要としないユーザには serde をビルド・リンクするコストを支払わせないべきなので、 `optional = true` でデフォルトで無効にする。

依存クレートに `optional = true` を指定した場合、クレートと同名の feature (この例では `serde` feature) が自動的に用意されるため、 `[features]` 部分に `serde = []` のようなものを用意する必要はない。

```toml
default-features = false
```

serde はデフォルトで `std` feature を有効化しているので、デフォルトで無効化するために `default-features = false` を指定する。

> The `serde` crate has a Cargo feature named `"std"` that is enabled by default.
> In order to use Serde in a no\_std context this feature needs to be disabled.
> Modify your Serde dependency in Cargo.toml to opt out of enabled-by-default features.
>
> [No-std support · Serde](https://serde.rs/no-std.html), 2020-12-25 閲覧

```toml
features = ["derive"]
```

`#[derive(serde::Serialize)]` のような custom derive を利用するために `derive` feature を有効化する。

以上で Cargo.toml のセットアップは完了である。


[dont-leave-cargo-toml-broken]: https://blog.cardina1.red/2020/12/14/dont-leave-cargo-toml-broken/
