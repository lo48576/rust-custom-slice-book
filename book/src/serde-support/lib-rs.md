# lib.rs

まず、必須ではないが便利な仕掛けとして、適切な serde の feature が有効化されていないときに、実際に serde を使っている場所よりも早い段階で lib.rs からエラーを出すことができる。
serde に `String` や `Vec` をサポートさせるには `alloc` か `std` feature を有効化する必要があるが、ユーザがこれを忘れたときエラーで原因を報告できるということである。
(ちなみに serde クレートの `alloc` feature は今のところ (serde v1.0.118 時点) nightly rustc が必要らしい。)

```rust
// 適切な feature が設定されているか確認する lib.rs の例 (抜粋)。

#[cfg(all(
    feature = "serde",
    feature = "alloc",
    not(feature = "std"),
    not(any(feature = "serde-alloc", feature = "serde-std"))
))]
compile_error!(
    "When both `serde` and `alloc` features are enabled, \
     `serde-alloc` or `serde-std` should also be enabled."
);

#[cfg(all(feature = "serde", feature = "std", not(feature = "serde-std")))]
compile_error!(
    "When both `serde` and `std` features are enabled, `serde-std` should also be enabled."
);
```

構造としては、マズい組み合わせだった (具体的には、指定されるべき feature が不足していた) ときに `compile_error!` マクロでメッセージとともにコンパイルエラーを発生させるという簡単なものである。

![不適当な feature の組み合わせでコンパイルエラーが出る。](serde-feature-compile-error.png)

<details>

<summary>テキストで見る</summary>

```text
$ cargo clippy --no-default-features --features=alloc,serde
    Checking custom-slice-book-sample v0.0.0 (/home/lo48576/works/repos/mydev/rust-custom-slice-book/sample)
error: When both `serde` and `alloc` features are enabled, `serde-alloc` or `serde-std` should also be enabled.
  --> src/lib.rs:10:1
   |
10 | / compile_error!(
11 | |     "When both `serde` and `alloc` features are enabled, \
12 | |      `serde-alloc` or `serde-std` should also be enabled."
13 | | );
   | |__^

error: aborting due to previous error

error: could not compile `custom-slice-book-sample`

To learn more, run the command again with --verbose.
$
```

</details>

まず簡単な std 用の方から解説しよう。

```rust
#[cfg(all(feature = "serde", feature = "std", not(feature = "serde-std")))]
# compile_error!(
#     "When both `serde` and `std` features are enabled, `serde-std` should also be enabled."
# );
```

`all()` 内の条件指定のうち最初の2つ、 `feature = "serde"` と `feature = "std"` がそれぞれ「`serde` feature が有効である」と「`std` feature が有効である」という条件、最後の `not(feature = "serde-std")` が「`serde-std` feature が有効でない」という条件である。
`all()` は名前の通り「すべての条件が成り立っている」という条件である。
これらを組み合わせて、「`serde` と `std` feature が有効で、かつ `serde-std` feature が無効である場合」という条件になる。

```rust
# #[cfg(all(feature = "serde", feature = "std", not(feature = "serde-std")))]
compile_error!(
    "When both `serde` and `std` features are enabled, `serde-std` should also be enabled."
);
```

[`compile_error!`] マクロは与えた文字列をエラーメッセージとして出力してコンパイルエラーを発生させる。

```rust
#[cfg(all(
    feature = "serde",
    feature = "alloc",
    not(feature = "std"),
    not(any(feature = "serde-alloc", feature = "serde-std"))
))]
# compile_error!(
#     "When both `serde` and `alloc` features are enabled, \
#      `serde-alloc` or `serde-std` should also be enabled."
# );
```

alloc 用の条件は少々複雑だが、根本的には同じである。
std との違いは `not(feature = "std")` が追加されている点と、 `not(feature = "serde-std")` の代わりに `not(any(feature = "serde-alloc", feature = "serde-std"))` が指定されている点である。

`not(feature = "std")` は必須ではないが、これがないと `std` feature が有効なときまで alloc 環境用のメッセージが一緒に出てしまう。
これは `std` feature が自動的に `alloc` feature も有効化するよう Cargo.toml で設定したからである。
`not(feature = "std")` を条件に追加することで、 `std` feature が有効な場合を除外できる。

`any()` は「列挙された条件のひとつ以上が有効である」という条件である。
このことから `not(any(feature = "serde-alloc", feature = "serde-std"))` は文字通り「`serde-alloc` も `serde-std` も有効でない」という条件であるとわかる。
`all(not(feature = "serde-alloc"), not(feature = "serde-std"))` と書いても同じことではあるが、 `not()` を複数回書くことになるので若干長くなり読みづらいかもしれない。

これで「`serde` と `alloc` feature が有効で、しかし `std` feature は有効でなく、また `serde-alloc` feature も `serde-std` feature も有効でない」という条件が指定できた。

```rust
# #[cfg(all(
#     feature = "serde",
#     feature = "alloc",
#     not(feature = "std"),
#     not(any(feature = "serde-alloc", feature = "serde-std"))
# ))]
compile_error!(
    "When both `serde` and `alloc` features are enabled, \
     `serde-alloc` or `serde-std` should also be enabled."
);
```

残りはもう解説の必要はないだろう。
std 用のものをコピペしたきりメッセージを変え忘れたりしないよう注意しよう。
(←忘れていて首をひねったことがある顔)


[`compile_error!`]: https://doc.rust-lang.org/1.48.0/core/macro.compile_error.html
