# 所有権付きの型

幸いなことに、所有権なしのスライス型の実装とほとんど変わらない。
`From` や `TryFrom` 等を実装しておいたおかげである。

まずは追加の制約なしの型から。

```rust
// 追加の制約なしのスライス型で `Deserialize` を実装する。

#[cfg(feature = "serde")]
impl<'de> serde::Deserialize<'de> for MyString {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        /// Visitor for `MyString`.
        struct StringVisitor;

        impl<'de> serde::de::Visitor<'de> for StringVisitor {
            type Value = MyString;

            #[inline]
            fn expecting(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                f.write_str("a string")
            }

            #[inline]
            fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                Ok(Self::Value::from(v))
            }

            #[inline]
            fn visit_string<E>(self, v: String) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                Ok(Self::Value::from(v))
            }
        }

        deserializer.deserialize_string(StringVisitor)
    }
}
```

着目すべきなのは、 [`Visitor::Value`][`serde::de::Visitor::Value`] を適切に設定することと、 [`visit_str()`][`serde::de::Visitor::visit_str`] と [`visit_string()`][`serde::de::Visitor::visit_string`] の両方を実装することくらいだろうか。

```rust
# #[cfg(feature = "serde")]
# impl<'de> serde::Deserialize<'de> for MyString {
#     fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
#     where
#         D: serde::Deserializer<'de>,
#     {
#         /// Visitor for `MyString`.
#         struct StringVisitor;
        impl<'de> serde::de::Visitor<'de> for StringVisitor {
            type Value = MyString;
#             fn expecting(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result { unimplemented!() }
#         }
#         deserializer.deserialize_string(StringVisitor)
#     }
# }
```

当然ではあるが、今回返したいのは `MyString` なので、 [`Visitor`][`serde::de::Visitor`] トレイトの関連型もそのように設定する。
`&'de MyStr` のときと違って `'de` が表れないのはポイントで、作られる値が入力の lifetime に非依存ということである。
すなわち、デシリアライズ後に即座に入力が破棄されても作られた値はそれ以上長く存在できるという、所有権付きの型に当然の性質が表れている。

```rust
# #[cfg(feature = "serde")]
# impl<'de> serde::Deserialize<'de> for MyString {
#     fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
#     where
#         D: serde::Deserializer<'de>,
#     {
#         /// Visitor for `MyString`.
#         struct StringVisitor;
#         impl<'de> serde::de::Visitor<'de> for StringVisitor {
#             type Value = MyString;
#             fn expecting(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result { unimplemented!() }
            #[inline]
            fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                Ok(Self::Value::from(v))
            }

            #[inline]
            fn visit_string<E>(self, v: String) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                Ok(Self::Value::from(v))
            }
#         }
#         deserializer.deserialize_string(StringVisitor)
#     }
# }
```

メソッドの中身については `&MyStr` の場合と同様だが、今回定義するメソッドは [`visit_str`][`serde::de::Visitor::visit_str`] と [`visit_string`][`serde::de::Visitor::visit_string`] である。
これらはいずれもデシリアライズ後すぐに入力が破棄されることを想定しており、デシリアライザが内部的に文字列デコードで `String` を使った場合は `visit_string()` が呼ばれ、 `&str` で済んだ場合であれば `visit_str()` が呼ばれる。

デシリアライザが内部的に `String` を使うというのは、たとえばエスケープの解除などが考えられる。
`foo\"bar` のような入力を `foo"bar` として解釈しなければならない場合、入力である `foo\"bar` をそのまま `&str` として `visit_str()` に渡されても困るわけである。
このように入力フォーマット特有の解釈や変換などの処理の過程で `String` を利用することがあり、 visitor が `visit_string()` を実装しているというのは「もしデシリアライザが所有権付きの文字列を持っていたら、所有権ごと渡してくれるとより効率的な処理ができます」という表明である。

`visit_string()` の実装は必須ではないが、実装しないことのメリットが皆無なので実装すべきである。
実装しなかった場合、デシリアライザが `String` を使っていても `visit_str()` に参照が渡される。
このとき `Self::Value::from()` つまり `<MyString as From<&str>>::from` は内部で動的にメモリを確保することになるので、デシリアライザが行った分と合わせて二度のアロケーションが発生しており非効率になってしまう。

<aside class="admon note">

## 手抜き実装

所有権付きの型では、追加の制約なしであれば一応 `#[serde(transparent)]` のおかげで `derive(serde::Deserialize)` を使うことはできる。
しかしこの場合、デシリアライズ失敗時のエラーメッセージとして内部の型のものが使われるので、もしメッセージを変えたければ自分で実装すべきである。

たとえば以下のコードでは、内部の型が String なので、エラー時には期待されていた値の説明として "a string" が使われる。
`MyString` の場合それでも良いのだが、 strong typedef で型を用意する場合は何か特殊な意味や意図を持つ型を作りたい場合だろうから、 "a string" という説明は通常不適当だろう。
適切な説明を用意するため `Deserialize` を手動で実装することをおすすめしたい。

```rust
// 追加の制約なしのスライス型で `derive(serde::Deserialize)` することも可能ではある。

/// My string.
#[cfg_attr(feature = "serde", derive(serde::Deserialize, serde::Serialize))]
#[cfg_attr(feature = "serde", serde(transparent))]
pub struct MyString(String);
```

残念ながら追加の制約付きの型ではこのような手抜きさえできないので、諦めて自前で実装するしかない。

</aside>

追加の制約がある場合についても、所有権なしのスライス型を参考にいけるので、解説なしでコード例だけ載せておく。

```rust
// 追加の制約なしのスライス型で文字列のみを受け付ける `Deserialize` 実装。

#[cfg(feature = "serde")]
impl<'de> serde::Deserialize<'de> for AsciiString {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        /// Visitor for `AsciiString`.
        struct StringVisitor;

        impl<'de> serde::de::Visitor<'de> for StringVisitor {
            type Value = AsciiString;

            #[inline]
            fn expecting(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                f.write_str("an ASCII string")
            }

            #[inline]
            fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                Self::Value::try_from(v).map_err(E::custom)
            }

            #[inline]
            fn visit_string<E>(self, v: String) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                Self::Value::try_from(v).map_err(E::custom)
            }
        }

        deserializer.deserialize_string(StringVisitor)
    }
}
```

```rust
// 追加の制約なしのスライス型で文字列とバイト列を受け付ける `Deserialize` 実装。

#[cfg(feature = "serde")]
impl<'de> serde::Deserialize<'de> for AsciiByteBuf {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        /// Visitor for `AsciiByteBuf`.
        struct ByteBufVisitor;

        impl<'de> serde::de::Visitor<'de> for ByteBufVisitor {
            type Value = AsciiByteBuf;

            #[inline]
            fn expecting(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                f.write_str("ASCII bytes")
            }

            #[inline]
            fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                Self::Value::try_from(v).map_err(E::custom)
            }

            #[inline]
            fn visit_string<E>(self, v: String) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                Self::Value::try_from(v).map_err(E::custom)
            }

            #[inline]
            fn visit_bytes<E>(self, v: &[u8]) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                Self::Value::try_from(v).map_err(E::custom)
            }

            #[inline]
            fn visit_byte_buf<E>(self, v: Vec<u8>) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                Self::Value::try_from(v).map_err(E::custom)
            }
        }

        deserializer.deserialize_any(ByteBufVisitor)
    }
}
```

見た目には単調で情報量の薄いコードに見えるが、それは様々な型 (たとえば `&str`, `String`, `&[u8]`, `Vec<u8>` など) に対して適切な `From` や `TryFrom` トレイトの実装を既に済ませてあるからであり、内部的に実行されるコードは少しずつ異なっている。


[`serde::de::Visitor::Value`]: https://docs.rs/serde/1.0.118/serde/de/trait.Visitor.html#associatedtype.Value
[`serde::de::Visitor::visit_str`]: https://docs.rs/serde/1.0.118/serde/de/trait.Visitor.html#method.visit_str
[`serde::de::Visitor::visit_string`]: https://docs.rs/serde/1.0.118/serde/de/trait.Visitor.html#method.visit_string
[`serde::de::Visitor`]: https://docs.rs/serde/1.0.118/serde/de/trait.Visitor.html
