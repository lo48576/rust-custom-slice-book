# `serde::Serialize` の実装

serialize は楽である。
内部の型と同じように (たとえば `str` を持つなら文字列として、 `[u8]` を持つならバイト列として) 扱ってよいのであれば、 `#[derive(serde::Serialize)]` と `#[serde(transparent)]` を指定するだけである。

```rust
// ごく普通な serde の使い方。

/// My string.
#[repr(transparent)]
#[cfg_attr(feature = "serde", derive(serde::Serialize))]
#[cfg_attr(feature = "serde", serde(transparent))]
pub struct MyStr(str);
```

`cfg_attr` は、もし第1引数の条件が成立している場合は第2引数の attribute を有効化するというものである。
ここで誤って `cfg_attr()` でなく `cfg()` を使うと、条件が成立しなかったとき型定義ごと無視されることになるので注意。

```rust
// **誤った定義例**。
// `#[cfg(..)]` は次の `#[..]` などではなく、アイテム (つまり型定義) 全体に適用される。

/// My string.
#[repr(transparent)]
// ↓ こうすると、 `serde` feature が無効なとき
// `derive(..)` どころでなく `MyStr` の型定義が無視される。
#[cfg(feature = "serde")]
#[derive(serde::Serialize)]
pub struct MyStr(str);
```

`#[serde(transparent)]` は、まさに strong typedef された型のための属性である。
その効果は、外側の包んでいる型の存在を無視して、 derive された [`Serialize`][`serde::Serialize`] や [`Deserialize`][`serde::Deserialize`] の実装で内部の型を透過的に利用するというものである。
上の例では、 `MyStr` によって包まれているということを無視して `str` を読むのと同様な `Serialize` 実装を生やしてくれるようになる。

> Serialize and deserialize a newtype struct or a braced struct with one field exactly the same as if its one field were serialized and deserialized by itself.
> Analogous to `#[repr(transparent)]`.
>
> ——[Container attributes · Serde](https://serde.rs/container-attrs.html#transparent), 2020-12-25 閲覧

もし内部の型と別の形式でシリアライズしたいときは、素直に実装することになる。
serde の serialize は (deserialize と比べると) かなり簡単なので、実際のコードの例を載せておく。

以下は [datetime-string v0.2.1][datetime-string-v0.2.1] クレート の [`FullTimeStr`][`datetime_string::rfc3339::FullTimeStr`] 型
([src/rfc3339/full\_time.rs](https://gitlab.com/lo48576/datetime-string/-/blob/v0.2.1/src/rfc3339/full_time.rs), [40–53行 (型定義)](https://gitlab.com/lo48576/datetime-string/-/blob/v0.2.1/src/rfc3339/full_time.rs#L40-53), [558–566行 (実装)](https://gitlab.com/lo48576/datetime-string/-/blob/v0.2.1/src/rfc3339/full_time.rs#L558-566)) の実際の実装である。
この型は ASCII 文字列ではあるが、内部処理の実装しやすさの都合で `str` でなく `[u8]` を内部の型として使っている。

```rust,ignore
// 内部で `[u8]` を持つ型を `str` のようにシリアライズする実装例。

/// String slice for a time in RFC 3339 [`full-time`] format, such as `12:34:56.7890-23:12`.
///
/// [`full-time`]: https://tools.ietf.org/html/rfc3339#section-5.6
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(transparent)]
// Note that `derive(Serialize)` cannot used here, because it encodes this as
// `[u8]` rather than as a string.
//
// Comparisons implemented for the type are consistent (at least it is intended to be so).
// See <https://github.com/rust-lang/rust-clippy/issues/2025>.
// Note that `clippy::derive_ord_xor_partial_ord` would be introduced since Rust 1.47.0.
#[allow(clippy::derive_hash_xor_eq)]
#[allow(clippy::unknown_clippy_lints, clippy::derive_ord_xor_partial_ord)]
pub struct FullTimeStr([u8]);

/* 中略 */

#[cfg(feature = "serde")]
impl Serialize for FullTimeStr {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_str(self.as_str())
    }
}
```


[`datetime_string::rfc3339::FullTimeStr`]: https://docs.rs/datetime-string/0.2.1/datetime_string/rfc3339/struct.FullTimeStr.html
[`serde::Deserialize`]: https://docs.rs/serde/1.0.118/serde/trait.Deserialize.html
[`serde::Serialize`]: https://docs.rs/serde/1.0.118/serde/trait.Serialize.html
[datetime-string-v0.2.1]: https://crates.io/crates/datetime-string/0.2.1
